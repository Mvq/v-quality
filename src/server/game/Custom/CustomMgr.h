#ifndef _VQMGR_H
#define _VQMGR_H

#include "Player.h"

struct VQRank
{
    uint16 pointsNeeded;
    std::string name;
    std::string color;
	std::string icon;

    VQRank(uint16 _pointsNeeded = 0, std::string _name = "Unranked", std::string _color = "A4A4A4", std::string _icon = "U") :
    pointsNeeded(_pointsNeeded), name(_name), color(_color), icon(_icon) { }
};

typedef std::list<VQRank> VQRankListType;

class CustomMgr
{
private:
    VQRankListType VQRankList;

public:
    CustomMgr();
    ~CustomMgr();

	static CustomMgr* instance()
	{
		static CustomMgr instance;
		return &instance;
	}

    void AddRank(uint16 pointsNeeded, std::string name, std::string color, std::string icon);
    void LoadRanks();

	VQRank GetRankDataByPoints(uint16 points);
    VQRank GetRankDataByName(std::string name);
    VQRank GetRankDataByColor(std::string color);

	std::string classtostring(uint16 class_id)
	{
		const char* classStr = NULL;
		switch (class_id)
		{
			//warrior
		case CLASS_WARRIOR: classStr = "|TInterface\\ICONS\\inv_sword_27:20:20:0:0|t";
			break;
			//paladin
		case CLASS_PALADIN: classStr = "|TInterface\\ICONS\\ability_thunderbolt:20:20:0:0|t";
			break;
			//hunter
		case CLASS_HUNTER: classStr = "|TInterface\\ICONS\\inv_weapon_bow_07:20:20:0:0|t";
			break;
			//rogue
		case CLASS_ROGUE: classStr = "|TInterface\\ICONS\\inv_throwingknife_04:20:20:0:0|t";
			break;
			//priest
		case CLASS_PRIEST: classStr = "|TInterface\\ICONS\\inv_staff_30:20:20:0:0|t";
			break;
			//Deathknight
		case CLASS_DEATH_KNIGHT: classStr = "|TInterface\\ICONS\\spell_deathknight_classicon:20:20:0:0|t";
			break;
			//Shaman
		case CLASS_SHAMAN: classStr = "|TInterface\\ICONS\\spell_nature_bloodlust:20:20:0:0|t";
			break;
			//mage
		case CLASS_MAGE: classStr = "|TInterface\\ICONS\\inv_staff_13.jpg:20:20:0:0|t";
			break;
			//Warlock
		case CLASS_WARLOCK: classStr = "|TInterface\\ICONS\\spell_nature_drowsy:20:20:0:0|t";
			break;
			//Druid
		case CLASS_DRUID: classStr = "|TInterface\\ICONS\\Ability_Druid_Maul:20:20:0:0|t";
			break;
		default: classStr = "|TInterface\\ICONS\\Inv_misc_questionmark:20:20:0:0|t";
			break;
		}
		return classStr;
	}

	std::string killstorank(uint8 race, uint32 kills)
	{
		const char* rankStr = NULL;
		// alliance icons [blue]
		if ((race == RACE_HUMAN) || (race == RACE_DWARF) || (race == RACE_NIGHTELF) || (race == RACE_GNOME) || (race == RACE_DRAENEI))
		{
			if (kills < 250) // rank under 250 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_01:20:20:0:0|t";
			else if (kills >= 250 && kills < 500) // rank at 250 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_02:20:20:0:0|t";
			else if (kills >= 500 && kills < 750) // rank at 500 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_03:20:20:0:0|t";
			else if (kills >= 750 && kills < 1000) // rank at 750 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_04:20:20:0:0|t";
			else if (kills >= 1000 && kills < 1250) // rank at 1000 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_05:20:20:0:0|t";
			else if (kills >= 1250 && kills < 1500) // rank at 1250 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_06:20:20:0:0|t";
			else if (kills >= 1500 && kills < 1750) // rank at 1500 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_07:20:20:0:0|t";
			else if (kills >= 1750 && kills < 2000) // rank at 1750 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_08:20:20:0:0|t";
			else if (kills >= 2000 && kills < 2250) // rank at 2000 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_09:20:20:0:0|t";
			else if (kills >= 2250 && kills < 2500) // rank at 2250 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_10:20:20:0:0|t";
			else if (kills >= 2500 && kills < 2750) // rank at 2500 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_11:20:20:0:0|t";
			else if (kills >= 2750 && kills < 3000) // rank at 2750 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_12:20:20:0:0|t";
			else if (kills >= 3000 && kills < 4000) // rank at 3000 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_13:20:20:0:0|t";
			else if (kills >= 4000 && kills < 7000) // rank at 4000 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_14:20:20:0:0|t";
			else if (kills >= 7000 && kills < 10000) // rank at 7000 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_15:20:20:0:0|t";
			else if (kills >= 10000) // rank at 10000+ kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_P_250K:20:20:0:0|t";
			else
				rankStr = "|TInterface\\ICONS\\Inv_misc_questionmark:20:20:0:0|t";
		}
		else // horde icons [red]
		{
			if (kills < 250) // rank under 250 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_01:20:20:0:0|t";
			else if (kills >= 250 && kills < 500) // rank at 250 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_02:20:20:0:0|t";
			else if (kills >= 500 && kills < 750) // rank at 500 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_03:20:20:0:0|t";
			else if (kills >= 750 && kills < 1000) // rank at 750 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_04:20:20:0:0|t";
			else if (kills >= 1000 && kills < 1250) // rank at 1000 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_05:20:20:0:0|t";
			else if (kills >= 1250 && kills < 1500) // rank at 1250 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_06:20:20:0:0|t";
			else if (kills >= 1500 && kills < 1750) // rank at 1500 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_07:20:20:0:0|t";
			else if (kills >= 1750 && kills < 2000) // rank at 1750 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_08:20:20:0:0|t";
			else if (kills >= 2000 && kills < 2250) // rank at 2000 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_09:20:20:0:0|t";
			else if (kills >= 2250 && kills < 2500) // rank at 2250 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_10:20:20:0:0|t";
			else if (kills >= 2500 && kills < 2750) // rank at 2500 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_11:20:20:0:0|t";
			else if (kills >= 2750 && kills < 3000) // rank at 2750 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_12:20:20:0:0|t";
			else if (kills >= 3000 && kills < 4000) // rank at 3000 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_13:20:20:0:0|t";
			else if (kills >= 4000 && kills < 7000) // rank at 4000 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_14:20:20:0:0|t";
			else if (kills >= 7000 && kills < 10000) // rank at 7000 kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_15:20:20:0:0|t";
			else if (kills >= 10000) // rank at 10000+ kills
				rankStr = "|TInterface\\ICONS\\Achievement_PVP_P_250K:20:20:0:0|t";
			else
				rankStr = "|TInterface\\ICONS\\Inv_misc_questionmark:20:20:0:0|t";
		}
		return rankStr;
	}

	std::string TimeToStamp(time_t t)
	{
		tm aTm;
		localtime_r(&t, &aTm);
		//       YYYY   year
		//       MM     month (2 digits 01-12)
		//       DD     day (2 digits 01-31)
		//       HH     hour (2 digits 00-23)
		//       MM     minutes (2 digits 00-59)
		//       SS     seconds (2 digits 00-59)
		char buf[20];
		snprintf(buf, 20, "%02d-%02d-%04d", aTm.tm_mday, aTm.tm_mon + 1, aTm.tm_year + 1900, aTm.tm_hour, aTm.tm_min, aTm.tm_sec);
		return std::string(buf);
	}
};

//Assign the class
#define sCustomMgr CustomMgr::instance()

#endif