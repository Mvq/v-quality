#include "CustomMgr.h"
#include "World.h"
#include "ObjectMgr.h"
#include "WorldSession.h"
#include "ScriptedGossip.h"
#include "Config.h"
#include "ScriptPCH.h"

CustomMgr::CustomMgr() 
{
}

VQRank CustomMgr::GetRankDataByPoints(uint16 points)
{
    VQRank rank = VQRank();
    for(VQRankListType::iterator it = VQRankList.begin(); it != VQRankList.end(); ++it)
    {
        if(it->pointsNeeded <= points)
            rank = *it;
        else
            break;
    }
    return rank;
}
VQRank CustomMgr::GetRankDataByName(std::string name)
{
    for(VQRankListType::iterator it = VQRankList.begin(); it != VQRankList.end(); ++it)
    {
        if(it->name == name)
            return *it;
    }
    return VQRank();
}
VQRank CustomMgr::GetRankDataByColor(std::string color)
{
    for(VQRankListType::iterator it = VQRankList.begin(); it != VQRankList.end(); ++it)
    {
        if(it->color == color)
            return *it;
    }
    return VQRank();
}

void CustomMgr::AddRank(uint16 pointsNeeded, std::string name, std::string color, std::string icon)
{
    if(name.empty() || color.empty() || icon.empty())
        return;
    
    for(VQRankListType::iterator it = VQRankList.begin(); it != VQRankList.end(); ++it)
        if(it->pointsNeeded >= pointsNeeded)
            return;

    VQRankList.push_back(VQRank(pointsNeeded, name, color, icon));
}

void CustomMgr::LoadRanks()
{
    // Get the time before we started loading.
    uint32 oldMSTime = getMSTime();

    //                                                      0             1          2
    QueryResult result = WorldDatabase.Query("SELECT rankPointsNeeded, rankName, rankColor, rankIcon FROM custom_ranks ORDER BY rankPointsNeeded ASC");

    //If no rows found
    if (!result) 
    {
        //Display something
        TC_LOG_INFO("server.loading", ">> Loaded 0 VQ ranks. DB table `custom_ranks` is empty.");
    } 
    else
    {
        //Continue loading.

        //Reset row counter
        uint32 count = 0;

        //Loop through the results
        do 
        {
            // Get the fields
            Field *fields = result->Fetch();
			AddRank(fields[0].GetUInt16(), fields[1].GetString(), fields[2].GetString(), fields[3].GetString());

            //Increase row counter
            ++count;
        } while (result->NextRow());

        // Log that we loaded everything.
        TC_LOG_INFO("server.loading", ">> Loaded %u VQ", count);
    }
}

CustomMgr::~CustomMgr() 
{
    VQRankList.clear();
}