#include "ScriptPCH.h"
#include "Chat.h"
#include "Language.h"
#include "ObjectMgr.h"
#include <time.h>

#ifdef __unix__
#include "../boost/date_time.hpp"
#elif defined(_WIN32) || defined(WIN32)
#include "boost/date_time.hpp"
#endif

class kick_player_delay_event : public BasicEvent
{
public:
	kick_player_delay_event(Player* player) : _player(player) { }

	bool Execute(uint64 /*time*/, uint32 /*diff*/)
	{
		if (_player && _player->GetSession())
			_player->GetSession()->KickPlayer();
		return true;
	}

private:
	Player* _player;
};

static inline std::string getDateTime(long timestamp) {
	std::stringstream date_str;
	boost::posix_time::ptime pt_1 = boost::posix_time::from_time_t(timestamp);

	boost::gregorian::date d = pt_1.date();

	auto td = pt_1.time_of_day();

	/* construct the Date Time string */
	date_str << d.year() << "-" << std::setw(2) << std::setfill('0') << d.month().as_number() << "-" << std::setw(2) << std::setfill('0') << d.day() << " "
		<< td.hours() << ":" << td.minutes() << ":" << td.seconds();

	return date_str.str();
}

std::string GetItemIkon(uint32 entry, uint32 width, uint32 height, int x, int y)
{
	std::ostringstream ss;
	ss << "|TInterface";
	const ItemTemplate* temp = sObjectMgr->GetItemTemplate(entry);
	const ItemDisplayInfoEntry* dispInfo = NULL;
	if (temp)
	{
		dispInfo = sItemDisplayInfoStore.LookupEntry(temp->DisplayInfoID);
		if (dispInfo)
			ss << "/ICONS/" << dispInfo->inventoryIcon;
	}
	if (!dispInfo)
		ss << "/InventoryItems/WoWUnknownItem01";
	ss << ":" << width << ":" << height << ":" << x << ":" << y << "|t";
	return ss.str();
}

class DonorNPC : public CreatureScript
{
public:
	DonorNPC() : CreatureScript("ShopNPC") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		player->PlayerTalkClass->ClearMenus();

		char points[250];

		if (player->GetItemCount(20880, false) > 0)
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Warlock_Eradication:31:31:-22:0|tUse Transmog Token ->", 800, 0);

		snprintf(points, 250, "|TInterface/ICONS/INV_Jewelcrafting_DragonsEye03:40:40:-24:0|t Redeem Vote Points. (%u VP)", player->GetSession()->GetVP());
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, points, 1, 0);
		snprintf(points, 250, "|TInterface/ICONS/INV_Jewelcrafting_DragonsEye05:40:40:-24:0|t Redeem Donor Points. (%u DP)", player->GetSession()->GetDP());
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, points, 2, 0);
		if (!player->m_BoostContainer.empty())
		{
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Quests_Completed_07:31:31:-22:0|tYour Boosting information:", MAIN_MENU, MAIN_MENU);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", MAIN_MENU, MAIN_MENU);
			for (Player::BoostContainer::const_iterator itr = player->m_BoostContainer.begin(); itr != player->m_BoostContainer.end(); ++itr)
			{
				std::ostringstream menu;
				menu << "Active: " << " " << player->GetBoosterName(itr->second->boost_type) << "\nExpiration date: " << getDateTime(itr->second->unsetdate);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, menu.str().c_str(), MAIN_MENU, MAIN_MENU);
				menu.clear();
				menu.str("");
			}
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------", MAIN_MENU, MAIN_MENU);
		}
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:40:40:-24:0|t Close the Shop.", 1000, 0);
		player->SEND_GOSSIP_MENU(50015, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
	{
		player->PlayerTalkClass->ClearMenus();
		char points[250];

		switch (sender)
		{
		case 800:
		{
			player->PlayerTalkClass->ClearMenus();
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface/ICONS/Creatureportrait_SC_EyeOfAcherus_02:35:35:-22:0|t Choose an item for Transmog:", 800, 1);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 800, 0);
			WorldSession* session = player->GetSession();
			for (uint8 slot = EQUIPMENT_SLOT_START; slot < EQUIPMENT_SLOT_END; ++slot)
			{
				if (slot == EQUIPMENT_SLOT_BODY || slot == EQUIPMENT_SLOT_FINGER1 || slot == EQUIPMENT_SLOT_FINGER2 || slot == EQUIPMENT_SLOT_TRINKET1 || slot == EQUIPMENT_SLOT_TRINKET2)
					continue;

				Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, slot);
				if (item)
				{
					std::string icon = GetItemIkon(item->GetEntry(), 30, 30, -22, 0);
					player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, icon + std::string(item->GetTemplate()->Name1), 801, slot, "Enter the Item Entry you wish your selected item shall look like:\n", 0, true);
				}
			}
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 800, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|t Close the Shop.", 1000, 0);
			player->SEND_GOSSIP_MENU(50016, creature->GetGUID());
		}
			break;
		case 1:
			snprintf(points, 250, "|TInterface/ICONS/INV_Jewelcrafting_DragonsEye03:35:35:-24:0|t Current Vote Points: %u", player->GetSession()->GetVP());
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, points, 1, 1);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 1, 0);

			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_bg_trueavshutout.:35:35:-22:0|t Vote Items.", 200, 0);
			//player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Druid_NaturalPerfection:35:35:-22:0|t Visuals.", 11, 0);
			//player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_dungeon_theviolethold_25man:35:35:-22:0|t Character Options", 10, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 1, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|t Close the Shop.", 1000, 0);
			player->SEND_GOSSIP_MENU(50016, creature->GetGUID());
			break;
		case 2:
			snprintf(points, 250, "|TInterface/ICONS/INV_Jewelcrafting_DragonsEye05:35:35:-24:0|t Current Donor Points: %u", player->GetSession()->GetDP());
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, points, 1, 1);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 2, 0);

			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_bg_trueavshutout.:35:35:-22:0|t Boosters.", 20, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Druid_NaturalPerfection:35:35:-22:0|t Donor Items.", 14, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_dungeon_gloryoftheraider:35:35:-22:0|t Premium Rank (VIP).", 3, 0);
			//player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Druid_NaturalPerfection:35:35:-22:0|t Visuals.", 6, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_dungeon_theviolethold_25man:35:35:-22:0|t Character Options", 5, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 2, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|t Close The Shop.", 1000, 0);
			player->SEND_GOSSIP_MENU(50017, creature->GetGUID());
			break;
		case MAIN_MENU: OnGossipHello(player, creature); break;
		case 1000:
			player->PlayerTalkClass->SendCloseGossip();
			break;
		case 200:
			player->PlayerTalkClass->ClearMenus();
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Misc_Rune_07:35:35:-22:0|tWSG Marks [6 for 1 VP]", 1003, 1, "Confirm purchase of:\n\nWSG Marks.\n\nWrite: How many you want 1 VP = 6 WSG Marks then click Accept to confirm.", 0, true);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Frost_Wizardmark:35:35:-22:0|tWintergrasp Commendation [|cffFF00005 VP|r]\nGrants your 2000 Honor.", 1003, 2, "Confirm purchase of:\n\nWintergrasp Commendation.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Shirt_15:35:35:-22:0|tPurple Trophy Tabard [|cffFF000010 VP|r]", 1003, 3, "Confirm purchase of:\n\nPurple Trophy Tabard.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Shirt_15:35:35:-22:0|tGreen Trophy Tabard [|cffFF000010 VP|r]", 1003, 4, "Confirm purchase of:\n\nGreen Trophy Tabard.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Shirt_Guildtabard_01:35:35:-22:0|tTabard of the Shattered Sun [|cffFF000015 VP|r]", 1003, 5, "Confirm purchase of:\n\nTabard of the Shattered Sun.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Misc_Orb_03:35:35:-22:0|tSuper Simian Sphere [|cffFF000025 VP|r]", 1003, 6, "Confirm purchase of:\n\nSuper Simian Sphere.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Mount_Charger:35:35:-22:0|tArgent Charger [|cffFF000030 VP|r]", 1003, 7, "Confirm purchase of:\n\nArgent Charger.\n", 0, false);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 2, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|t Close The Shop.", 1000, 0);
			player->SEND_GOSSIP_MENU(50017, creature->GetGUID());
			break;
		case 14:
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Shield_20:35:35:-22:0|tHat Token [|cffFF00002 DP|r]", 1002, 1, "Confirm purchase of:\n\n1 Hat Token.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Shirt_GuildTabard_01:35:35:-22:0|tTabard of the Void [|cffFF00005 DP|r]", 1002, 2, "Confirm purchase of:\n\n1 Tabard of the Void.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Shirt_GuildTabard_01:35:35:-22:0|tTabard of Nature [|cffFF00005 DP|r]", 1002, 3, "Confirm purchase of:\n\nTabard of Nature.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Misc_TabardPVP_01:35:35:-22:0|tTabard of Frost [|cffFF00005 DP|r]", 1002, 4, "Confirm purchase of:\n\nTabard of Frost.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Shirt_GuildTabard_01:35:35:-22:0|tTabard of Brilliance [|cffFF00005 DP|r]", 1002, 5, "Confirm purchase of:\n\nTabard of Brilliance.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Shirt_GuildTabard_01:35:35:-22:0|tTabard of the Arcane [|cffFF00005 DP|r]", 1002, 6, "Confirm purchase of:\n\nTabard of the Arcane.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Shirt_GuildTabard_01:35:35:-22:0|tTabard of Fury [|cffFF00005 DP|r]", 1002, 7, "Confirm purchase of:\n\nTabard of Fury.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Misc_TabardPVP_02:35:35:-22:0|tTabard of Flame [|cffFF00005 DP|r]", 1002, 8, "Confirm purchase of:\n\nTabard of Flame.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Shirt_GuildTabard_01:35:35:-22:0|tTabard of the Defender [|cffFF00005 DP|r]", 1002, 9, "Confirm purchase of:\n\nTabard of the Defender.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Mount_JungleTiger:35:35:-22:0|tReins of the Bengal Tiger [|cffFF00005 DP|r]", 1002, 10, "Confirm purchase of:\n\nReins of the Bengal Tiger.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Misc_Fish_Turtle_02:35:35:-22:0|tSea Turtle [|cffFF00005 DP|r]", 1002, 11, "Confirm purchase of:\n\nSea Turtle.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Shirt_Red_01:35:35:-22:0|tShirt of the Tattered Raven [|cffFF000010 DP|r]", 1002, 12, "Confirm purchase of:\n\nShirt of the Tattered Raven.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_sword_39:35:35:-22:0|tThunderfury Main-Hand [|cffFF000010 DP|r]\nTransmogs your main-hand into Thunderfury.", 1002, 16, "Confirm purchase of:\n\nThunderfury Main-Hand Transmogrification.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_sword_39:35:35:-22:0|tThunderfury Off-Hand [|cffFF000010 DP|r]\nTransmogs your off-hand into Thunderfury.", 1002, 17, "Confirm purchase of:\n\nThunderfury Off-Hand Transmogrification.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_sword_07:35:35:-22:0|tWarglaive of Azzinoth (Left) [|cffFF000010 DP|r]Transmogs your main-hand into Warglaive.", 1002, 18, "Confirm purchase of:\n\nWarglaive of Azzinoth (Left).\nTransmogrification.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Mace_89:35:35:-22:0|tThorim's Lightning Mace [|cffFF000010 DP|r]Transmogs your main-hand into Thorim's Lightning Mace.", 1002, 19, "Confirm purchase of:\n\nThorim's Lightning Mace.\nTransmogrification.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_DeathKnight_SummonDeathCharger:35:35:-22:0|tReins of Crimson Deathcharger  [|cffFF000010 DP|r]", 1002, 13, "Confirm purchase of:\n\nReins of the Crimson Deathcharger.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Misc_QirajiCrystal_05:35:35:-22:0|tBlack Qiraji Resonating Crystal [|cffFF000015 DP|r]", 1002, 14, "Confirm purchase of:\n\nBlack Qiraji Resonating Crystal.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Sword_2H_AshbringerCorrupt:35:35:-22:0|tCorrupted Ashbringer [|cffFF000015 DP|r]", 1002, 20, "Confirm purchase of:\n\nCorrupted Ashbringer.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Warlock_Eradication:35:35:-22:0|tTransmog Token [|cffFF00004 DP|r]\nA token to make one weapon/armor look like anything you choose.", 1002, 15, "Confirm purchase of:\n\nTransmog Token.\n", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Warlock_Eradication:35:35:-22:0|tFull Set Transmog Token (x13) [|cffFF000035 DP|r]\n13 tokens to make one weapon/armor look like anything you choose.", 1002, 150, "Confirm purchase of:\n\n13 Transmog Tokens.\n", 0, false);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 2, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|t Close The Shop.", 1000, 0);
			player->SEND_GOSSIP_MENU(50017, creature->GetGUID());
			break;
		case 15:
			break;
		case 17:
			if (player->GetSession()->GetDP() < 30)
			{
				player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 30 - You have: %u", player->GetSession()->GetDP());
				OnGossipHello(player, creature);
				return false;
			}

			player->GetSession()->ModifyDP(-30);
			player->AddItem(247256, 1);
			player->GetSession()->SendAreaTriggerMessage("Congratulations!\nYou can now Transmogrify for Free.");
			OnGossipHello(player, creature);
			break;
		case 18:
			if (player->GetSession()->GetDP() < 100)
			{
				player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 100 - You have: %u", player->GetSession()->GetDP());
				OnGossipHello(player, creature);
				return false;
			}

			player->GetSession()->ModifyDP(-100);
			player->AddItem(247257, 1);
			player->GetSession()->SendAreaTriggerMessage("Congratulations!\nYou can now Transmogrify for Free.");
			OnGossipHello(player, creature);
			break;
		case 3:
		{
			player->PlayerTalkClass->ClearMenus();

			if (player->GetSession()->IsPremium())
			{
				player->GetSession()->SendAreaTriggerMessage("You are already a VIP Member.");
				OnGossipHello(player, creature);
				return false;
			}

			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_paladin_artofwar:25:25:-22:0|tVIP Rank:\nDuration: 1 Week.\nPrice: 20 DP. Discount: 0 DP.", 100, 1, "Confirm purchase of:\n\n1 week of VIP Rank.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_paladin_artofwar:25:25:-22:0|tVIP Rank:\nDuration: 2 Weeks.\nPrice: 25 DP. Discount: 5 DP.", 100, 2, "Confirm purchase of:\n\n2 weeks of VIP Rank.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_paladin_artofwar:25:25:-22:0|tVIP Rank:\nDuration: 3 Weeks.\nPrice: 30 DP. Discount: 10 DP.", 100, 3, "Confirm purchase of:\n\n3 weeks of VIP Rank.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_paladin_artofwar:25:25:-22:0|tVIP Rank:\nDuration: 4 Weeks.\nPrice: 35 DP. Discount: 15 DP.", 100, 4, "Confirm purchase of:\n\n4 weeks of VIP Rank.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_paladin_artofwar:25:25:-22:0|tVIP Rank:\nDuration: 5 Weeks.\nPrice: 40 DP. Discount: 20 DP.", 100, 5, "Confirm purchase of:\n\n5 weeks of VIP Rank.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_paladin_artofwar:25:25:-22:0|tVIP Rank:\nDuration: 6 Weeks.\nPrice: 45 DP. Discount: 25 DP.", 100, 6, "Confirm purchase of:\n\n6 weeks of VIP Rank.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_paladin_artofwar:25:25:-22:0|tVIP Rank:\nDuration: 7 Weeks.\nPrice: 50 DP. Discount: 30 DP.", 100, 7, "Confirm purchase of:\n\n7 weeks of VIP Rank.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_paladin_artofwar:25:25:-22:0|tVIP Rank:\nDuration: 8 Weeks.\nPrice: 55 DP. Discount: 35 DP.", 100, 8, "Confirm purchase of:\n\n8 weeks of VIP Rank.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_paladin_artofwar:25:25:-22:0|tVIP Rank:\nDuration: 9 Weeks.\nPrice: 60 DP. Discount: 40 DP.", 100, 9, "Confirm purchase of:\n\n9 weeks of VIP Rank.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_paladin_artofwar:25:25:-22:0|tVIP Rank:\nDuration: Permanent.\nPrice: 150 DP. Discount: Infinite DP.", 100, 10, "Confirm purchase of:\n\nPermanent VIP Rank.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Holy_DevineAegis:35:35:-22:0|t <- Main Menu.", MAIN_MENU, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|t Close the shop.", 1000, 0);
			player->PlayerTalkClass->SendGossipMenu(50018, creature->GetGUID());
		}
		break;
		case 5:
		{
			player->PlayerTalkClass->ClearMenus();
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_bg_returnxflags_def_wsg:25:25:-22:0|tChange Faction: Price: 5 DP", 6, GOSSIP_ACTION_INFO_DEF, "Confirm purchase of:\n\nCharacter Faction Change.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_bg_returnxflags_def_wsg:25:25:-22:0|tChange Race: Price: 5 DP", 7, GOSSIP_ACTION_INFO_DEF, "Confirm purchase of:\n\nCharacter Race Change.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_bg_returnxflags_def_wsg:25:25:-22:0|tChange Style: Price: 5 DP", 8, GOSSIP_ACTION_INFO_DEF, "Confirm purchase of:\n\nCharacter Layout Change.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Holy_DevineAegis:35:35:-22:0|t <- Main Menu.", MAIN_MENU, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|t Close the shop.", 1000, 0);
			player->PlayerTalkClass->SendGossipMenu(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
		}
		break;
		case 10:
		{
			player->PlayerTalkClass->ClearMenus();
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_bg_returnxflags_def_wsg:25:25:-22:0|tChange Faction: Price: 120 VP", 111, GOSSIP_ACTION_INFO_DEF, "Confirm purchase of:\n\nCharacter Faction Change.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_bg_returnxflags_def_wsg:25:25:-22:0|tChange Race: Price: 120 VP", 121, GOSSIP_ACTION_INFO_DEF, "Confirm purchase of:\n\nCharacter Race Change.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_bg_returnxflags_def_wsg:25:25:-22:0|tChange Style: Price: 120 VP", 131, GOSSIP_ACTION_INFO_DEF, "Confirm purchase of:\n\nCharacter Layout Change.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Holy_DevineAegis:35:35:-22:0|t <- Main Menu.", MAIN_MENU, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|t Close the shop.", 1000, 0);
			player->PlayerTalkClass->SendGossipMenu(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
		}
		break;
		case 6:
		{
			player->PlayerTalkClass->ClearMenus();
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Warlock_Eradication:25:25:-22:0|tShadowmourne Visual Low: 5 DP", 9, GOSSIP_ACTION_INFO_DEF, "Confirm purchase of:\n\nShadowmourne Visual.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Warlock_Eradication:25:25:-22:0|tShadowmourne Visual Large: 10 DP", 13, GOSSIP_ACTION_INFO_DEF, "Confirm purchase of:\n\nShadowmourne Visual Large.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Vehicle_ShellShieldGenerator:25:25:-22:0|tSphere Visual: Price: 5 DP", 10, GOSSIP_ACTION_INFO_DEF, "Confirm purchase of:\n\nSphere Visual.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Vehicle_SonicShockwave:25:25:-22:0|tDefensive Shell Visual: Price: 5 DP", 11, GOSSIP_ACTION_INFO_DEF, "Confirm purchase of:\n\nDefensive Shell Visual.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Dungeon_NexusRaid_Heroic:25:25:-22:0|tEthereal Becon Visual: Price: 5 DP", 12, GOSSIP_ACTION_INFO_DEF, "Confirm purchase of:\n\nEthereal Becon Visual.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Holy_DevineAegis:35:35:-22:0|t <- Main Menu.", MAIN_MENU, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|t Close the shop.", 1000, 0);
			player->PlayerTalkClass->SendGossipMenu(50026, creature->GetGUID());
		}
		break;
		case 11:
		{
			player->PlayerTalkClass->ClearMenus();
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Warlock_Eradication:25:25:-22:0|tShadowmourne Visual Low: 120 VP", 91, GOSSIP_ACTION_INFO_DEF, "Confirm purchase of:\n\nShadowmourne Visual.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Warlock_Eradication:25:25:-22:0|tShadowmourne Visual Large: 180 VP", 92, GOSSIP_ACTION_INFO_DEF, "Confirm purchase of:\n\nShadowmourne Visual Large.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Vehicle_ShellShieldGenerator:25:25:-22:0|tSphere Visual: Price: 120 VP", 93, GOSSIP_ACTION_INFO_DEF, "Confirm purchase of:\n\nSphere Visual.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Vehicle_SonicShockwave:25:25:-22:0|tDefensive Shell Visual: Price: 110 VP", 94, GOSSIP_ACTION_INFO_DEF, "Confirm purchase of:\n\nDefensive Shell Visual.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Dungeon_NexusRaid_Heroic:25:25:-22:0|tEthereal Becon Visual: Price: 130 VP", 95, GOSSIP_ACTION_INFO_DEF, "Confirm purchase of:\n\nEthereal Becon Visual.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Holy_DevineAegis:35:35:-22:0|t <- Main Menu.", MAIN_MENU, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|t Close the shop.", 1000, 0);
			player->PlayerTalkClass->SendGossipMenu(50026, creature->GetGUID());
		}
		break;
		case 20:
		{
			player->PlayerTalkClass->ClearMenus();
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface/ICONS/Ability_Paladin_BlessedHands:35:35:-22:0|tSelect Boosting mod:", 20, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 20, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_2v2_7:35:35:-22:0|t Honor Booster.", 30, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_5v5_7:35:35:-22:0|t Arena Points Pooster.", 80, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_3v3_7:35:35:-22:0|t Rank Points Booster.", 40, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Misc_Rune_07:35:35:-22:0|t WSG Mark Booster.", 50, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 20, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Holy_DevineAegis:35:35:-22:0|t <- Main Menu.", MAIN_MENU, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|t Close the shop.", 1000, 0);
			player->SEND_GOSSIP_MENU(50019, creature->GetGUID());
		}
		break;
		case 21:
		{
			if (QueryResult result = CharacterDatabase.PQuery("SELECT 1 FROM character_boost WHERE id = %u AND boost_type = 0", player->GetGUIDLow()))
			{
				player->GetSession()->SendAreaTriggerMessage("You already have an active gold booster.");
				OnGossipHello(player, creature);
				return false;
			}

			player->PlayerTalkClass->ClearMenus();
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface/ICONS/Ability_Paladin_BlessedHands:35:35:-22:0|tSelect Multiplier:", 21, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 21, 0);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Misc_Coin_01:35:35:-22:0|t 10% Extra Gold:\nDuration: 1 week.\nPrice: 5 DP.", 22, 10, "Confirm purchase of:\n\n1 week of 10% Extra Gold boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Misc_Coin_01:35:35:-22:0|t 15% Extra Gold:\nDuration: 1 week.\nPrice: 10 DP.", 22, 15, "Confirm purchase of:\n\n1 week of 15% Extra Gold boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Misc_Coin_01:35:35:-22:0|t 20% Extra Gold:\nDuration: 1 week.\nPrice: 15 DP.", 22, 20, "Confirm purchase of:\n\n1 week of 20% Extra Gold boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Misc_Coin_01:35:35:-22:0|t 25% Extra Gold:\nDuration: 1 week.\nPrice: 20 DP.", 22, 25, "Confirm purchase of:\n\n1 week of 25% Extra Gold boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Misc_Coin_01:35:35:-22:0|t 30% Extra Gold:\nDuration: 1 week.\nPrice: 25 DP.", 22, 30, "Confirm purchase of:\n\n1 week of 30% Extra Gold boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Misc_Coin_01:35:35:-22:0|t 40% Extra Gold:\nDuration: 1 week.\nPrice: 30 DP.", 22, 40, "Confirm purchase of:\n\n1 week of 40% Extra Gold boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\INV_Misc_Coin_01:35:35:-22:0|t 50% Extra Gold:\nDuration: 1 week.\nPrice: 40 DP.", 22, 50, "Confirm purchase of:\n\n1 week of 50% Extra Gold boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 21, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Holy_DevineAegis:35:35:-22:0|t <- Main Menu.", MAIN_MENU, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|t Close the shop.", 1000, 0);
			player->SEND_GOSSIP_MENU(50020, creature->GetGUID());
		}
		break;
		case 30:
		{
			if (QueryResult result = CharacterDatabase.PQuery("SELECT 1 FROM character_boost WHERE id = %u AND boost_type = 1", player->GetGUIDLow()))
			{
				player->GetSession()->SendAreaTriggerMessage("You already have an active honor booster.");
				OnGossipHello(player, creature);
				return false;
			}

			player->PlayerTalkClass->ClearMenus();
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface/ICONS/Ability_Paladin_BlessedHands:35:35:-22:0|tSelect Multiplier:", 30, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 30, 0);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_2v2_7:35:35:-22:0|t 10% Extra Honor Points:\nDuration: 1 week.\nPrice: 5 DP.", 32, 10, "Confirm purchase of:\n\n1 week of 10% Extra Honor boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_2v2_7:35:35:-22:0|t 15% Extra Honor Points:\nDuration: 1 week.\nPrice: 10 DP.", 32, 15, "Confirm purchase of:\n\n1 week of 15% Extra Honor boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_2v2_7:35:35:-22:0|t 20% Extra Honor Points:\nDuration: 1 week.\nPrice: 15 DP.", 32, 20, "Confirm purchase of:\n\n1 week of 20% Extra Honor boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_2v2_7:35:35:-22:0|t 25% Extra Honor Points:\nDuration: 1 week.\nPrice: 20 DP.", 32, 25, "Confirm purchase of:\n\n1 week of 25% Extra Honor boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_2v2_7:35:35:-22:0|t 30% Extra Honor Points:\nDuration: 1 week.\nPrice: 25 DP.", 32, 30, "Confirm purchase of:\n\n1 week of 30% Extra Honor boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_2v2_7:35:35:-22:0|t 40% Extra Honor Points:\nDuration: 1 week.\nPrice: 30 DP.", 32, 40, "Confirm purchase of:\n\n1 week of 40% Extra Honor boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_2v2_7:35:35:-22:0|t 50% Extra Honor Points:\nDuration: 1 week.\nPrice: 40 DP.", 32, 50, "Confirm purchase of:\n\n1 week of 50% Extra Honor boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 30, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Holy_DevineAegis:35:35:-22:0|t <- Main Menu.", MAIN_MENU, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|t Close the shop.", 1000, 0);
			player->SEND_GOSSIP_MENU(50021, creature->GetGUID());
		}
		break;
		case 40:
		{
			if (QueryResult result = CharacterDatabase.PQuery("SELECT 1 FROM character_boost WHERE id = %u AND boost_type = 2", player->GetGUIDLow()))
			{
				player->GetSession()->SendAreaTriggerMessage("You already have an active rank points booster.");
				OnGossipHello(player, creature);
				return false;
			}

			player->PlayerTalkClass->ClearMenus();
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface/ICONS/Ability_Paladin_BlessedHands:35:35:-22:0|tSelect Multiplier:", 40, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 40, 0);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_3v3_7:35:35:-22:0|t 2 Extra Rank Points Per Win:\nDuration: 1 week.\nPrice: 5 DP.", 42, 1, "Confirm purchase of:\n\n1 week of 10% Extra Rank Points boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_3v3_7:35:35:-22:0|t 4 Extra Rank Points Per Win:\nDuration: 1 week.\nPrice: 10 DP.", 42, 2, "Confirm purchase of:\n\n1 week of 15% Extra Rank Points boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_3v3_7:35:35:-22:0|t 7 Extra Rank Points Per Win:\nDuration: 1 week.\nPrice: 15 DP.", 42, 3, "Confirm purchase of:\n\n1 week of 20% Extra Rank Points boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_3v3_7:35:35:-22:0|t 10 Extra Rank Points Per Win:\nDuration: 1 week.\nPrice: 20 DP.", 42, 4, "Confirm purchase of:\n\n1 week of 25% Extra Rank Points boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_3v3_7:35:35:-22:0|t 15 Extra Rank Points Per Win:\nDuration: 1 week.\nPrice: 25 DP.", 42, 5, "Confirm purchase of:\n\n1 week of 30% Extra Rank Points boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_3v3_7:35:35:-22:0|t 20 Extra Rank Points Per Win:\nDuration: 1 week.\nPrice: 30 DP.", 42, 6, "Confirm purchase of:\n\n1 week of 40% Extra Rank Points boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_3v3_7:35:35:-22:0|t 30 Extra Rank Points Per Win:\nDuration: 1 week.\nPrice: 40 DP.", 42, 7, "Confirm purchase of:\n\n1 week of 50% Extra Rank Points boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 40, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Holy_DevineAegis:35:35:-22:0|t <- Main Menu.", MAIN_MENU, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|t Close the shop.", 1000, 0);
			player->SEND_GOSSIP_MENU(50022, creature->GetGUID());
		}
		break;
		case 50:
		{
			if (QueryResult result = CharacterDatabase.PQuery("SELECT 1 FROM character_boost WHERE id = %u AND boost_type = 3", player->GetGUIDLow()))
			{
				player->GetSession()->SendAreaTriggerMessage("You already have an active Warsong booster.");
				OnGossipHello(player, creature);
				return false;
			}

			player->PlayerTalkClass->ClearMenus();
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface/ICONS/Ability_Paladin_BlessedHands:35:35:-22:0|tSelect Multiplier:", 50, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 50, 0);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Paladin_BeaconofLight:35:35:-22:0|t 1 Extra Warsong Marks:\nDuration: 1 week.\nPrice: 5 DP.", 52, 1, "Confirm purchase of:\n\n1 week of 1 Extra Warsong Marks boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Paladin_BeaconofLight:35:35:-22:0|t 2 Extra Warsong Marks:\nDuration: 1 week.\nPrice: 10 DP.", 52, 2, "Confirm purchase of:\n\n1 week of 2 Extra Warsong Marks boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Paladin_BeaconofLight:35:35:-22:0|t 3 Extra Warsong Marks:\nDuration: 1 week.\nPrice: 15 DP.", 52, 3, "Confirm purchase of:\n\n1 week of 3 Extra Warsong Marks boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Paladin_BeaconofLight:35:35:-22:0|t 4 Extra Warsong Marks:\nDuration: 1 week.\nPrice: 20 DP.", 52, 4, "Confirm purchase of:\n\n1 week of 4 Extra Warsong Marks boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Paladin_BeaconofLight:35:35:-22:0|t 5 Extra Warsong Marks:\nDuration: 1 week.\nPrice: 25 DP.", 52, 5, "Confirm purchase of:\n\n1 week of 5 Extra Warsong Marks boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Paladin_BeaconofLight:35:35:-22:0|t 6 Extra Warsong Marks:\nDuration: 1 week.\nPrice: 30 DP.", 52, 6, "Confirm purchase of:\n\n1 week of 6 Extra Warsong Marks boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Paladin_BeaconofLight:35:35:-22:0|t 10 Extra Warsong Marks:\nDuration: 1 week.\nPrice: 40 DP.", 52, 10, "Confirm purchase of:\n\n1 week of 10 Extra Warsong Marks boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 50, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Holy_DevineAegis:35:35:-22:0|t <- Main Menu.", MAIN_MENU, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|t Close the shop.", 1000, 0);
			player->SEND_GOSSIP_MENU(50023, creature->GetGUID());
		}
		break;
		case 70:
		{
			if (QueryResult result = CharacterDatabase.PQuery("SELECT 1 FROM character_boost WHERE id = %u AND boost_type = 5", player->GetGUIDLow()))
			{
				player->GetSession()->SendAreaTriggerMessage("You already have an active XP booster.");
				OnGossipHello(player, creature);
				return false;
			}

			player->PlayerTalkClass->ClearMenus();
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface/ICONS/Ability_Paladin_BlessedHands:35:35:-22:0|tSelect Multiplier:", 70, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 70, 0);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Paladin_JudgementofthePure:35:35:-22:0|t 10% Extra Experience:\nDuration: 1 week.\nPrice: 5 DP.", 72, 10, "Confirm purchase of:\n\n1 week of 10% Extra XP boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Paladin_JudgementofthePure:35:35:-22:0|t 15% Extra Experience:\nDuration: 1 week.\nPrice: 10 DP.", 72, 15, "Confirm purchase of:\n\n1 week of 15% Extra XP boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Paladin_JudgementofthePure:35:35:-22:0|t 20% Extra Experience:\nDuration: 1 week.\nPrice: 15 DP.", 72, 20, "Confirm purchase of:\n\n1 week of 20% Extra XP boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Paladin_JudgementofthePure:35:35:-22:0|t 25% Extra Experience:\nDuration: 1 week.\nPrice: 20 DP.", 72, 25, "Confirm purchase of:\n\n1 week of 25% Extra XP boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Paladin_JudgementofthePure:35:35:-22:0|t 30% Extra Experience:\nDuration: 1 week.\nPrice: 25 DP.", 72, 30, "Confirm purchase of:\n\n1 week of 30% Extra XP boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Paladin_JudgementofthePure:35:35:-22:0|t 40% Extra Experience:\nDuration: 1 week.\nPrice: 30 DP.", 72, 40, "Confirm purchase of:\n\n1 week of 40% Extra XP boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Ability_Paladin_JudgementofthePure:35:35:-22:0|t 50% Extra Experience:\nDuration: 1 week.\nPrice: 40 DP.", 72, 50, "Confirm purchase of:\n\n1 week of 50% Extra XP boost.\n\nClick accept to confirm.", 0, false);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 70, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Holy_DevineAegis:35:35:-22:0|t <- Main Menu.", MAIN_MENU, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|t Close the shop.", 1000, 0);
			player->SEND_GOSSIP_MENU(50024, creature->GetGUID());
		}
		break;
		case 80:
		{
			if (QueryResult result = CharacterDatabase.PQuery("SELECT 1 FROM character_boost WHERE id = %u AND boost_type = 7", player->GetGUIDLow()))
			{
				player->GetSession()->SendAreaTriggerMessage("You already have an active arena points booster.");
				OnGossipHello(player, creature);
				return false;
			}
			player->PlayerTalkClass->ClearMenus();
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface/ICONS/Ability_Paladin_BlessedHands:35:35:-22:0|tSelect Multiplier:", 80, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 80, 0);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_5v5_7:35:35:-22:0|t 10% Extra Arena Points:\nDuration: 1 week.\nPrice: 5 DP.", 82, 10, "Confirm purchase of:\n\n1 week of 10% Extra Arena Points boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_5v5_7:35:35:-22:0|t 15% Extra Arena Points:\nDuration: 1 week.\nPrice: 10 DP.", 82, 15, "Confirm purchase of:\n\n1 week of 15% Extra Arena Points boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_5v5_7:35:35:-22:0|t 20% Extra Arena Points:\nDuration: 1 week.\nPrice: 15 DP.", 82, 20, "Confirm purchase of:\n\n1 week of 20% Extra Arena Points boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_5v5_7:35:35:-22:0|t 25% Extra Arena Points:\nDuration: 1 week.\nPrice: 20 DP.", 82, 25, "Confirm purchase of:\n\n1 week of 25% Extra Arena Points boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_5v5_7:35:35:-22:0|t 30% Extra Arena Points:\nDuration: 1 week.\nPrice: 25 DP.", 82, 30, "Confirm purchase of:\n\n1 week of 30% Extra Arena Points boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_5v5_7:35:35:-22:0|t 40% Extra Arena Points:\nDuration: 1 week.\nPrice: 30 DP.", 82, 40, "Confirm purchase of:\n\n1 week of 40% Extra Arena Points boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Arena_5v5_7:35:35:-22:0|t 50% Extra Arena Points:\nDuration: 1 week.\nPrice: 40 DP.", 82, 50, "Confirm purchase of:\n\n1 week of 50% Extra Arena Points boost.\n\nClick accept to confirm..", 0, false);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", 80, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Holy_DevineAegis:35:35:-22:0|t <- Main Menu.", MAIN_MENU, 0);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|t Close the shop.", 1000, 0);
			player->SEND_GOSSIP_MENU(50025, creature->GetGUID());
		}
		break;
		case 1003:
			switch(sender)
			{
		case 2:
			if (player->GetSession()->GetVP() < 5)
			{
				player->GetSession()->SendAreaTriggerMessage("You do not have enough VP for this purchase.\nPrice is: 5 - You have: %u", player->GetSession()->GetVP());
				OnGossipHello(player, creature);
				return false;
			}
			player->GetSession()->ModifyVP(-5);
			player->AddItem(44115, 1);
			player->SaveToDB();
			player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			break;
		case 3:
			if (player->GetSession()->GetVP() < 10)
			{
				player->GetSession()->SendAreaTriggerMessage("You do not have enough VP for this purchase.\nPrice is: 10 - You have: %u", player->GetSession()->GetVP());
				OnGossipHello(player, creature);
				return false;
			}
			player->GetSession()->ModifyVP(-10);
			player->AddItem(31405, 1);
			player->SaveToDB();
			player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			break;
		case 4:
			if (player->GetSession()->GetVP() < 10)
			{
				player->GetSession()->SendAreaTriggerMessage("You do not have enough VP for this purchase.\nPrice is: 10 - You have: %u", player->GetSession()->GetVP());
				OnGossipHello(player, creature);
				return false;
			}
			player->GetSession()->ModifyVP(-10);
			player->AddItem(31404, 1);
			player->SaveToDB();
			player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			break;
		case 5:
			if (player->GetSession()->GetVP() < 15)
			{
				player->GetSession()->SendAreaTriggerMessage("You do not have enough VP for this purchase.\nPrice is: 15 - You have: %u", player->GetSession()->GetVP());
				OnGossipHello(player, creature);
				return false;
			}
			player->GetSession()->ModifyVP(-15);
			player->AddItem(35221, 1);
			player->SaveToDB();
			player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			break;
		case 6:
			if (player->GetSession()->GetVP() < 25)
			{
				player->GetSession()->SendAreaTriggerMessage("You do not have enough VP for this purchase.\nPrice is: 25 - You have: %u", player->GetSession()->GetVP());
				OnGossipHello(player, creature);
				return false;
			}
			player->GetSession()->ModifyVP(-25);
			player->AddItem(37254, 1);
			player->SaveToDB();
			player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			break;
		case 7:
			if (player->GetSession()->GetVP() < 30)
			{
				player->GetSession()->SendAreaTriggerMessage("You do not have enough VP for this purchase.\nPrice is: 30 - You have: %u", player->GetSession()->GetVP());
				OnGossipHello(player, creature);
				return false;
			}
			player->GetSession()->ModifyVP(-30);
			player->AddItem(47179, 1);
			player->SaveToDB();
			player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			break;
			}
			OnGossipHello(player, creature);
			break;
		case 1002:
			switch (action)
			{
			case 1:
			{
				if (player->GetSession()->GetDP() < 2)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 2 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}

				player->GetSession()->ModifyDP(-2);
				player->AddItem(14964, 1);
				player->SaveToDB();
				ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(14964);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), pProto->Name1.c_str(), pProto->ItemId, 2, 1);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
			break;
			case 2:
			{
				if (player->GetSession()->GetDP() < 5)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 5 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}

				player->GetSession()->ModifyDP(-5);
				player->AddItem(38311, 1);
				player->SaveToDB();
				ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(38311);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), pProto->Name1.c_str(), pProto->ItemId, 5, 1);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
			break;
			case 3:
			{
				if (player->GetSession()->GetDP() < 5)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 5 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}

				player->GetSession()->ModifyDP(-5);
				player->AddItem(38309, 1);
				player->SaveToDB();
				ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(38309);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), pProto->Name1.c_str(), pProto->ItemId, 5, 1);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
			break;
			case 4:
			{
				if (player->GetSession()->GetDP() < 5)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 5 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}

				player->GetSession()->ModifyDP(-5);
				player->AddItem(23709, 1);
				player->SaveToDB();
				ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(23709);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), pProto->Name1.c_str(), pProto->ItemId, 5, 1);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
			break;
			case 5:
			{
				if (player->GetSession()->GetDP() < 5)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 5 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}

				player->GetSession()->ModifyDP(-5);
				player->AddItem(38312, 1);
				player->SaveToDB();
				ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(38312);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), pProto->Name1.c_str(), pProto->ItemId, 5, 1);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
			break;
			case 6:
			{
				if (player->GetSession()->GetDP() < 5)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 5 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}

				player->GetSession()->ModifyDP(-5);
				player->AddItem(38310, 1);
				player->SaveToDB();
				ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(38310);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), pProto->Name1.c_str(), pProto->ItemId, 5, 1);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
			break;
			case 7:
			{
				if (player->GetSession()->GetDP() < 5)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 5 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}

				player->GetSession()->ModifyDP(-5);
				player->AddItem(38313, 1);
				player->SaveToDB();
				ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(38313);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), pProto->Name1.c_str(), pProto->ItemId, 5, 1);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
			break;
			case 8:
			{
				if (player->GetSession()->GetDP() < 5)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 5 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}

				player->GetSession()->ModifyDP(-5);
				player->AddItem(23705, 1);
				player->SaveToDB();
				ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(23705);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), pProto->Name1.c_str(), pProto->ItemId, 5, 1);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
			break;
			case 9:
			{
				if (player->GetSession()->GetDP() < 5)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 5 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}

				player->GetSession()->ModifyDP(-5);
				player->AddItem(38314, 1);
				player->SaveToDB();
				ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(38314);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), pProto->Name1.c_str(), pProto->ItemId, 5, 1);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
			break;
			case 10:
			{
				if (player->GetSession()->GetDP() < 5)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 5 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}

				player->GetSession()->ModifyDP(-5);
				player->AddItem(8630, 1);
				player->SaveToDB();
				ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(8630);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), pProto->Name1.c_str(), pProto->ItemId, 5, 1);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
			break;
			case 11:
			{
				if (player->GetSession()->GetDP() < 5)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 5 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}
				player->GetSession()->ModifyDP(-5);
				player->AddItem(46109, 1);
				player->SaveToDB();
				ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(46109);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), pProto->Name1.c_str(), pProto->ItemId, 5, 1);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
			break;
			case 12:
			{
				if (player->GetSession()->GetDP() < 10)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 10 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}
				player->GetSession()->ModifyDP(-10);
				player->AddItem(99908, 1);
				player->SaveToDB();
				ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(99908);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), pProto->Name1.c_str(), pProto->ItemId, 10, 1);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
			break;
			case 13:
			{
				if (player->GetSession()->GetDP() < 10)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 10 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}
				player->GetSession()->ModifyDP(-10);
				player->AddItem(52200, 1);
				player->SaveToDB();
				ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(52200);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), pProto->Name1.c_str(), pProto->ItemId, 10, 1);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
			break;
			case 14:
			{
				if (player->GetSession()->GetDP() < 15)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 15 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}
				player->GetSession()->ModifyDP(-15);
				player->AddItem(21176, 1);
				player->SaveToDB();
				ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(21176);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), pProto->Name1.c_str(), pProto->ItemId, 15, 1);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
			break;
			case 15:
			{

				if (player->GetSession()->GetDP() < 4)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 4 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}
				player->GetSession()->ModifyDP(-4);
				player->AddItem(20880, 1);
				player->SaveToDB();
				ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(20880);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), pProto->Name1.c_str(), pProto->ItemId, 5, 1);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
			break;
			case 150:
			{

				if (player->GetSession()->GetDP() < 35)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 35 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}
				player->GetSession()->ModifyDP(-35);
				player->AddItem(20880, 13);
				player->SaveToDB();
				ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(20880);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), pProto->Name1.c_str(), pProto->ItemId, 5, 13);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
			break;
			case 16:
			{

				if (player->GetSession()->GetDP() < 10)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 10 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}
				Item* main = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);
				if (!main)
				{
					player->GetSession()->SendAreaTriggerMessage("You need to have a main hand equipped.");
					OnGossipHello(player, creature);
					return false;
				}

				player->GetSession()->ModifyDP(-10);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), "Thunderfury Main-Hand Transmog", 0, 10, 1);
				CharacterDatabase.PExecute("delete from transmog_force_item where item1 = (select itemEntry from item_instance where guid = (select item from character_inventory where guid = %u and slot = 15 and bag = 0))", player->GetGUIDLow());
				CharacterDatabase.PExecute("insert into custom_transmogrification values((select guid from item_instance where guid = (select item from character_inventory where guid = %u and slot = 15 and bag = 0)), 19019, %u) ", player->GetGUIDLow(), player->GetGUIDLow());
				player->transmogMap[main->GetGUID()] = 19019;
				player->SetVisibleItemSlot(EQUIPMENT_SLOT_MAINHAND, main);
				main->SendUpdateToPlayer(player);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
			break;
			case 17:
			{

				if (player->GetSession()->GetDP() < 10)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 10 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}
				Item* off = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);
				if (!off)
				{
					player->GetSession()->SendAreaTriggerMessage("You need to have a main hand equipped.");
					OnGossipHello(player, creature);
					return false;
				}
				player->GetSession()->ModifyDP(-10);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), "Thunderfury Off-Hand Transmog", 0, 10, 1);
				CharacterDatabase.PExecute("delete from custom_transmogrification where GUID = (select guid from item_instance where guid = (select item from character_inventory where guid = %u and slot = 16 and bag = 0))", player->GetGUIDLow());
				CharacterDatabase.PExecute("insert into custom_transmogrification values ((select guid from item_instance where guid = (select item from character_inventory where guid = %u and slot = 16 and bag = 0)), 19019, %u) ", player->GetGUIDLow(), player->GetGUIDLow());
				player->transmogMap[off->GetGUID()] = 19019;
				player->SetVisibleItemSlot(EQUIPMENT_SLOT_OFFHAND, off);
				off->SendUpdateToPlayer(player);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
			break;
			case 18:
			{
				if (player->GetSession()->GetDP() < 10)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 10 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}
				Item* off = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);
				if (!off)
				{
					player->GetSession()->SendAreaTriggerMessage("You need to have a main hand equipped.");
					OnGossipHello(player, creature);
					return false;
				}

				player->GetSession()->ModifyDP(-10);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), "Warglaive Left Transmog", 0, 30, 1);
				CharacterDatabase.PExecute("delete from transmog_force_item where item1 = (select itemEntry from item_instance where guid = (select item from character_inventory where guid = %u and slot = 15 and bag = 0))", player->GetGUIDLow());
				CharacterDatabase.PExecute("insert into transmog_force_item values ((select itemEntry from item_instance where guid = (select item from character_inventory where guid = %u and slot = 15 and bag = 0)), 18584, %u) ", player->GetGUIDLow(), player->GetGUIDLow());
				player->transmogMap[off->GetGUID()] = 18584;
				player->SetVisibleItemSlot(EQUIPMENT_SLOT_MAINHAND, off);
				off->SendUpdateToPlayer(player);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
				break;
			case 19:
			{
				if (player->GetSession()->GetDP() < 10)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 10 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}
				Item* off = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);
				if (!off)
				{
					player->GetSession()->SendAreaTriggerMessage("You need to have a main hand equipped.");
					OnGossipHello(player, creature);
					return false;
				}
				player->GetSession()->ModifyDP(-10);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), "Thorims Mace Transmog", 0, 10, 1);
				CharacterDatabase.PExecute("delete from transmog_force_item where item1 = (select itemEntry from item_instance where guid = (select item from character_inventory where guid = %u and slot = 15 and bag = 0))", player->GetGUIDLow());
				CharacterDatabase.PExecute("insert into transmog_force_item values ((select itemEntry from item_instance where guid = (select item from character_inventory where guid = %u and slot = 15 and bag = 0)), 45900, %u) ", player->GetGUIDLow(), player->GetGUIDLow());
				player->transmogMap[off->GetGUID()] = 45900;
				player->SetVisibleItemSlot(EQUIPMENT_SLOT_MAINHAND, off);
				off->SendUpdateToPlayer(player);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
				break;
			case 20:
			{

				if (player->GetSession()->GetDP() < 15)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 15 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}
				Item* off = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);
				if (!off)
				{
					player->GetSession()->SendAreaTriggerMessage("You need to have a main hand equipped.");
					OnGossipHello(player, creature);
					return false;
				}
				player->GetSession()->ModifyDP(-15);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), "Corrupted Ashbringer Transmog", 0, 15, 1);
				CharacterDatabase.PExecute("delete from transmog_force_item where item1 = (select itemEntry from item_instance where guid = (select item from character_inventory where guid = %u and slot = 15 and bag = 0))", player->GetGUIDLow());
				CharacterDatabase.PExecute("insert into transmog_force_item values ((select itemEntry from item_instance where guid = (select item from character_inventory where guid = %u and slot = 15 and bag = 0)), 22691, %u) ", player->GetGUIDLow(), player->GetGUIDLow());
				player->transmogMap[off->GetGUID()] = 22691;
				player->SetVisibleItemSlot(EQUIPMENT_SLOT_MAINHAND, off);
				off->SendUpdateToPlayer(player);
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
			break;
			case 21:
			{

				if (player->GetSession()->GetDP() < 25)
				{
					player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 25 - You have: %u", player->GetSession()->GetDP());
					OnGossipHello(player, creature);
					return false;
				}
				player->GetSession()->ModifyDP(-25);
				WorldDatabase.PExecute("INSERT INTO `check_donation_purchases` (`charName`, `accId`, `itemName`, `itemId`, `price`, `amount`) VALUES ('%s', '%u', \"%s\", '%u', '%u', '%u')",
					player->GetName().c_str(), player->GetSession()->GetAccountId(), "Reputation Max", 0, 25, 1);
				CharacterDatabase.PExecute("UPDATE character_reputation SET standing = 52999 WHERE guid = %u AND faction = 70", player->GetGUIDLow());
				CharacterDatabase.PExecute("UPDATE character_reputation SET standing = 52999 WHERE guid = %u AND faction = 349", player->GetGUIDLow());
				player->SaveToDB();
				player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
			}
			break;
			}
			player->PlayerTalkClass->SendCloseGossip();
			break;
						case 32: // honor
						{

							int32 price = action > 30 ? action - 10 : action - 5;
							float modifier = (action / 100.0f) + 1.0f;
							if (player->GetSession()->GetDP() < uint32(price))
							{
								player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: %i - You have: %u", price, player->GetSession()->GetDP());
								OnGossipHello(player, creature);
								return false;
							}

							player->GetSession()->ModifyDP(-price);
							CharacterDatabase.PExecute("INSERT INTO character_boost (id, boost_type, unsetdate, boost_modifier, startdate) VALUES (%u, '1', UNIX_TIMESTAMP()+604800, '%f', UNIX_TIMESTAMP())", player->GetGUIDLow(), modifier);
							player->GetSession()->SendAreaTriggerMessage("Congratulations!\nRelog to activate your booster. Enjoy your gaming!");
							
							OnGossipHello(player, creature);
						}
						break;
						case 52: // reputation
						{

							int32 price = 0;
							switch (action)
							{
							case 1:
								price = 5;
								break;
							case 2:
								price = 10;
								break;
							case 3:
								price = 15;
								break;
							case 4:
								price = 20;
								break;
							case 5:
								price = 25;
								break;
							case 6:
								price = 30;
								break;
							case 10:
								price = 40;
								break;
							default:
								price = 10;
								break;
							}
							float modifier = action;
							if (player->GetSession()->GetDP() < uint32(price))
							{
								player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: %i - You have: %u", price, player->GetSession()->GetDP());
								OnGossipHello(player, creature);
								return false;
							}

							player->GetSession()->ModifyDP(-price);
							CharacterDatabase.PExecute("INSERT INTO character_boost (id, boost_type, unsetdate, boost_modifier, startdate) VALUES (%u, '3', UNIX_TIMESTAMP()+604800, '%f', UNIX_TIMESTAMP())", player->GetGUIDLow(), modifier);
							player->GetSession()->SendAreaTriggerMessage("Congratulations!\nRelog to activate your booster. Enjoy your gaming!");
							
							OnGossipHello(player, creature);
						}
						break;
						case 82: // arena points
						{
							int32 price = action > 30 ? action - 10 : action - 5;
							float modifier = (action / 100.0f) + 1.0f;
							if (player->GetSession()->GetDP() < uint32(price))
							{
								player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: %i - You have: %u", price, player->GetSession()->GetDP());
								OnGossipHello(player, creature);
								return false;
							}

							player->GetSession()->ModifyDP(-price);
							CharacterDatabase.PExecute("INSERT INTO character_boost (id, boost_type, unsetdate, boost_modifier, startdate) VALUES (%u, '7', UNIX_TIMESTAMP()+604800, '%f', UNIX_TIMESTAMP())", player->GetGUIDLow(), modifier);
							player->GetSession()->SendAreaTriggerMessage("Congratulations!\nRelog to activate your booster. Enjoy your gaming!");
							
							OnGossipHello(player, creature);
						}
						break;
						

						case 7: // change race
						{


							if (player->GetSession()->GetDP() < 5)
							{
								player->GetSession()->SendAreaTriggerMessage("Your do not have enough DP for this purchase.\nPrice is: 5 - You have: %u", player->GetSession()->GetDP());
								OnGossipHello(player, creature);
								return false;
							}

							PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_ADD_AT_LOGIN_FLAG);
							stmt->setUInt16(0, uint16(AT_LOGIN_CHANGE_RACE));
							if (player)
							{
								ChatHandler(player->GetSession()).PSendSysMessage(LANG_CUSTOMIZE_PLAYER, ChatHandler(player->GetSession()).GetNameLink(player).c_str());
								player->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
								stmt->setUInt32(1, player->GetGUIDLow());
							}
							CharacterDatabase.Execute(stmt);
							player->GetSession()->ModifyDP(-5);
							player->GetSession()->SendAreaTriggerMessage("Congratulations!\nYour purchase has been completed. Please relog to update your character.");
							OnGossipHello(player, creature);
						}
						break;
						case 8: // change customize
						{


							if (player->GetSession()->GetVP() < 80)
							{
								player->GetSession()->SendAreaTriggerMessage("You do not have enough VP for this purchase.\nPrice is: 80 - You have: %u", player->GetSession()->GetVP());
								OnGossipHello(player, creature);
								return false;
							}

							PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_ADD_AT_LOGIN_FLAG);
							stmt->setUInt16(0, uint16(AT_LOGIN_CUSTOMIZE));
							if (player)
							{
								ChatHandler(player->GetSession()).PSendSysMessage(LANG_CUSTOMIZE_PLAYER, ChatHandler(player->GetSession()).GetNameLink(player).c_str());
								player->SetAtLoginFlag(AT_LOGIN_CUSTOMIZE);
								stmt->setUInt32(1, player->GetGUIDLow());
							}
							CharacterDatabase.Execute(stmt);
							player->GetSession()->ModifyVP(-80);
							player->GetSession()->SendAreaTriggerMessage("Congratulations!\nYou purchase has been completed. Please relog to update your character.");
							OnGossipHello(player, creature);
						}
						break;
						case 111: // change faction
						{


							if (player->GetSession()->GetVP() < 120)
							{
								player->GetSession()->SendAreaTriggerMessage("You do not have enough VP for this purchase.\nPrice is: 120 - You have: %u", player->GetSession()->GetVP());
								OnGossipHello(player, creature);
								return false;
							}

							PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_ADD_AT_LOGIN_FLAG);
							stmt->setUInt16(0, uint16(AT_LOGIN_CHANGE_FACTION));
							if (player)
							{
								ChatHandler(player->GetSession()).PSendSysMessage(LANG_CUSTOMIZE_PLAYER, ChatHandler(player->GetSession()).GetNameLink(player).c_str());
								player->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
								stmt->setUInt32(1, player->GetGUIDLow());
							}
							CharacterDatabase.Execute(stmt);
							player->GetSession()->ModifyVP(-120);
							player->GetSession()->SendAreaTriggerMessage("Congratulations!\nYour purchase has been completed. Please relog to update your character.");
							OnGossipHello(player, creature);
						}
						break;
						case 121: // change race
						{


							if (player->GetSession()->GetVP() < 120)
							{
								player->GetSession()->SendAreaTriggerMessage("Your do not have enough VP for this purchase.\nPrice is: 120 - You have: %u", player->GetSession()->GetVP());
								OnGossipHello(player, creature);
								return false;
							}

							PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_ADD_AT_LOGIN_FLAG);
							stmt->setUInt16(0, uint16(AT_LOGIN_CHANGE_RACE));
							if (player)
							{
								ChatHandler(player->GetSession()).PSendSysMessage(LANG_CUSTOMIZE_PLAYER, ChatHandler(player->GetSession()).GetNameLink(player).c_str());
								player->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
								stmt->setUInt32(1, player->GetGUIDLow());
							}
							CharacterDatabase.Execute(stmt);
							player->GetSession()->ModifyVP(-120);
							player->GetSession()->SendAreaTriggerMessage("Congratulations!\nYour purchase has been completed. Please relog to update your character.");
							OnGossipHello(player, creature);
						}
						break;
						case 131: // change customize
						{


							if (player->GetSession()->GetVP() < 120)
							{
								player->GetSession()->SendAreaTriggerMessage("You do not have enough VP for this purchase.\nPrice is: 120 - You have: %u", player->GetSession()->GetVP());
								OnGossipHello(player, creature);
								return false;
							}

							PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_ADD_AT_LOGIN_FLAG);
							stmt->setUInt16(0, uint16(AT_LOGIN_CUSTOMIZE));
							if (player)
							{
								ChatHandler(player->GetSession()).PSendSysMessage(LANG_CUSTOMIZE_PLAYER, ChatHandler(player->GetSession()).GetNameLink(player).c_str());
								player->SetAtLoginFlag(AT_LOGIN_CUSTOMIZE);
								stmt->setUInt32(1, player->GetGUIDLow());
							}
							CharacterDatabase.Execute(stmt);
							player->GetSession()->ModifyVP(-120);
							player->GetSession()->SendAreaTriggerMessage("Congratulations!\nYou purchase has been completed. Please relog to update your character.");
							OnGossipHello(player, creature);
						}
						break;
						case 12:
						{


							if (player->HasSpell(32368))
							{
								player->GetSession()->SendAreaTriggerMessage("You already have this visual.");
								OnGossipHello(player, creature);
								return false;
							}

							if (player->GetSession()->GetDP() < 5)
							{
								player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 5 - You have: %u", player->GetSession()->GetDP());
								OnGossipHello(player, creature);
								return false;
							}

							player->LearnSpell(32368, false);
							player->GetSession()->ModifyDP(-5);
							player->GetSession()->SendAreaTriggerMessage("Congratulations!\nYou purchase has been completed. You can locate your new visual in your spell book.");
							OnGossipHello(player, creature);
						}
						break;
						case 13:
						{


							if (player->HasSpell(72523))
							{
								player->GetSession()->SendAreaTriggerMessage("You already have this visual.");
								OnGossipHello(player, creature);
								return false;
							}

							if (player->GetSession()->GetDP() < 10)
							{
								player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 10 - You have: %u", player->GetSession()->GetDP());
								OnGossipHello(player, creature);
								return false;
							}

							player->LearnSpell(72523, false);
							player->GetSession()->ModifyDP(-10);
							player->GetSession()->SendAreaTriggerMessage("Congratulations!\nYou purchase has been completed. You can locate your new visual in your spell book.");
							OnGossipHello(player, creature);
						}
						break;

						case 91:
						{


							if (player->HasSpell(72521))
							{
								player->GetSession()->SendAreaTriggerMessage("You already have this visual.");
								OnGossipHello(player, creature);
								return false;
							}

							if (player->GetSession()->GetVP() < 120)
							{
								player->GetSession()->SendAreaTriggerMessage("You do not have enough VP for this purchase.\nPrice is: 120 - You have: %u", player->GetSession()->GetVP());
								OnGossipHello(player, creature);
								return false;
							}

							player->LearnSpell(72521, false);
							player->GetSession()->ModifyVP(-120);
							player->GetSession()->SendAreaTriggerMessage("Congratulations!\nYou purchase has been completed. You can locate your new visual in your spell book.");
							OnGossipHello(player, creature);
						}
						break;
						case 92:
						{

							if (player->HasSpell(72523))
							{
								player->GetSession()->SendAreaTriggerMessage("You already have this visual.");
								OnGossipHello(player, creature);
								return false;
							}
							if (player->GetSession()->GetVP() < 180)
							{
								player->GetSession()->SendAreaTriggerMessage("You do not have enough VP for this purchase.\nPrice is: 180 - You have: %u", player->GetSession()->GetVP());
								OnGossipHello(player, creature);
								return false;
							}

							player->LearnSpell(72523, false);
							player->GetSession()->ModifyVP(-180);
							player->GetSession()->SendAreaTriggerMessage("Congratulations!\nYou purchase has been completed. You can locate your new visual in your spell book.");
							OnGossipHello(player, creature);
						}
						break;
						case 93:
						{


							if (player->HasSpell(56075))
							{
								player->GetSession()->SendAreaTriggerMessage("You already have this visual.");
								OnGossipHello(player, creature);
								return false;
							}

							if (player->GetSession()->GetVP() < 120)
							{
								player->GetSession()->SendAreaTriggerMessage("You do not have enough VP for this purchase.\nPrice is: 120 - You have: %u", player->GetSession()->GetVP());
								OnGossipHello(player, creature);
								return false;
							}

							player->LearnSpell(56075, false);
							player->GetSession()->ModifyVP(-120);
							player->GetSession()->SendAreaTriggerMessage("Congratulations!\nYou purchase has been completed. You can locate your new visual in your spell book.");
							OnGossipHello(player, creature);
						}
						break;
						case 94:
						{


							if (player->HasSpell(40158))
							{
								player->GetSession()->SendAreaTriggerMessage("You already have this visual.");
								OnGossipHello(player, creature);
								return false;
							}

							if (player->GetSession()->GetVP() < 110)
							{
								player->GetSession()->SendAreaTriggerMessage("You do not have enough VP for this purchase.\nPrice is: 110 - You have: %u", player->GetSession()->GetVP());
								OnGossipHello(player, creature);
								return false;
							}

							player->LearnSpell(40158, false);
							player->GetSession()->ModifyVP(-110);
							player->GetSession()->SendAreaTriggerMessage("Congratulations!\nYou purchase has been completed. You can locate your new visual in your spell book.");
							OnGossipHello(player, creature);
						}
						break;
						case 95:
						{


							if (player->HasSpell(32368))
							{
								player->GetSession()->SendAreaTriggerMessage("You already have this visual.");
								OnGossipHello(player, creature);
								return false;
							}

							if (player->GetSession()->GetVP() < 130)
							{
								player->GetSession()->SendAreaTriggerMessage("You do not have enough VP for this purchase.\nPrice is: 130 - You have: %u", player->GetSession()->GetVP());
								OnGossipHello(player, creature);
								return false;
							}

							player->LearnSpell(32368, false);
							player->GetSession()->ModifyVP(-130);
							player->GetSession()->SendAreaTriggerMessage("Congratulations!\nYou purchase has been completed. You can locate your new visual in your spell book.");
							OnGossipHello(player, creature);
						}
						break;
						case 22: // gold
						{


							int32 price = action > 30 ? action - 10 : action - 5;
							float modifier = (action / 100.0f) + 1.0f;
							if (player->GetSession()->GetDP() < uint32(price))
							{
								player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: %i - You have: %u", price, player->GetSession()->GetDP());
								OnGossipHello(player, creature);
								return false;
							}

							player->GetSession()->ModifyDP(-price);
							CharacterDatabase.PExecute("REPLACE INTO character_boost (id, boost_type, unsetdate, boost_modifier, startdate) VALUES (%u, '0', UNIX_TIMESTAMP()+604800, '%f', UNIX_TIMESTAMP())", player->GetGUIDLow(), modifier);
							player->GetSession()->SendAreaTriggerMessage("Congratulations!\nRelog to activate your booster. Please restart WoW to update your account.");
							OnGossipHello(player, creature);
						}
						break;
						case 42: // rankpoints
						{
							int32 price = 0;
							float modifier = 0;
							switch (action)
							{
							case 1:
								price = 5;
								modifier = 2;
								break;
							case 2:
								price = 10;
								modifier = 4;
								break;
							case 3:
								price = 15;
								modifier = 7;
								break;
							case 4:
								price = 20;
								modifier = 10;
								break;
							case 5:
								price = 25;
								modifier = 15;
								break;
							case 6:
								price = 30;
								modifier = 20;
								break;
							case 10:
								price = 40;
								modifier = 30;
								break;
							default:
								price = 10;
								break;
							}
							if (player->GetSession()->GetDP() < uint32(price))
							{
								player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: %i - You have: %u", price, player->GetSession()->GetDP());
								OnGossipHello(player, creature);
								return false;
							}

							player->GetSession()->ModifyDP(-price);
							CharacterDatabase.PExecute("INSERT INTO character_boost (id, boost_type, unsetdate, boost_modifier, startdate) VALUES (%u, '2', UNIX_TIMESTAMP()+604800, '%f', UNIX_TIMESTAMP())", player->GetGUIDLow(), modifier);
							player->GetSession()->SendAreaTriggerMessage("Congratulations!\nRelog to activate your booster. Please restart WoW to update your account.");
							OnGossipHello(player, creature);
						}
						break;
						case 72: // xp
						{


							int32 price = action > 30 ? action - 10 : action - 5;
							float modifier = (action / 100.0f) + 1.0f;
							if (player->GetSession()->GetDP() < uint32(price))
							{
								player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: %i - You have: %u", price, player->GetSession()->GetDP());
								OnGossipHello(player, creature);
								return false;
							}

							player->GetSession()->ModifyDP(-price);
							CharacterDatabase.PExecute("INSERT INTO character_boost (id, boost_type, unsetdate, boost_modifier, startdate) VALUES (%u, '5', UNIX_TIMESTAMP()+604800, '%f', UNIX_TIMESTAMP())", player->GetGUIDLow(), modifier);
							player->GetSession()->SendAreaTriggerMessage("Congratulations!\nRelog to activate your booster. Please restart WoW to update your account.");
							OnGossipHello(player, creature);
						}
						break;
						case 100:
						{


							int32 price = action == 10 ? 150 : ((5 * action) + 15);
							if (player->GetSession()->GetDP() < price)
							{
								player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: %i - You have: %u", price, player->GetSession()->GetDP());
								OnGossipHello(player, creature);
								return false;
							}

							player->GetSession()->ModifyDP(-price);
							if (action != 10)
								LoginDatabase.PExecute("REPLACE INTO account_premium (id, setdate, unsetdate, premium_type, active) VALUES (%u, UNIX_TIMESTAMP(), UNIX_TIMESTAMP()+%u, 1, 1)", player->GetSession()->GetAccountId(), WEEK * action);
							else
								LoginDatabase.PExecute("REPLACE INTO account_premium (id, setdate, unsetdate, premium_type, active) VALUES (%u, 0, 0, 1, 1)", player->GetSession()->GetAccountId());
							player->GetSession()->SendAreaTriggerMessage("Congratulations!\nYour premium membership is activated. You will be kicked so your VIP rank will update..");
							player->PlayerTalkClass->SendCloseGossip();
							player->m_Events.AddEvent(new kick_player_delay_event(player), player->m_Events.CalculateTime(3000));
						}
						break;
						case 151:
						{

							if (player->GetSession()->GetDP() < 10)
							{
								player->GetSession()->SendAreaTriggerMessage("You do not have enough DP for this purchase.\nPrice is: 10 - You have: %u", player->GetSession()->GetDP());
								OnGossipHello(player, creature);
								return false;
							}

							player->GetSession()->ModifyDP(-10);
							player->GetSession()->SetExtraRaces(action, player->GetSession()->GetAccountId());
							creature->Talk("You have unlocked a new Race to play. Congratulations!", CHAT_MSG_MONSTER_YELL, LANG_UNIVERSAL, 20.0f, player);
							OnGossipHello(player, creature);
						}
						break;
		default:
			break;
		}
		return true;
	}

	bool OnGossipSelectCode(Player *player, Creature* creature, uint32 sender, uint32 action, const char * code)
	{
		player->PlayerTalkClass->ClearMenus();

		std::string codeStr = code;
		switch (sender)
		{
		case 801:
		{
			uint32 item = atoi(code);
			if (!item || item < 0)
			{
				player->GetSession()->SendNotification("Please enter a correct item id.");
				OnGossipHello(player, creature);
				return false;
			}
			ItemEntry const* dbcitem = sItemStore.LookupEntry(item);
			if (!dbcitem)
			{
				player->GetSession()->SendNotification("Item Entry not found in Item.dbc.\nPlease re-check your ID.");
				OnGossipHello(player, creature);
				return false;
			}
			if (player->GetItemCount(20880) < 1)
			{
				player->GetSession()->SendNotification("You do not have any Transmog Tokens!");
				OnGossipHello(player, creature);
				return false;
			}
			ItemTemplate const* newitem = sObjectMgr->GetItemTemplate(item);
			Item* olditem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, action);
			if (!olditem || !newitem)
			{
				player->GetSession()->SendNotification("You do not have an item equipped!");
				OnGossipHello(player, creature);
				return false;
			}

			if (newitem->Class != olditem->GetTemplate()->Class)
			{
				player->GetSession()->SendNotification("The selected item and new item do not match.");
				OnGossipHello(player, creature);
				return false;
			}

			player->DestroyItemCount(20880, 1, true);
			CharacterDatabase.PExecute("REPLACE INTO transmog_force_item_character (`charid`, `item1`, `item2`, `comment`) VALUES ('%u', '%u', '%u', '%s')", player->GetGUIDLow(), olditem->GetEntry(), item, player->GetName().c_str());
			player->PlayerTalkClass->SendCloseGossip();
			player->SetVisibleItemSlot(action, olditem);
			player->GetSession()->SendNotification("Success!\nYour item has been transmogged.\nRelog to see the effects!");
		}
			break;
		case 1003:
		{
			int32 amount = atol(code);
			if (!amount || amount < 0)
			{
				player->GetSession()->SendAreaTriggerMessage("Please enter a real amount.");
				OnGossipHello(player, creature);
				return false;
			}
			if (player->GetSession()->GetVP() < amount)
			{
				player->GetSession()->SendAreaTriggerMessage("You do not have enough VP for this purchase.\nPrice is: %i - You have: %u", amount, player->GetSession()->GetVP());
				OnGossipHello(player, creature);
				return false;
			}
			player->GetSession()->ModifyVP(-amount);
			player->AddItem(20558, amount * 6);
			player->SaveToDB();
			player->GetSession()->SendNotification("Purchase completed.\nYour character has been saved.\nHave a fantastic day!");
		}
		break;
		default:
			break;
		}

		return true;
	}

private:

	enum Senders
	{
		MAIN_MENU = GOSSIP_ACTION_INFO_DEF + 1,
		SELECT_ITEM,
		SELECT_STAT_INCREASE,
		SELECT_TOKEN_COST,
		VENDOR = GOSSIP_ACTION_INFO_DEF + 2,
	};
};

void AddSC_DonorNPC()
{
	new DonorNPC();
}