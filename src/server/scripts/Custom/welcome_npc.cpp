enum defines
{
    SPELL_ROOT = 40885,
};

class welcome_player : public PlayerScript
{
public:
    welcome_player() : PlayerScript("welcome_player") { }

    class welcome_player_class : public BasicEvent
    {
    public:
        welcome_player_class(Player* player) : player(player) {}

        bool Execute(uint64 /*time*/, uint32 /*diff*/)
        {
            player->m_introduction = 1;
            player->SetPhaseMask(64, true);
            player->SummonCreature(9400000, -326.201080f, 1455.312500f, 30.832047f, 1.616960f, TEMPSUMMON_MANUAL_DESPAWN);
            return true;
        }

        Player* player;
    };

    void OnLogin(Player* player, bool f)
    {
        //if (player->m_introduction != 1)
        //{
        if (player->GetGUIDLow() == 9000000000)
        {
            player->TeleportTo(0, -327.559174f, 1471.447266f, 31.772167f, 4.691989f);
            player->AddAura(SPELL_ROOT, player);
            player->m_Events.AddEvent(new welcome_player_class(player), player->m_Events.CalculateTime(5000));
        }
        //}
    }

    void OnLogout(Player* player)
    {
        if (player->m_introduction == 0)
        {
            player->RemoveAura(SPELL_ROOT);
        }
    }
};

class welcome_creature : public CreatureScript
{
public:
    welcome_creature() : CreatureScript("welcome_creature") { }
    
    struct welcome_npcAI : public ScriptedAI
    {
        welcome_npcAI(Creature * c) : ScriptedAI(c){}

        Player* player;

        uint32 ini_timer = 100;
        bool init = false;

        uint32 walk_timer1 = 2000;
        bool walk1 = false;

        uint32 walk_timer2 = 8000;
        bool walk2 = false;

        uint32 walk_timer3 = 12850;
        bool walk3 = false;

        uint32 walk_timer4 = 14850;
        bool walk4 = false;

        uint32 walk_timer5 = 18350;
        bool walk5 = false;

        uint32 walk_timer6 = 20350;
        bool walk6 = false;

        uint32 walk_timer7 = 22350;
        bool walk7 = false;

        uint32 finish_timer = 25000;
        bool c_finished = false;

        void UpdateAI(uint32 diff)
        {
            if (ini_timer < diff && !init)
            {
                if (me->ToTempSummon())
                {
                    if (me->ToTempSummon()->GetSummoner())
                    {
                        player = me->ToTempSummon()->GetSummoner()->ToPlayer();
                    }
                }
                init = true;
            }
            else
                ini_timer -= diff;

            if (walk_timer1 < diff && !walk1)
            {
                me->Say("I'm happy that you've decided to meet up with me.", LANG_UNIVERSAL);
                me->SetWalk(true);
                me->GetMotionMaster()->MovePoint(0, -326.975861f, 1465.428345f, 31.774616f);
                walk1 = true;
            }
            else
                walk_timer1 -= diff;

            if (walk_timer2 < diff && !walk2)
            {
                me->Say("My name is Jr. Alfred Copperworth, Son of Alfred the 1st. My dad used to do all the dirty work around here, but after he passed away to the evil outsiders, I've had to replace him.", LANG_UNIVERSAL);
                me->GetMotionMaster()->MovePoint(0, -324.117249f, 1470.916870f, 31.773222f);
                walk2 = true;
            }
            else
                walk_timer2 -= diff;

            if (walk_timer3 < diff && !walk3)
            {
                me->GetMotionMaster()->MovePoint(0, -324.110199f, 1471.624268f, 31.773222f);
                walk3 = true;
            }
            else
                walk_timer3 -= diff;

            if (walk_timer4 < diff && !walk4)
            {
                me->Say("A lot of blood has been spilled. The worgens cover almost the entirety of these lands.", LANG_UNIVERSAL);
                me->HandleEmoteCommand(68);
                walk4 = true;
            }
            else
                walk_timer4 -= diff;

            if (walk_timer5 < diff && !walk5)
            {
                me->Say("So many men.. Dead..", LANG_UNIVERSAL);
                walk5 = true;
            }
            else
                walk_timer5 -= diff;

            if (walk_timer6 < diff && !walk6)
            {
                me->Say("We had to sacrifice so much to defend the city.", LANG_UNIVERSAL);
                walk6 = true;
            }
            else
                walk_timer6 -= diff;

            if (walk_timer7 < diff && !walk7)
            {
                me->HandleEmoteCommand(0);
                me->GetMotionMaster()->MovePoint(0, -326.864410f, 1467.571533f, 31.773222f);
                walk7 = true;
            }
            else
                walk_timer7 -= diff;

            if (finish_timer < diff && !c_finished)
            {
                me->Say("Finished", LANG_UNIVERSAL);

                if (player)
                {
                    if (player->IsInWorld())
                    {
                        player->RemoveAura(SPELL_ROOT);
                        player->SetPhaseMask(1, true);
                        player->m_introduction = 1;
                        me->DespawnOrUnsummon();
                    }
                    else
                    {
                        player->RemoveAura(SPELL_ROOT);
                        player->SetPhaseMask(1, true);
                        player->m_introduction = 0;
                        me->DespawnOrUnsummon();
                    }
                }
                else
                {
                    player->RemoveAura(SPELL_ROOT);
                    player->SetPhaseMask(1, true);
                    player->m_introduction = 0;
                    me->DespawnOrUnsummon();
                }
                c_finished = true;
            }
            else
                finish_timer -= diff;
        }
    };

    CreatureAI* GetAI(Creature* creature) const
    {
        return new welcome_npcAI(creature);
    }
};

void AddSC_welcome()
{
    new welcome_player();
    new welcome_creature();
}