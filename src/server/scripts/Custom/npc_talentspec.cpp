#include "ScriptPCH.h"

#pragma region Gurky

struct GurkyInfo
{
	std::string specName;
	std::string spellIds;
	uint32 playerClass;
	uint8 spec;
};

static std::vector<GurkyInfo> gurkyInfoContainer;

class GurkyLoader : public WorldScript
{
public:
	GurkyLoader() : WorldScript("GurkyLoader") {}

	void OnStartup() override
	{
		LoadFromDB();
	}

	void LoadFromDB()
	{
		gurkyInfoContainer.clear();
		QueryResult result = WorldDatabase.Query("SELECT * FROM gurky_specs");
		if (!result)
			return;

		do {
			Field* fields = result->Fetch();
			if (!fields[4].GetBool())
				continue;

			GurkyInfo info;
			info.specName = fields[0].GetString();
			info.spellIds = fields[1].GetString();
			info.playerClass = fields[2].GetUInt32();
			info.spec = fields[3].GetUInt8();
			gurkyInfoContainer.push_back(info);
		} while (result->NextRow());
	}
};

class Gurky : public CreatureScript
{
public:
	Gurky() : CreatureScript("Gurky") { }

	bool OnGossipHello(Player* player, Creature* creature) override
	{
		if (player->GetFreeTalentPoints())
		{
			player->GetSession()->SendNotification("You have not spent all of your talent points yet!");
			return false;
		}

		std::vector<GurkyInfo> availableSpecs;
		for (auto val : gurkyInfoContainer)
		{
			if (val.playerClass == player->getClass())
				availableSpecs.push_back(val);
		}

		for (auto val : availableSpecs)
		{
			std::ostringstream ss;
			ss << val.specName << "\n";
			std::string spellds = val.spellIds;
			const char* spells = spellds.c_str();
			char* modString = new char[spellds.size() + 1];
			strcpy(modString, spells);
			const char* spell = strtok(modString, ";");
			while (spell)
			{
				uint32 spellId = atoi(spell);
				if (!spellId)
					continue;

				auto spellInfo = sSpellMgr->GetSpellInfo(spellId);
				if (!spellInfo)
					continue;

				std::string spellName = spellInfo->SpellName[player->GetSession()->GetSessionDbcLocale()];

				uint32 rank = spellInfo->GetRank();
				ss << spellName;
				if (rank)
					ss << " - " << "Rank " << rank;
				ss << "\n";
				spell = strtok(NULL, ";");
			}
			delete[] modString;
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss.str().c_str(), GOSSIP_SENDER_MAIN, val.spec);
		}
		player->SEND_GOSSIP_MENU(1, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 Sender, uint32 action) override
	{
		std::vector<GurkyInfo> availableSpecs;
		GurkyInfo chosenSpec;
		for (auto val : gurkyInfoContainer)
		{
			if (val.playerClass == player->getClass())
			{
				availableSpecs.push_back(val);
				if (val.spec == action)
					chosenSpec = val;
			}
		}

		if (chosenSpec.playerClass != player->getClass())
			return false;

		std::string spellds = chosenSpec.spellIds;
		const char* spells = spellds.c_str();
		char* modString = new char[spellds.size() + 1];
		strcpy(modString, spells);
		const char* spell = strtok(modString, ";");
		while (spell)
		{
			uint32 spellId = atoi(spell);
			if (!spellId)
				continue;

			auto spellInfo = sSpellMgr->GetSpellInfo(spellId);
			if (!spellInfo)
				continue;

			player->LearnSpell(spellId, false);
			player->AddTalent(spellId, 0, true);
			player->SendTalentsInfoData(false);
			spell = strtok(NULL, ";");
		}

		delete[] modString;

		for (auto val : availableSpecs)
		{
			if (val.spec == action)
				continue;

			modString = new char[val.spellIds.size() + 1];
			strcpy(modString, val.spellIds.c_str());
			spell = strtok(modString, ";");
			while (spell)
			{
				uint32 spellId = atoi(spell);
				if (!spellId)
					continue;

				auto spellInfo = sSpellMgr->GetSpellInfo(spellId);
				if (!spellInfo)
					continue;

				player->RemoveSpell(spellId);
				player->ResetTalent(spellId);
				player->SendTalentsInfoData(false);
				spell = strtok(NULL, ";");
			}
			delete[] modString;
		}
		player->SaveToDB();
		player->CLOSE_GOSSIP_MENU();
		return true;
	}
};


#pragma endregion

void AddSC_TalentGossip()
{
     new GurkyLoader();
	 new Gurky();
}