/*#include "ScriptPCH.h"
#include "Chat.h"
#include "Common.h"
 
#define FACTION_SPECIFIC 0
 

 
class cs_world_chat : public CommandScript
{
        public:
                cs_world_chat() : CommandScript("cs_world_chat"){}
 
        ChatCommand * GetCommands() const
        {
                static ChatCommand WorldChatCommandTable[] =
                {
                        {"c",        rbac::RBAC_PERM_COMMAND_WORLD_CHAT,             true,           &HandleWorldChatCommand,        "", NULL},
                        {NULL,          0,                              false,          NULL,                                           "", NULL}
                };
 
                return WorldChatCommandTable;
        }
 
        static bool HandleWorldChatCommand(ChatHandler * handler, const char * args)
        {
                if (!handler->GetSession()->GetPlayer()->CanSpeak())
                        return false;
                std::string temp = args;
 
                if (!args || temp.find_first_not_of(' ') == std::string::npos)
                        return false;
 
                std::string msg = "";
                Player * player = handler->GetSession()->GetPlayer();
 
                switch(player->GetSession()->GetSecurity())
                {
                        // Player
                        case SEC_PLAYER:
                                if (player->GetTeam() == ALLIANCE)
                                {
                                        msg += "|cff0000ff[Alliance] ";
                                        msg += GetNameLink(player);
                                        msg += " |cfffaeb00";
                                }
 
                                else
                                {
                                        msg += "|cffff0000[Horde] ";
                                        msg += GetNameLink(player);
                                        msg += " |cfffaeb00";
                                }
                                break;
                        // VIP
                        case SEC_MODERATOR:
                                msg += "|cffff8a00[VIP] ";
                                msg += GetNameLink(player);
                                msg += " |cfffaeb00";
                                break;
                        // VIP2
                        case SEC_GAMEMASTER:
                                msg += "|cffF71010[VIP2] ";
                                msg += GetNameLink(player);
                                msg += " |cff04B404";
                                break;

                        case SEC_ADMINISTRATOR:
                                msg += "|cff0CC3CC[Gamemaster] ";
                                msg += GetNameLink(player);
                                msg += " |cff0CC3CC";
								break;

                }
                       
                msg += args;
                if (FACTION_SPECIFIC)
                {
                        SessionMap sessions = sWorld->GetAllSessions();
                        for (SessionMap::iterator itr = sessions.begin(); itr != sessions.end(); ++itr)
                                if (Player* plr = itr->second->GetPlayer())
                                        if (plr->GetTeam() == player->GetTeam())
                                                sWorld->SendServerMessage(SERVER_MSG_STRING, msg.c_str(), plr);
                }
                else
                        sWorld->SendServerMessage(SERVER_MSG_STRING, msg.c_str(), 0);  
 
                return true;
        }
};
 
void AddSC_cs_world_chat()
{
        new cs_world_chat();
}*/