#include "Language.h"
#include "RBAC.h"
class vipcommands : public CommandScript
{
public:
	vipcommands() : CommandScript("vipcommands") { }

	ChatCommand* GetCommands() const
	{
		static ChatCommand vipCommandTable[] =

		{
			//{ "mall", rbac::RBAC_PERM_COMMAND_HELP, false, &HandleVipMallCommand, "", NULL },
			{ "online", rbac::RBAC_PERM_COMMAND_HELP, false, &HandleVIPOnlineCommand, "", NULL },
			{ "changerace", rbac::RBAC_PERM_COMMAND_HELP, false, &HandleChangeRaceCommand, "", NULL },
			{ "changefaction", rbac::RBAC_PERM_COMMAND_HELP, false, &HandleChangeFactionCommand, "", NULL },
			{ "maxskills", rbac::RBAC_PERM_COMMAND_HELP, false, &HandleMaxSkillsCommand, "", NULL },
			{ "customize", rbac::RBAC_PERM_COMMAND_HELP, false, &HandleCustomizeCommand, "", NULL },
			{ NULL, 0, false, NULL, "", NULL }
		};
		static ChatCommand commandTable[] =
		{
			{ "vip", rbac::RBAC_PERM_COMMAND_VIP, true, NULL, "", vipCommandTable },
			{ NULL, 0, false, NULL, "", NULL }
		};
		return commandTable;
	}

	/* The commands */

	static bool HandleChangeRaceCommand(ChatHandler* handler, const char* args)
	{

		Player* me = handler->GetSession()->GetPlayer();
		if (!me->GetSession()->IsPremium())
		{
			handler->SendSysMessage("You are not a VIP.");
			handler->SetSentErrorMessage(true);
			return false;
		}
		me->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
		handler->PSendSysMessage("Relog to change race of your character.");
		return true;
	}

	static bool HandleChangeFactionCommand(ChatHandler* handler, const char* args)
	{

		Player* me = handler->GetSession()->GetPlayer();
		if (!me->GetSession()->IsPremium())
		{
			handler->SendSysMessage("You are not a VIP.");
			handler->SetSentErrorMessage(true);
			return false;
		}
		me->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
		handler->PSendSysMessage("Relog to change faction of your character.");
		return true;
	}

	static bool HandleMaxSkillsCommand(ChatHandler* handler, const char* args)
	{

		Player* me = handler->GetSession()->GetPlayer();
		if (!me->GetSession()->IsPremium())
		{
			handler->SendSysMessage("You are not a VIP.");
			handler->SetSentErrorMessage(true);
			return false;
		}
		me->UpdateSkillsForLevel();
		handler->PSendSysMessage("Your weapon skills are now maximized.");
		return true;
	}

	static bool HandleCustomizeCommand(ChatHandler* handler, const char* args)
	{

		Player* me = handler->GetSession()->GetPlayer();
		if (!me->GetSession()->IsPremium())
		{
			handler->SendSysMessage("You are not a VIP.");
			handler->SetSentErrorMessage(true);
			return false;
		}
		me->SetAtLoginFlag(AT_LOGIN_CUSTOMIZE);
		handler->PSendSysMessage("Relog to customize your character.");
		return true;
	}

	static bool HandleVipMallCommand(ChatHandler* handler, const char* args)
	{

		Player* me = handler->GetSession()->GetPlayer();
		if (!me->GetSession()->IsPremium())
		{
			handler->SendSysMessage("You are not a VIP.");
			handler->SetSentErrorMessage(true);
			return false;
		}
		if (me->IsInCombat())
		{
			handler->SendSysMessage(LANG_YOU_IN_COMBAT);
			handler->SetSentErrorMessage(true);
			return false;
		}

		// stop flight if need
		if (me->IsInFlight())
		{
			me->GetMotionMaster()->MovementExpired();
			me->CleanupAfterTaxiFlight();
		}
		// stop flight if need
		if (me->IsInFlight())
		{
			me->GetMotionMaster()->MovementExpired();
			me->CleanupAfterTaxiFlight();
		}
		// save only in non-flight case
		else
			me->SaveRecallPosition();

		me->TeleportTo(209, 1648, 795, 12.5, 2.4); // MapId, X, Y, Z, O
		handler->PSendSysMessage("You Have Been Teleported!");
		return true;
	}

	static bool HandleVIPOnlineCommand(ChatHandler* handler, char const* /*args*/)
	{
		bool first = true;
		bool footer = false;

		boost::shared_lock<boost::shared_mutex> lock(*HashMapHolder<Player>::GetLock());
		HashMapHolder<Player>::MapType const& m = sObjectAccessor->GetPlayers();
		for (HashMapHolder<Player>::MapType::const_iterator itr = m.begin(); itr != m.end(); ++itr)
		{
			if ((itr->second->GetSession()->IsPremium()))
			{
				if (first)
				{
					first = false;
					footer = true;
					handler->SendSysMessage("There are the following active VIPs on this server:");
					handler->SendSysMessage("========================");
				}
				std::string const& name = itr->second->GetName();
				uint8 size = name.size();;
				uint8 max = ((16 - size) / 2);
				uint8 max2 = max;
				if ((max + max2 + size) == 16)
					max2 = max - 1;
				if (handler->GetSession())
					handler->PSendSysMessage("|    %s", name.c_str());
				else
					handler->PSendSysMessage("|%*s%s%*s|   %u  |", max, " ", name.c_str(), max2, " ");
			}
		}
		if (footer)
			handler->SendSysMessage("========================");
		if (first)
			handler->SendSysMessage(LANG_GMS_NOT_LOGGED);
		return true;
	}

};

void AddSC_vipcommands()
{
	new vipcommands();
}
