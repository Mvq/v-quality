#include "AchievementMgr.h"

class SeasonReward : public CreatureScript
{
public:
	SeasonReward() : CreatureScript("SeasonReward") { }

    bool OnGossipHello(Player* player, Creature* creature) override
    {
        uint32 achievementId = 0;

        if (player->GetGUIDLow() == 2977 || player->GetGUIDLow() == 8616) // Rank 1's
        {
            achievementId = 420;
            if (AchievementEntry const* achievementEntry = sAchievementMgr->GetAchievement(achievementId))
                player->CompletedAchievement(achievementEntry);
            player->GetSession()->SendAreaTriggerMessage("Congratulations on achieving Rank 1 this season!");
        }
        else if (player->GetGUIDLow() == 9 || player->GetGUIDLow() == 881 || player->GetGUIDLow() == 1418) // Gladiator's
        {
            achievementId = 2091;
            if (AchievementEntry const* achievementEntry = sAchievementMgr->GetAchievement(achievementId))
                player->CompletedAchievement(achievementEntry);
            player->GetSession()->SendAreaTriggerMessage("Congratulations on achieving Gladiator this season!");
        }
        else if (player->GetGUIDLow() == 1913 || player->GetGUIDLow() == 2705) // Dualist's
        {
            achievementId = 2092;
            if (AchievementEntry const* achievementEntry = sAchievementMgr->GetAchievement(achievementId))
                player->CompletedAchievement(achievementEntry);
            player->GetSession()->SendAreaTriggerMessage("Congratulations on achieving Duelist this season!");
        }
        else if (player->GetGUIDLow() == 1161 || player->GetGUIDLow() == 1163 || player->GetGUIDLow() == 1960 || player->GetGUIDLow() == 2135 || player->GetGUIDLow() == 2174 || player->GetGUIDLow() == 3239 || player->GetGUIDLow() == 2787 || player->GetGUIDLow() == 3676 || player->GetGUIDLow() == 4702 || player->GetGUIDLow() == 9968) // Rival's
        {
            achievementId = 2093;
            if (AchievementEntry const* achievementEntry = sAchievementMgr->GetAchievement(achievementId))
                player->CompletedAchievement(achievementEntry);
            player->GetSession()->SendAreaTriggerMessage("Congratulations on achieving Rival this season!");
        }
        else if (player->GetGUIDLow() == 7) // Rival's
        {
            achievementId = 2090;
            if (AchievementEntry const* achievementEntry = sAchievementMgr->GetAchievement(achievementId))
                player->CompletedAchievement(achievementEntry);
            player->GetSession()->SendAreaTriggerMessage("Congratulations on achieving Challenger this season!");
        }
        else
        {
            player->GetSession()->SendAreaTriggerMessage("You did not meet this season's requirements for rewards. Good luck next season!");
        }
        player->CLOSE_GOSSIP_MENU();
        return true;
    }
};

void AddSC_SeasonReward()
{
	new SeasonReward();
}