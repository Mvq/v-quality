#include "ScriptPCH.h"
#include "ArenaTeamMgr.h"
#include "VendorRating.h"

class ShittyLoader : public WorldScript
{
public:
	ShittyLoader() : WorldScript("ShittyLoader") {}

	void OnStartup() override
	{
		QueryResult result = WorldDatabase.Query("SELECT * FROM rating_vendor ORDER BY rating");

		if (result)
		{
			do{
				Field* fields = result->Fetch();
				uint32 entry = fields[0].GetUInt32();
				uint32 rating = fields[1].GetUInt32();
				const ItemTemplate* proto = sObjectMgr->GetItemTemplate(entry);
				sRatingAccessor->vendorItems.push_back(std::make_pair(proto, rating));
			} while (result->NextRow());
		}
	}
};

class RatingVendor : public CreatureScript
{
public:
	RatingVendor() : CreatureScript("RatingVendor") {}

	bool OnGossipHello(Player* player, Creature* creature) override
	{
		TC_LOG_DEBUG("network", "WORLD: Sent SMSG_LIST_INVENTORY");

		Creature* vendor = player->GetNPCIfCanInteractWith(creature->GetGUID(), UNIT_NPC_FLAG_VENDOR);
		if (!vendor)
		{
			TC_LOG_DEBUG("network", "WORLD: SendListInventory - Unit (GUID: %u) not found or you can not interact with him.", creature->GetGUIDLow());
			player->SendSellError(SELL_ERR_CANT_FIND_VENDOR, nullptr, ObjectGuid::Empty, 0);
			return true;
		}

		if (player->HasUnitState(UNIT_STATE_DIED))
			player->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

		if (vendor->HasUnitState(UNIT_STATE_MOVING))
			vendor->StopMoving();

		uint8 count = 0;

		WorldPacket data(SMSG_LIST_INVENTORY, 8 + 1 + sRatingAccessor->vendorItems.size() * 8 * 4);
		data << uint64(creature->GetGUID());

		size_t countPos = data.wpos();
		data << uint8(count);

		uint32 item_amount = 0;
		uint32 teamId = player->GetArenaTeamId(2);
		auto arenaTeam = sArenaTeamMgr->GetArenaTeamById(teamId);
		uint32 rating = 0;
		if (arenaTeam)
			rating = arenaTeam->GetRating();
		for (auto& vendorItem : sRatingAccessor->vendorItems)
		{
			bool grey = false;
			if (vendorItem.second > rating)
				grey = true;

			data << uint32(count + 1);
			data << uint32(vendorItem.first->ItemId);
			data << uint32(vendorItem.first->DisplayInfoID);
			if (!grey)
				data << int32(0xFFFFFFFF);
			else
				data << int32(0);
			data << uint32(0);
			data << uint32(vendorItem.first->MaxDurability);
			data << uint32(vendorItem.first->BuyCount);
			data << uint32(0);
			++item_amount;
		}
		data.put<uint8>(countPos, item_amount);
		player->GetSession()->SendPacket(&data);
		return true;
	}

};

void AddSC_VendorRating_NPC()
{
	new ShittyLoader;
	new RatingVendor;
}