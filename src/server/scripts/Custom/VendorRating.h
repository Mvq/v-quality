#include "ScriptPCH.h"
#include "ArenaTeamMgr.h"

class RatingAccessor
{
public:
	RatingAccessor() {}

	static RatingAccessor* instance()
	{
		static RatingAccessor instance;
		return &instance;
	}


	void HandleRatingVendor(Player* player, Creature* creature, uint32 vendorslot, uint32 entry)
	{
		auto items = instance()->vendorItems;
		uint32 requiredRating = 0;
		const ItemTemplate* proto = nullptr;
		for (auto itr = items.begin(); itr != items.end(); ++itr)
		{
			if (itr->first->ItemId == entry)
			{
				requiredRating = itr->second;
				proto = itr->first;
				break;
			}
		}

		uint32 teamId = player->GetArenaTeamId(2);
		auto arenaTeam = sArenaTeamMgr->GetArenaTeamById(teamId);
		uint32 rating = 0;
		if (arenaTeam)
			rating = arenaTeam->GetRating();
		else
		{
			ChatHandler(player->GetSession()).PSendSysMessage("You dont have a high enough Rated Battleground rating, you need %u", requiredRating);
			return;
		}

		if (!proto)
			return;

		if (rating < requiredRating)
		{
			ChatHandler(player->GetSession()).PSendSysMessage("You dont have a high enough Rated Battleground rating, you need %u", requiredRating);
			return;
		}

		VendorItem vendorItem(entry, 0, 0, 0);

		player->_StoreOrEquipNewItem(vendorslot, entry, 1, NULL_BAG, NULL_SLOT, 0, proto, creature, &vendorItem, true);
	}
	std::vector< std::pair<const ItemTemplate*, uint32> > vendorItems;
};

#define sRatingAccessor RatingAccessor::instance()