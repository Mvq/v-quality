#include "ScriptPCH.h"
#include "Battleground.h"

std::vector<uint32> DailyQuests;

class QuestLoader : public WorldScript
{
public:
	QuestLoader() : WorldScript("QuestLoader") {}

	void OnStartup() override
	{
		LoadDailyQuests();
	}

	void LoadDailyQuests()
	{
		// Get the time before we started loading.
		uint32 oldMSTime = getMSTime();

		QueryResult result = WorldDatabase.PQuery("SELECT questId FROM dailyquests");

		//If no rows found
		if (!result)
		{
			//Display something
			TC_LOG_INFO("server.loading", ">> Loaded 0 Daily quests ids. DB table `dailyquests` is empty.");
			return;
		}
		else
		{
			//Reset row counter
			uint32 count = 0;

			//Loop through the results
			do
			{
				// Get the fields
				Field *fields = result->Fetch();
				uint32 questId = fields[0].GetUInt32();

				DailyQuests.push_back(questId);
				//Increase row counter
				++count;

			} while (result->NextRow());

			// Log that we loaded everything.
			TC_LOG_INFO("server.loading", ">> Loaded %u Daily Quest ids", count);
		}
	}
};

class QuestScripts : public PlayerScript
{
public:
	QuestScripts() : PlayerScript("QuestScripts") { }

	void OnQuestComplete(Player* player, uint32 quest) override
	{
		for (uint32 id : DailyQuests)
			if (id == quest)
			{
				Quest const* questI = sObjectMgr->GetQuestTemplate(id);
				player->RewardQuest(questI, 0, player);
				player->ReduceDailyAmount();
			}
	}

	void GiveRandomQuest(Player* player)
	{
		std::vector<uint32> quests;
		for (uint32 id : DailyQuests) // loop through the quests
			if (!player->IsActiveQuest(id))
				quests.push_back(id); // those quests the player doesnt have add them to available list

		if (!quests.empty())
		{
			Quest const* questInfo = sObjectMgr->GetQuestTemplate(quests[urand(0, quests.size())]); // get a random result from the vector
			if (!questInfo)
				return;
			if (player->CanAddQuest(questInfo, true))
			{
				player->AddQuestAndCheckCompletion(questInfo, NULL);
				player->GetSession()->SendAreaTriggerMessage("You have gotten a new Daily Quest!");
				player->SetDailyQuest();
			}
		}
	}

	void OnLogin(Player* player, bool /*firstLogin*/) override
	{
		if (!player->HasGottenDailyQuest())
			if (player->GottenDailyAmount() < 3)
				GiveRandomQuest(player);

		QueryResult currency = LoginDatabase.PQuery("SELECT dp, vp FROM website.account_data WHERE id = %u", player->GetSession()->GetAccountId());
		if (currency)
		{
			Field* field = currency->Fetch();
			player->GetSession()->SetDP(field[0].GetUInt32());
			player->GetSession()->SetVP(field[1].GetUInt32());
		}
	}

	void OnWinBattleground(Player* player, Battleground* bg, bool isArena) override
	{
		if (isArena)
		{
			if (bg->isRated())
			{
				if (player->IsActiveQuest(600000))
					player->KilledMonsterCredit(600004); // Step into the arena

				if (player->IsActiveQuest(600001)) // world wide winner
				{
					switch (bg->GetTypeID())
					{
					case BATTLEGROUND_BE:
						player->KilledMonsterCredit(600000);
						break;
					case BATTLEGROUND_NA:
						player->KilledMonsterCredit(600001);
						break;
					case BATTLEGROUND_RV:
						player->KilledMonsterCredit(600002);
						break;
					case BATTLEGROUND_DS:
						player->KilledMonsterCredit(600003);
						break;
					default:
						break;
					}
				}

				if (player->IsActiveQuest(600005) && player->GetArenaPersonalRating(0) >= 1550) // just the two of us - 0 = 2v2
					player->CompleteQuest(600005);
				if (player->IsActiveQuest(600006) && player->GetArenaPersonalRating(1) >= 1550) // three's company - 1 = 3v3
					player->CompleteQuest(600006);
				if (player->IsActiveQuest(600007) && player->GetArenaPersonalRating(2) >= 1550) // high five 1550 - 2 = 5v5
					player->CompleteQuest(600007);
			}
			else // battlegrounds
			{
				if (player->IsActiveQuest(51000) || player->IsActiveQuest(51001) || player->IsActiveQuest(51002) || player->IsActiveQuest(51003) || player->IsActiveQuest(51004))
					player->KilledMonsterCredit(11);
			}
		}
	}
};

void AddSC_QuestScripts()
{
	new QuestLoader();
	new QuestScripts();
}