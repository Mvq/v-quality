#include "ReputationMgr.h"
#include "AccountMgr.h"

class npc_donate : public CreatureScript
{
public:
	npc_donate() : CreatureScript("npc_donate") {}

	bool OnGossipHello(Player* player, Creature* creature)
	{
		if (player->GetSession()->GetSecurity() < SEC_GAMEMASTER)
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/Spell_Nature_SpiritArmor:50:50:-25:0|tRedeem VIP2 Token", GOSSIP_SENDER_MAIN, 1);	
		if (player->GetSession()->GetSecurity() == SEC_GAMEMASTER)
		player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/Spell_Nature_SpiritArmor:50:50:-25:0|tGet Tokens and Reputation", GOSSIP_SENDER_MAIN, 2);
		player->PlayerTalkClass->SendGossipMenu(6001, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
	{
		uint32 x = player->GetItemCount(20558, false);
		switch (action)
		{
		case 1:

			//ChatHandler(player->GetSession()).PSendSysMessage("|cff53F06BReturning %u", player->GetReputationMgr().GetReputation(sFactionStore.LookupEntry(349)));
			if (player->GetItemCount(38498, false))
			{
				player->DestroyItemCount(38498, 1, true);
				if (player->GetItemCount(20558, false) < 2580)
					player->AddItem(20558, (2580 - x));
				player->AddItem(20880, 1);

				uint32 x = player->GetReputationMgr().GetReputation(sFactionStore.LookupEntry(70));
				uint32 y = player->GetReputationMgr().GetReputation(sFactionStore.LookupEntry(349));

				if (player->GetTeam() == ALLIANCE)
				{
					if (x <= 21000)
					{
						player->GetReputationMgr().ModifyReputation(sFactionStore.LookupEntry(70), (21000 - x));
					}
				}
				else
				{
					std::cout << "NEINENEINEINEINEINEINE" << std::endl;
					if (y <= 21000)
					{
						player->GetReputationMgr().ModifyReputation(sFactionStore.LookupEntry(349), (21000 - y));
					}
				}
				
				if (player->GetSession()->GetSecurity() < 3)
					LoginDatabase.PExecute("REPLACE INTO account_access (id, gmlevel, RealmID) VALUES (%u, 2, -1);", player->GetSession()->GetAccountId());

				player->CastSpell(player, 47292, true);
				ChatHandler(player->GetSession()).PSendSysMessage("|cff53F06BThank you for supporting the server!");
			}
			else
				ChatHandler(player->GetSession()).PSendSysMessage("|cff53F06BYou do not have a VIP2 Token!");
				player->CLOSE_GOSSIP_MENU();
			break;


		case 2:
		{
			if (player->GetItemCount(20558, false) < 2580)
					player->AddItem(20558, (2580 - x));

				uint32 x = player->GetReputationMgr().GetReputation(sFactionStore.LookupEntry(70));
				uint32 y = player->GetReputationMgr().GetReputation(sFactionStore.LookupEntry(349));

				if (player->GetTeam() == ALLIANCE)
				{
					if (x <= 21000)
					{
						player->GetReputationMgr().ModifyReputation(sFactionStore.LookupEntry(70), (21000 - x));
					}
				}
				else
				{
					std::cout << "NEINENEINEINEINEINEINE" << std::endl;
					if (y <= 21000)
					{
						player->GetReputationMgr().ModifyReputation(sFactionStore.LookupEntry(349), (21000 - y));
					}
				}

				player->CastSpell(player, 47292, true);
				ChatHandler(player->GetSession()).PSendSysMessage("|cff53F06BThank you for supporting the server!");
			player->CLOSE_GOSSIP_MENU();
		}
			break;

		default:
			return false;
			break;
		}
		return false;

	}
};
void AddSC_npc_donate()
{
	new npc_donate();
}