#include "world_pvp.h"
#include "OutdoorPvP.h"
#include "OutdoorPvPMgr.h"
#include "Player.h"
#include "WorldPacket.h"
#include "World.h"
#include "Group.h"
#include "GroupMgr.h"
#include "MapManager.h"
#include "ObjectMgr.h"
#include "ScriptPCH.h"
 
OutdoorPvPHJ::OutdoorPvPHJ()
{
        m_TypeId = OUTDOOR_PVP_HJ;
        m_time_elapsed = 0;
        m_time_since_last = 0;
        m_countdown_timer = 0;
        flagGUID = 0;
        m_flagState = FLAG_NOT_SPAWNED;
        winner = 0;
        for (uint8 i = 0; i < 10; i++)
                announce[i] = false;
        eventStarted = false;
        eventEnded = false;
        firstStart = true;
}
 
bool OutdoorPvPHJ::SetupOutdoorPvP()
{
        RegisterZone(ZONE_ID);
        return true;
}
 
bool OutdoorPvPHJ::Update(uint32 diff)
{
        OutdoorPvP::Update(diff);
        SessionMap sessions = sWorld->GetAllSessions();
        if (!sMapMgr->FindMap(MAP_ID, 0))
                return false;
 
        if (firstStart && !eventEnded)
        {
                m_time_since_last += diff;
                if (m_time_since_last >= TIME_BETWEEN_EVENTS && !announce[0])
                {
                        announce[0] = true;
                        sWorld->SendServerMessage(SERVER_MSG_STRING, "|cff6666FF[Capture the Flag] |cffFFFFFFEvent starting in Hellfire at the Path of Glory 10 minutes!");
                        m_time_since_last = 0;
                }
                if (announce[0])
                {
                        m_countdown_timer += diff;
                        Countdown();
                        if (m_countdown_timer > 600000 && !eventStarted)
                        {
                                sWorld->SendServerMessage(SERVER_MSG_STRING, "|cff6666FF[Capture the Flag] |cffFFFFFFEvent has started, Flag has been spawned at the fork in the roads before the Path of Glory. Help your faction capture it to win the event!");
                                GameObject* go = new GameObject;
                                Map* map = sMapMgr->FindMap(MAP_ID, 0);
 
                                if (!go->Create(sObjectMgr->GenerateLowGuid(HIGHGUID_GAMEOBJECT), FLAG_ENTRY, map, 1, CapturePoints[2].m_positionX, CapturePoints[2].m_positionY, CapturePoints[2].m_positionZ, CapturePoints[2].m_orientation, 0.0f, 0.0f, 0.0f, 0.0f, 100, GO_STATE_READY))
                                {
                                        delete go;
                                        return true;
                                }
                                go->CastSpell((Unit*)go, 57467);
                                go->SetRespawnTime(0);
                                flagGUID = go->GetGUID();
                                if (!map->AddToMap(go))
                                {
                                        delete go;
                                        return true;
                                }
                                m_flagState = FLAG_ON_BASE;
                                eventStarted = true;
                                firstStart = false;
                        }
                }
        }
 
        if (m_time_elapsed < MAX_TIME_ELAPSED && eventStarted && !eventEnded)
        {
                m_time_elapsed += diff;
 
                if (m_flagState == FLAG_ON_BASE)
                {
                        Map* map = sMapMgr->FindMap(MAP_ID, 0);
                        GameObject* go = map->GetGameObject(go->GetGUID());
                        if (!go)
                        {
                                go = new GameObject;
                                if (!go->Create(sObjectMgr->GenerateLowGuid(HIGHGUID_GAMEOBJECT), FLAG_ENTRY, map, 1, CapturePoints[2].m_positionX, CapturePoints[2].m_positionY, CapturePoints[2].m_positionZ, CapturePoints[2].m_orientation, 0.0f, 0.0f, 0.0f, 0.0f, 100, GO_STATE_READY))
                                {
                                        delete go;
                                        return true;
                                }
                                go->SetRespawnTime(0);
                                flagGUID = go->GetGUID();
 
                                if (!map->AddToMap(go))
                                {
                                        delete go;
                                        return true;
                                }
 
                        }
                }
 
                if (m_flagState == FLAG_ON_PLAYER)
                {
                        Map::PlayerList const& Players = sMapMgr->FindMap(MAP_ID, 0)->GetPlayers();
                        for (Map::PlayerList::const_iterator itr = Players.begin(); itr != Players.end(); ++itr)
                        {
                                if (Player* plr = itr->GetSource())
                                {
                                        if (plr->HasAura(FLAG_SPELL))
                                        {
                                                if (plr->GetTeam() == HORDE)
                                                {
                                                        if (plr->GetDistance(CapturePoints[0].m_positionX, CapturePoints[0].m_positionY, CapturePoints[0].m_positionZ) < 2.0f)
                                                        {
                                                                winner = HORDE;
                                                                char msg[250];
                                                                sprintf(msg, "|cff6666FF[Capture the Flag] |cffFFFFFF%s captured the flag, winning the event for the Horde!", plr->GetName().c_str());
                                                                sWorld->SendServerMessage(SERVER_MSG_STRING, msg);
                                                                PlaySoundToAll(SOUND_FLAG_CAPTURED_HORDE, plr);
                                                                m_flagState = FLAG_NOT_SPAWNED;
                                                                HandleReset();
                                                                eventEnded = true;
                                                                plr->RemoveAurasDueToSpell(FLAG_SPELL);
                                                                // +1 Flagcapture to Legendary Quest (20 Flag Captures)
                                                                if (plr->GetQuestStatus(666773) == QUEST_STATUS_INCOMPLETE)
                                                                        plr->AddItem(107, 1);
                                                        }
                                                }
                                                else
                                                {
                                                        if (plr->GetDistance(CapturePoints[1].m_positionX, CapturePoints[1].m_positionY, CapturePoints[1].m_positionZ) < 2.0f)
                                                        {
                                                                winner = ALLIANCE;
                                                                char msg[250];
                                                                sprintf(msg, "|cff6666FF[Capture the Flag] |cffFFFFFF%s captured the flag, winning the event for the Alliance!", plr->GetName().c_str());
                                                                sWorld->SendServerMessage(SERVER_MSG_STRING, msg);
                                                                PlaySoundToAll(SOUND_FLAG_CAPTURED_ALLIANCE, plr);
                                                                m_flagState = FLAG_NOT_SPAWNED;
                                                                HandleReset();
                                                                eventEnded = true;
                                                                plr->RemoveAurasDueToSpell(FLAG_SPELL);
                                                                // +1 Flagcapture to Legendary Quest (20 Flag Captures)
                                                                if (plr->GetQuestStatus(666773) == QUEST_STATUS_INCOMPLETE)
                                                                        plr->AddItem(107, 1);
                                                        }
                                                }
                                        }
                                }
                        }
                }
        }
        if (m_time_elapsed > MAX_TIME_ELAPSED)
        {
                HandleReset();
                eventEnded = true;
        }
        if (eventEnded && m_time_since_last < TIME_BETWEEN_EVENTS)
        {
                m_time_since_last += diff;
        }
        if (eventEnded && m_time_since_last >= TIME_BETWEEN_EVENTS)
        {
                firstStart = true;
                m_time_since_last = 0;
                eventEnded = false;
        }
        return true;
}
 
void OutdoorPvPHJ::Countdown()
{
        if (m_countdown_timer > 60000 && m_countdown_timer < 61000 && !announce[1])
        {
                announce[1] = true;
                sWorld->SendServerMessage(SERVER_MSG_STRING, "|cff6666FF[Capture the Flag] |cffFFFFFFEvent starting in Hellfire at the Path of Glory in 9 minutes!");
        }
        if (m_countdown_timer > 120000 && m_countdown_timer < 121000 && !announce[2])
        {
                announce[2] = true;
                sWorld->SendServerMessage(SERVER_MSG_STRING, "|cff6666FF[Capture the Flag] |cffFFFFFFEvent starting in Hellfire at the Path of Glory in 8 minutes!");
        }
        if (m_countdown_timer > 180000 && m_countdown_timer < 181000 && !announce[3])
        {
                announce[3] = true;
                sWorld->SendServerMessage(SERVER_MSG_STRING, "|cff6666FF[Capture the Flag] |cffFFFFFFEvent starting in Hellfire at the Path of Glory in 7 minutes!");
        }
        if (m_countdown_timer > 240000 && m_countdown_timer < 241000 && !announce[4])
        {
                announce[4] = true;
                sWorld->SendServerMessage(SERVER_MSG_STRING, "|cff6666FF[Capture the Flag] |cffFFFFFFEvent starting in Hellfire at the Path of Glory in 6 minutes!");
        }
        if (m_countdown_timer > 300000 && m_countdown_timer < 301000 && !announce[5])
        {
                announce[5] = true;
                sWorld->SendServerMessage(SERVER_MSG_STRING, "|cff6666FF[Capture the Flag] |cffFFFFFFEvent starting in Hellfire at the Path of Glory in 5 minutes!");
        }
        if (m_countdown_timer > 360000 && m_countdown_timer < 361000 && !announce[6])
        {
                announce[6] = true;
                sWorld->SendServerMessage(SERVER_MSG_STRING, "|cff6666FF[Capture the Flag] |cffFFFFFFEvent starting in Hellfire at the Path of Glory in 4 minutes!");
        }
        if (m_countdown_timer > 420000 && m_countdown_timer < 421000 && !announce[7])
        {
                announce[7] = true;
                sWorld->SendServerMessage(SERVER_MSG_STRING, "|cff6666FF[Capture the Flag] |cffFFFFFFEvent starting in Hellfire at the Path of Glory in 3 minutes!");
        }
        if (m_countdown_timer > 480000 && m_countdown_timer < 481000 && !announce[8])
        {
                announce[8] = true;
                sWorld->SendServerMessage(SERVER_MSG_STRING, "|cff6666FF[Capture the Flag] |cffFFFFFFEvent starting in Hellfire at the Path of Glory in 2 minutes!");
        }
        if (m_countdown_timer > 540000 && m_countdown_timer < 541000 && !announce[9])
        {
                announce[9] = true;
                sWorld->SendServerMessage(SERVER_MSG_STRING, "|cff6666FF[Capture the Flag] |cffFFFFFFEvent starting in Hellfire at the Path of Glory in 1 minute!");
        }
}
 
void OutdoorPvPHJ::HandleReset()
{
        Map* map = sMapMgr->FindMap(MAP_ID, 0);
        // Something went really wrong
        if (!map)
                return;
        if (GameObject* go = map->GetGameObject(go->GetGUID()))
                go->Delete();
 
        if (winner != 0)
        {
                Map::PlayerList const& Players = map->GetPlayers();
                for (Map::PlayerList::const_iterator itr = Players.begin(); itr != Players.end(); ++itr)
                {
                        if (Player* plr = itr->GetSource())
                        {
                                std::string IPaddress = plr->GetSession()->GetRemoteAddress();
                                if (!contains(IPaddresses, IPaddress))
                                        IPaddresses.insert(IPaddress);
                                else
                                        continue;
                                if (plr->GetZoneId() == ZONE_ID)
                                {
 
                                        if (plr->GetTeam() == winner)
                                        {
                                                plr->ModifyMoney(10000 * 10);
                                                plr->AddItem(38186, 50); // 50 pvp badges
                                                plr->SetHonorPoints(plr->GetHonorPoints() + 3000);
                                        }
                                }
                        }
                }
                IPaddresses.clear();
        }
        else
                sWorld->SendServerMessage(SERVER_MSG_STRING, "|cff6666FF[CTF] |cffFFFFFFEvent has ended because time expired!");
 
        m_time_elapsed = 0;
        m_countdown_timer = 0;
        m_time_since_last = 0;
        for (uint8 i = 0; i < 10; i++)
                announce[i] = false;
        eventStarted = false;
        m_flagState = FLAG_NOT_SPAWNED;
        flagGUID = 0;
        winner = 0;
}
 
bool OutdoorPvPHJ::contains(std::set<std::string> addresses, std::string address)
{
        if (addresses.empty())
                return false;
        return addresses.find(address) != addresses.end();
}
 
void OutdoorPvPHJ::HandlePlayerEnterZone(Player* player, uint32 zone)
{
        if (eventStarted)
        {
                if (player->GetSession()->GetSecurity() > 0)
                        return;
 
                if (zone != ZONE_ID)
                        return;
 
       
                }
                OutdoorPvP::HandlePlayerEnterZone(player, zone);
        }
 
 
void OutdoorPvPHJ::HandlePlayerLeaveZone(Player* player, uint32 zone)
{
        if (eventStarted)
        {
                if (zone != ZONE_ID)
                        return;
 
                if (player->HasAura(FLAG_SPELL))
                        HandleDropFlag(player, FLAG_SPELL);
                OutdoorPvP::HandlePlayerLeaveZone(player, zone);
        }
}
 
bool OutdoorPvPHJ::HandleCustomSpell(Player* player, uint32 spellID, GameObject* go)
{
	if (player->HasAura(42792))
                return false;
        if (spellID != FLAG_SPELL_GO)
                return false;
        player->CastSpell(player, FLAG_SPELL, true);
        if (go->GetGOInfo()->entry == FLAG_ENTRY)
        {
                go->SetRespawnTime(0);
                go->Delete();
        }
 
        SendMessageToAll(PLAYER_PICKED_UP_FLAG, player);
        PlaySoundToAll(SOUND_ALLIANCE_FLAG_PICKED_UP, player);
        m_flagState = FLAG_ON_PLAYER;
        return true;
}
 
bool OutdoorPvPHJ::HandleDropFlag(Player* player, uint32 spell)
{
        if (eventEnded)
                return false;
        player->RemoveAurasDueToSpell(FLAG_SPELL);
		player->CastSpell(player, 42792, true);
        flagGUID = player->SummonGameObject(FLAG_ENTRY, CapturePoints[2].m_positionX, CapturePoints[2].m_positionY, CapturePoints[2].m_positionZ, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0)->GetGUID();
 
        SendMessageToAll(PLAYER_DROPPED_FLAG, player);
        PlaySoundToAll(SOUND_FLAG_PLACED, player);
        m_flagState = FLAG_ON_BASE;
        return true;
}
 
void OutdoorPvPHJ::SendMessageToAll(Yells id, Player* player)
{
        Map::PlayerList const& Players = player->GetMap()->GetPlayers();
        std::string team = player->GetTeam() == ALLIANCE ? "Alliance" : "Horde";
        for (Map::PlayerList::const_iterator itr = Players.begin(); itr != Players.end(); ++itr)
        {
                if (Player* plr = itr->GetSource())
                {
                        if (plr->GetZoneId() == ZONE_ID)
                        {
                                switch (id)
                                {
                                case PLAYER_DROPPED_FLAG:
                                        ChatHandler(plr->GetSession()).PSendSysMessage("|cff6666FF[CTF] |cffFFFFFFThe %s has dropped the flag and it has been reset!", team.c_str());
                                        break;
                                case PLAYER_PICKED_UP_FLAG:
                                        //ChatHandler(plr->GetSession()).PSendSysMessage("|cff6666FF[CTF] %s |cffFFFFFFpicked up the flag!", player->GetName());
                                        break;
                                case PLAYER_CAPTURED_FLAG:
                                        //ChatHandler(plr->GetSession()).PSendSysMessage("|cff6666FF[CTF] %s|cffFFFFFF captured the flag, winning the event for the %s!", player->GetName(), team.c_str());
                                        break;
                                case FLAG_RESET:
                                        ChatHandler(plr->GetSession()).PSendSysMessage("|cff6666ff[CTF] |cffFFFFFFThe flag has been reset!");
                                        break;
                                }
                        }
                }
        }
}

void OutdoorPvPHJ::PlaySoundToAll(Sounds soundID, Player* player)
{
        Map::PlayerList const& Players = player->GetMap()->GetPlayers();
        for (Map::PlayerList::const_iterator itr = Players.begin(); itr != Players.end(); ++itr)
        {
                if (Player* plr = itr->GetSource())
                {
                        if (plr->GetZoneId() == ZONE_ID)
                                plr->PlayDirectSound(soundID, plr);
                }
        }
}
 
void OutdoorPvPHJ::PlaySoundToAll(Sounds soundID, SessionMap sessions)
{
        for (SessionMap::iterator itr = sessions.begin(); itr != sessions.end(); ++itr)
        {
                if (Player* plr = itr->second->GetPlayer())
                {
                        if (plr->GetZoneId() == ZONE_ID)
                                plr->PlayDirectSound(soundID, plr);
                }
        }
}
 
class OutdoorPvP_hyjal : public OutdoorPvPScript
{
public:
        OutdoorPvP_hyjal() : OutdoorPvPScript("outdoorpvp_hj") { }
 
        OutdoorPvP* GetOutdoorPvP() const
        {
                return new OutdoorPvPHJ();
        }
};
 
void AddSC_outdoorpvp_hj()
{
        new OutdoorPvP_hyjal();
}