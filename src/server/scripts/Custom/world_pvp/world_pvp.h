#ifndef OUTDOOR_PVP_HJ_
#define OUTDOOR_PVP_HJ_
#include "OutdoorPvP.h"
#include <set>
 
enum Yells
{
        PLAYER_DROPPED_FLAG             = 0,
        PLAYER_PICKED_UP_FLAG   = 1,
        PLAYER_CAPTURED_FLAG    = 2,
        FLAG_RESET                              = 3,
};
 
enum Sounds
{
        SOUND_FLAG_CAPTURED_ALLIANCE        = 8173,
    SOUND_FLAG_CAPTURED_HORDE           = 8213,
    SOUND_FLAG_PLACED                   = 8232,
    SOUND_FLAG_RETURNED                 = 8192,
        SOUND_HORDE_FLAG_PICKED_UP          = 8212,
    SOUND_ALLIANCE_FLAG_PICKED_UP       = 8174,
    SOUND_FLAGS_RESPAWNED               = 8232
};
 
enum FlagStates
{
        FLAG_ON_PLAYER          = 0,
        FLAG_NOT_SPAWNED        = 1,
        FLAG_ON_BASE            = 2
};
 
enum Spells
{
    FLAG_SPELL                                      = 34976,
    DROPPED_FLAG_SPELL                  = 34991,
        FLAG_SPELL_GO                               = 60000,
    SPELL_RECENTLY_DROPPED_FLAG1    = 42792
};
 
static const Position CapturePoints[] =
{
        { 204.205566f, 2692.583740f, 90.703735f, 0.0f },                // Horde
        { -709.635498f, 2707.203857f, 94.720642f, 0.0f },               // Alliance
        { -212.500656f, 2161.370361f, 78.222771f, 0.0f },               // Spawn location
};
 
#define FLAG_ENTRY                                      500001
#define MAX_TIME_ELAPSED                        1000*60*60*1/2
#define TIME_BETWEEN_EVENTS                 1000*60*60*1
#define MAP_ID                                          530                                 // MapID where the event takes place
#define ZONE_ID                                         3483                            // ZoneID where the event takes place
 
class OutdoorPvPHJ : public OutdoorPvP
{
        public:
                OutdoorPvPHJ();
                bool SetupOutdoorPvP();
                void HandlePlayerEnterZone(Player* player, uint32 zone);
                void HandlePlayerLeaveZone(Player* player, uint32 zone);
 
                bool Update(uint32 diff);
                void HandleReset();
                void SendMessageToAll(Yells id, Player* source);
                void SendMessageToAll(Yells id, SessionMap sessions);
                void PlaySoundToAll(Sounds id, Player* source);
                void PlaySoundToAll(Sounds id, SessionMap sessions);
                bool HandleCustomSpell(Player* player, uint32 spellID, GameObject* go);
                bool HandleDropFlag(Player* player, uint32 spell);
                void Countdown();
 
                bool contains(std::set<std::string> addresses, std::string address);
 
                std::set<std::string> IPaddresses;
                uint64 m_time_elapsed;
                uint64 m_time_since_last;
                uint64 m_countdown_timer;
                uint64 flagGUID;
                uint8 m_flagState;
                uint64 guid;
                uint32 winner;
                bool announce[10];
                bool eventStarted;
                bool eventEnded;
                bool firstStart;
};
#endif