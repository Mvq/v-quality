#include "ScriptPCH.h"

class npc_transmog : public CreatureScript
{
public:
    npc_transmog() : CreatureScript("npc_transmog") { }

    bool OnGossipHello(Player* player, Creature* creature) override
    {
        WorldSession* session = player->GetSession();
        for (uint8 slot = EQUIPMENT_SLOT_START; slot < EQUIPMENT_SLOT_END; slot++)
        {
            // if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, slot))
            if (const char* slotName = getSlotName(slot, session))
                if (Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, slot))
                    if (item)
                        player->ADD_GOSSIP_ITEM_EXTENDED(4, GetItemIcon(item->GetEntry(), 50, 50, -25, 0), 1, slot, "", 0, true);
        }
        player->ADD_GOSSIP_ITEM_EXTENDED(4, "Remove Transmog", 2, 100, "", 0, true);
        player->SEND_GOSSIP_MENU(9425, creature->GetGUID());
        return true;
    }

    bool OnGossipSelectCode(Player* player, Creature* creature, uint32 sender, uint32 action, char const* code) override
    {
        player->PlayerTalkClass->ClearMenus();

        if (sender == 2 && action == 100)
        {
            WorldSession* session = player->GetSession();
            for (uint8 slot = EQUIPMENT_SLOT_START; slot < EQUIPMENT_SLOT_END; slot++)
            {
                // if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, slot))
                if (const char* slotName = getSlotName(slot, session))
                    if (Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, slot))
                        if (item)
                            player->ADD_GOSSIP_ITEM_EXTENDED(4, GetItemIcon(item->GetEntry(), 50, 50, -25, 0), 3, item->GetEntry(), "", 0, true);
            }
            player->ADD_GOSSIP_ITEM_EXTENDED(4, "Back", 2, 1000, "", 0, true);
            player->SEND_GOSSIP_MENU(9425, creature->GetGUID());
        }
        else if (sender == 2 && action == 1000)
        {
            WorldSession* session = player->GetSession();
            for (uint8 slot = EQUIPMENT_SLOT_START; slot < EQUIPMENT_SLOT_END; slot++)
            {
                // if (player->GetItemByPos(INVENTORY_SLOT_BAG_0, slot))
                if (const char* slotName = getSlotName(slot, session))
                    if (Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, slot))
                        if (item)
                            player->ADD_GOSSIP_ITEM_EXTENDED(4, GetItemIcon(item->GetEntry(), 50, 50, -25, 0), 1, item->GetEntry(), "", 0, true);
            }
            player->ADD_GOSSIP_ITEM_EXTENDED(4, "Remove Transmog", 2, 100, "", 0, true);
            player->SEND_GOSSIP_MENU(9425, creature->GetGUID());
        }
        else if (sender == 3)
        {
            Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, action);
            uint32 itemid = 0;

            uint32 id = atoul(code);
            const ItemTemplate* temp = id ? sObjectMgr->GetItemTemplate(id) : NULL;
            if (!temp)
            {
                CloseGossipMsg(player, "Item not found!");
                return true;
            }

            if (item)
            {
                itemid = item->GetEntry();
            }

            CharacterDatabase.PExecute("DELETE FROM transmog_force_item_character WHERE charid=%u AND item1=%u", player->GetGUIDLow(), itemid);
            CloseGossipMsg(player, "Your item's transmog was successfully removed");
        }
        else
        {
            Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, action);
            uint32 itemid = 0;

            uint32 id = atoul(code);
            const ItemTemplate* temp = id ? sObjectMgr->GetItemTemplate(id) : NULL;
            if (!temp || temp->InventoryType != item->GetSlot())
            {
                CloseGossipMsg(player, "The item you choose does either not exist or does not share the same slot type.");
                return true;
            }

            if (item)
            {
                itemid = item->GetEntry();
            }

            QueryResult result = CharacterDatabase.PQuery("SELECT * FROM transmog_force_item_character WHERE charid=%u AND item1=%u", player->GetGUIDLow(), itemid);

            if (result)
            {
                CloseGossipMsg(player, "Please remove the transmog on your current item before attempting to transmogging it again.");
                return true;
            }


            CharacterDatabase.PExecute("INSERT INTO transmog_force_item_character (charid, item1, item2, comment) VALUES(%u, %u, %u, 'Added from the ingame Character menu')", player->GetGUIDLow(), itemid, id);
            CloseGossipMsg(player, "Your item was successfully transmogged, please re-equip it!.");
        }

        player->CLOSE_GOSSIP_MENU();
        return true;
    }

    static void CloseGossipMsg(Player* player, char const* msg)
    {
        player->GetSession()->SendNotification(msg);
        ChatHandler(player->GetSession()).SendSysMessage(msg);
        player->CLOSE_GOSSIP_MENU();
    }

    const char* getSlotName(uint8 slot, WorldSession* /*session*/)
    {
        TC_LOG_DEBUG("custom.transmog", "TransmogDisplayVendorMgr::TransmogDisplayVendorMgr::getSlotName");

        switch (slot)
        {
        case EQUIPMENT_SLOT_HEAD: return  "Head";// session->GetTrinityString(LANG_SLOT_NAME_HEAD);
        case EQUIPMENT_SLOT_SHOULDERS: return  "Shoulders";// session->GetTrinityString(LANG_SLOT_NAME_SHOULDERS);
        case EQUIPMENT_SLOT_BODY: return  "Shirt";// session->GetTrinityString(LANG_SLOT_NAME_BODY);
        case EQUIPMENT_SLOT_CHEST: return  "Chest";// session->GetTrinityString(LANG_SLOT_NAME_CHEST);
        case EQUIPMENT_SLOT_WAIST: return  "Waist";// session->GetTrinityString(LANG_SLOT_NAME_WAIST);
        case EQUIPMENT_SLOT_LEGS: return  "Legs";// session->GetTrinityString(LANG_SLOT_NAME_LEGS);
        case EQUIPMENT_SLOT_FEET: return  "Feet";// session->GetTrinityString(LANG_SLOT_NAME_FEET);
        case EQUIPMENT_SLOT_WRISTS: return  "Wrists";// session->GetTrinityString(LANG_SLOT_NAME_WRISTS);
        case EQUIPMENT_SLOT_HANDS: return  "Hands";// session->GetTrinityString(LANG_SLOT_NAME_HANDS);
        case EQUIPMENT_SLOT_BACK: return  "Back";// session->GetTrinityString(LANG_SLOT_NAME_BACK);
        case EQUIPMENT_SLOT_MAINHAND: return  "Main hand";// session->GetTrinityString(LANG_SLOT_NAME_MAINHAND);
        case EQUIPMENT_SLOT_OFFHAND: return  "Off hand";// session->GetTrinityString(LANG_SLOT_NAME_OFFHAND);
        case EQUIPMENT_SLOT_RANGED: return  "Ranged";// session->GetTrinityString(LANG_SLOT_NAME_RANGED);
        case EQUIPMENT_SLOT_TABARD: return  "Tabard";// session->GetTrinityString(LANG_SLOT_NAME_TABARD);
        default: return nullptr;
        }
    }

    std::string GetItemIcon(uint32 entry, uint32 width, uint32 height, int x, int y) const
    {
        TC_LOG_DEBUG("custom.transmog", "GetItemIcon");

        std::ostringstream ss;
        ss << "|TInterface";
        const ItemTemplate* temp = sObjectMgr->GetItemTemplate(entry);
        const ItemDisplayInfoEntry* info = NULL;
        if (temp)
        {
            info = sItemDisplayInfoStore.LookupEntry(temp->DisplayInfoID);
            if (info)
                ss << "/ICONS/" << info->inventoryIcon;
        }
        if (!info)
            ss << "/InventoryItems/WoWUnknownItem01";
        ss << ":" << width << ":" << height << ":" << x << ":" << y << "|t";
        return ss.str();
    }
};

void AddSC_npc_transmog()
{
    new npc_transmog();
}
