﻿#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "Player.h"
#include "Language.h"
#include <cstring>

std::string ratingtorank(uint32 rating, uint8 type)
{
	const char* rankStr = NULL;
	if (type == 2)
	{
		if (rating < 1700) // if rating under 1700
			rankStr = "|TInterface\\ICONS\\Achievement_Arena_2v2_3:25:25:-22:0|t";
		else if (rating >= 1700 && rating < 1900) // if rating at 1700+
			rankStr = "|TInterface\\ICONS\\Achievement_Arena_2v2_5:25:25:-22:0|t";
		else if (rating >= 1900 && rating < 2100) // if rating at 1900+
			rankStr = "|TInterface\\ICONS\\Achievement_Arena_2v2_7:25:25:-22:0|t";
		else if (rating >= 2100) // if rating at 2100+
			rankStr = "|TInterface\\ICONS\\Achievement_FeatsOfStrength_Gladiator_02:25:25:-22:0|t";
		else
			rankStr = "|TInterface\\ICONS\\Inv_misc_questionmark:25:25:-22:0|t";
	}
	if (type == 3)
	{
		if (rating < 1700) // if rating under 1700
			rankStr = "|TInterface\\ICONS\\Achievement_Arena_3v3_1:25:25:-22:0|t";
		else if (rating >= 1700 && rating < 1900) // if rating at 1700+
			rankStr = "|TInterface\\ICONS\\Achievement_Arena_3v3_6:25:25:-22:0|t";
		else if (rating >= 1900 && rating < 2100) // if rating at 1900+
			rankStr = "|TInterface\\ICONS\\Achievement_Arena_3v3_7:25:25:-22:0|t";
		else if (rating >= 2100) // if rating at 2100+
			rankStr = "|TInterface\\ICONS\\Achievement_FeatsOfStrength_Gladiator_09:25:25:-22:0|t";
		else
			rankStr = "|TInterface\\ICONS\\Inv_misc_questionmark:25:25:-22:0|t";
	}
	if (type == 5)
	{
		if (rating < 1700) // if rating under 1700
			rankStr = "|TInterface\\ICONS\\Achievement_Arena_5v5_1:25:25:-22:0|t";
		else if (rating >= 1700 && rating < 1900) // if rating at 1700+
			rankStr = "|TInterface\\ICONS\\Achievement_Arena_5v5_5:25:25:-22:0|t";
		else if (rating >= 1900 && rating < 2100) // if rating at 1900+
			rankStr = "|TInterface\\ICONS\\Achievement_Arena_5v5_7:25:25:-22:0|t";
		else if (rating >= 2100) // if rating at 2100+
			rankStr = "|TInterface\\ICONS\\Achievement_FeatsOfStrength_Gladiator_04:25:25:-22:0|t";
		else
			rankStr = "|TInterface\\ICONS\\Inv_misc_questionmark:25:25:-22:0|t";
	}
	return rankStr;
}

std::string killstorank(uint8 race, uint32 kills)
{
	const char* rankStr = NULL;
	// alliance icons [blue]
	if ((race == RACE_HUMAN) || (race == RACE_DWARF) || (race == RACE_NIGHTELF) || (race == RACE_GNOME) || (race == RACE_DRAENEI))
	{
		if (kills < 250) // rank under 250 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_01:20:20:0:0|t";
		else if (kills >= 250 && kills < 500) // rank at 250 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_02:20:20:0:0|t";
		else if (kills >= 500 && kills < 750) // rank at 500 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_03:20:20:0:0|t";
		else if (kills >= 750 && kills < 1000) // rank at 750 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_04:20:20:0:0|t";
		else if (kills >= 1000 && kills < 1250) // rank at 1000 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_05:20:20:0:0|t";
		else if (kills >= 1250 && kills < 1500) // rank at 1250 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_06:20:20:0:0|t";
		else if (kills >= 1500 && kills < 1750) // rank at 1500 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_07:20:20:0:0|t";
		else if (kills >= 1750 && kills < 2000) // rank at 1750 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_08:20:20:0:0|t";
		else if (kills >= 2000 && kills < 2250) // rank at 2000 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_09:20:20:0:0|t";
		else if (kills >= 2250 && kills < 2500) // rank at 2250 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_10:20:20:0:0|t";
		else if (kills >= 2500 && kills < 2750) // rank at 2500 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_11:20:20:0:0|t";
		else if (kills >= 2750 && kills < 3000) // rank at 2750 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_12:20:20:0:0|t";
		else if (kills >= 3000 && kills < 4000) // rank at 3000 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_13:20:20:0:0|t";
		else if (kills >= 4000 && kills < 7000) // rank at 4000 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_14:20:20:0:0|t";
		else if (kills >= 7000 && kills < 10000) // rank at 7000 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_A_15:20:20:0:0|t";
		else if (kills >= 10000) // rank at 10000+ kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_P_250K:20:20:0:0|t";
		else
			rankStr = "|TInterface\\ICONS\\Inv_misc_questionmark:20:20:0:0|t";
	}
	else // horde icons [red]
	{
		if (kills < 250) // rank under 250 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_01:20:20:0:0|t";
		else if (kills >= 250 && kills < 500) // rank at 250 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_02:20:20:0:0|t";
		else if (kills >= 500 && kills < 750) // rank at 500 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_03:20:20:0:0|t";
		else if (kills >= 750 && kills < 1000) // rank at 750 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_04:20:20:0:0|t";
		else if (kills >= 1000 && kills < 1250) // rank at 1000 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_05:20:20:0:0|t";
		else if (kills >= 1250 && kills < 1500) // rank at 1250 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_06:20:20:0:0|t";
		else if (kills >= 1500 && kills < 1750) // rank at 1500 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_07:20:20:0:0|t";
		else if (kills >= 1750 && kills < 2000) // rank at 1750 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_08:20:20:0:0|t";
		else if (kills >= 2000 && kills < 2250) // rank at 2000 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_09:20:20:0:0|t";
		else if (kills >= 2250 && kills < 2500) // rank at 2250 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_10:20:20:0:0|t";
		else if (kills >= 2500 && kills < 2750) // rank at 2500 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_11:20:20:0:0|t";
		else if (kills >= 2750 && kills < 3000) // rank at 2750 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_12:20:20:0:0|t";
		else if (kills >= 3000 && kills < 4000) // rank at 3000 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_13:20:20:0:0|t";
		else if (kills >= 4000 && kills < 7000) // rank at 4000 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_14:20:20:0:0|t";
		else if (kills >= 7000 && kills < 10000) // rank at 7000 kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_H_15:20:20:0:0|t";
		else if (kills >= 10000) // rank at 10000+ kills
			rankStr = "|TInterface\\ICONS\\Achievement_PVP_P_250K:20:20:0:0|t";
		else
			rankStr = "|TInterface\\ICONS\\Inv_misc_questionmark:20:20:0:0|t";
	}
	return rankStr;
}

std::string classtostring(uint8 class_id)
{
	const char* classStr = NULL;
	switch (class_id)
	{
		//warrior
	case CLASS_WARRIOR: classStr = "|TInterface\\ICONS\\INV_Sword_27:20:20:0:0|t";
		break;
		//paladin
	case CLASS_PALADIN: classStr = "|TInterface\\ICONS\\Ability_Thunderbolt:20:20:0:0|t";
		break;
		//hunter
	case CLASS_HUNTER: classStr = "|TInterface\\ICONS\\INV_Weapon_Bow_07:20:20:0:0|t";
		break;
		//rogue
	case CLASS_ROGUE: classStr = "|TInterface\\ICONS\\INV_Throwingknife_04:20:20:0:0|t";
		break;
		//priest
	case CLASS_PRIEST: classStr = "|TInterface\\ICONS\\INV_Staff_30:20:20:0:0|t";
		break;
		//Deathknight
	case CLASS_DEATH_KNIGHT: classStr = "|TInterface\\ICONS\\Spell_Deathknight_Classicon:20:20:0:0|t";
		break;
		//Shaman
	case CLASS_SHAMAN: classStr = "|TInterface\\ICONS\\Spell_Nature_Bloodlust:20:20:0:0|t";
		break;
		//mage
	case CLASS_MAGE: classStr = "|TInterface\\ICONS\\INV_Staff_13.jpg:20:20:0:0|t";
		break;
		//Warlock
	case CLASS_WARLOCK: classStr = "|TInterface\\ICONS\\Spell_Nature_Drowsy:20:20:0:0|t";
		break;
		//Druid
	case CLASS_DRUID: classStr = "|TInterface\\ICONS\\Ability_Druid_Maul:20:20:0:0|t";
		break;
	default: classStr = "|TInterface\\ICONS\\Inv_Misc_Questionmark:20:20:0:0|t";
		break;
	}
	return classStr;
}

class npc_ladder : public CreatureScript
{
public:
	npc_ladder() : CreatureScript("npc_ladder") { }

	bool ShowArenaTeams(Player* player, Creature* creature, uint16 arena)
	{
		player->PlayerTalkClass->ClearMenus();

		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "|TInterface\\PaperDollInfoFrame\\UI-GearManager-Undo:25:25:-22:0|t |cff6E353A RETURN|r", GOSSIP_SENDER_MAIN, 0);

		QueryResult result = CharacterDatabase.PQuery("SELECT `name`, `rating` FROM `arena_team` WHERE `type`=%u ORDER BY `rating` DESC LIMIT 20", arena);
		if (!result)
		{
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|TInterface\\ICONS\\Inv_misc_questionmark:25:25:-22:0|t |cffFF0000No Results!|r", GOSSIP_SENDER_MAIN, 0);
			player->PlayerTalkClass->SendGossipMenu(50027, creature->GetGUID());
			return false;
		}

		switch (arena)
		{
		case 2:
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|TInterface\\ICONS\\Achievement_Leader_King_Varian_Wrynn:25:25:-22:0|t|cff434343 PvP Ladder 2v2 arena!|r", GOSSIP_SENDER_MAIN, 0);
			break;
		case 3:
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|TInterface\\ICONS\\Achievement_Leader_King_Varian_Wrynn:25:25:-22:0|t|cff434343 PvP Ladder 3v3 arena!|r", GOSSIP_SENDER_MAIN, 0);
			break;
		case 5:
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|TInterface\\ICONS\\Achievement_Leader_King_Varian_Wrynn:25:25:-22:0|t|cff434343 PvP Ladder 5v5 arena!|r", GOSSIP_SENDER_MAIN, 0);
			break;
		}

		uint16 counter = 0;
		Field * fields = NULL;
		do
		{
			std::ostringstream result_string;

			fields = result->Fetch();
			std::string team_name = fields[0].GetString();
			uint16 team_rating = fields[1].GetUInt16();

			counter += 1;
			result_string << ratingtorank(team_rating, arena) << " " << counter << " - TEAM |cffFF0000[" << team_name.c_str() << "]|r\n--------- Rating:|cffAB00FF " << team_rating << "|r";

			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, result_string.str().c_str(), GOSSIP_SENDER_MAIN, 0);

		} while (result->NextRow());
		player->PlayerTalkClass->SendGossipMenu(50027, creature->GetGUID());
		return true;
	}

	bool OnGossipHello(Player* player, Creature* creature) override
	{
		player->PlayerTalkClass->ClearMenus();

		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\ICONS\\Achievement_Arena_2v2_7:35:35:-22:0|t |cffFF0000[PvP]|r Top 30 Killers.", GOSSIP_SENDER_MAIN, 1);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\ICONS\\Ability_Warrior_WeaponMastery:35:35:-22:0|t |cffFF0000[PvP]|r Top BG Win Sprees.", GOSSIP_SENDER_MAIN, 6);
		/*player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\ICONS\\Achievement_BG_3flagcap_nodeaths:35:35:-22:0|t |cffFF0000[PvP]|r Top Flag Captures (WSG).", GOSSIP_SENDER_MAIN, 5);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\ICONS\\Achievement_BG_3flagcap_nodeaths:35:35:-22:0|t |cffFF0000[PvP]|r Top Flag Captures (EYE).", GOSSIP_SENDER_MAIN, 8);*/
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\ICONS\\Achievement_FeatsOfStrength_Gladiator_02:35:35:-22:0|t |cffFF0000[PvP]|r Top 20 Arena 2v2.", GOSSIP_SENDER_MAIN, 2);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\ICONS\\Achievement_FeatsOfStrength_Gladiator_09:35:35:-22:0|t |cffFF0000[PvP]|r Top 20 Arena 3v3.", GOSSIP_SENDER_MAIN, 3);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\ICONS\\Achievement_FeatsOfStrength_Gladiator_04:35:35:-22:0|t |cffFF0000[PvP]|r Top 20 Arena 5v5.", GOSSIP_SENDER_MAIN, 4);

		player->PlayerTalkClass->SendGossipMenu(50027, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action) override
	{
		player->PlayerTalkClass->ClearMenus();
		switch (action)
		{
		case 0: // main menu
			OnGossipHello(player, creature);
			break;
		case 1: // top x killers
		{
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "|TInterface\\PaperDollInfoFrame\\UI-GearManager-Undo:25:25:-22:0|t |cff6E353ARETURN|r", GOSSIP_SENDER_MAIN, 0);

			QueryResult result = CharacterDatabase.Query("SELECT `name`, `race`, `class`, `totalKills` FROM `characters` WHERE `totalKills`>=1 ORDER BY `totalKills` DESC LIMIT 30");
			if (!result)
			{
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|TInterface\\ICONS\\Inv_misc_questionmark:25:25:-22:0|t |cffFF0000No Results!|r", GOSSIP_SENDER_MAIN, 0);
				player->PlayerTalkClass->SendGossipMenu(50027, creature->GetGUID());
				return false;
			}

			uint16 counter = 0;
			Field * fields = NULL;
			do
			{
				std::ostringstream result_string;

				fields = result->Fetch();
				std::string char_name = fields[0].GetString();
				uint8 char_race = fields[1].GetUInt8();
				uint8 char_class = fields[2].GetUInt8();
				uint32 char_kills = fields[3].GetUInt32();

				counter += 1;
				result_string << killstorank(char_race, char_kills) << " " << counter << " " << classtostring(char_class) << " Name |cffFF0000[" << char_name.c_str() << "]|r, Kills:|cff00FFFF " << char_kills << "|r";

				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, result_string.str().c_str(), GOSSIP_SENDER_MAIN, 0);

			} while (result->NextRow());
			player->PlayerTalkClass->SendGossipMenu(50027, creature->GetGUID());
		} break;
		case 2: // top x 2v2 arenas
			ShowArenaTeams(player, creature, 2);
			break;
		case 3: // top x 3v3 arenas
			ShowArenaTeams(player, creature, 3);
			break;
		case 4: // top x 5v5 arenas
			ShowArenaTeams(player, creature, 5);
			break;
		case 5:
		{
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "|TInterface\\PaperDollInfoFrame\\UI-GearManager-Undo:25:25:-22:0|t |cff6E353ARETURN|r", GOSSIP_SENDER_MAIN, 0);

			QueryResult result = CharacterDatabase.Query("SELECT `name`, `race`, `class`, `totalKills`, `FlagCaptures` FROM `characters` WHERE `FlagCaptures`>=1 ORDER BY `FlagCaptures` DESC LIMIT 30");
			if (!result)
			{
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|TInterface\\ICONS\\Inv_misc_questionmark:25:25:-22:0|t |cffFF0000No Results!|r", GOSSIP_SENDER_MAIN, 0);
				player->PlayerTalkClass->SendGossipMenu(50027, creature->GetGUID());
				return false;
			}

			uint16 counter = 0;
			Field * fields = NULL;
			do
			{
				std::ostringstream result_string;

				fields = result->Fetch();
				std::string char_name = fields[0].GetString();
				uint8 char_race = fields[1].GetUInt8();
				uint8 char_class = fields[2].GetUInt8();
				uint32 char_kills = fields[3].GetUInt32();
				uint32 char_flags = fields[4].GetUInt16();

				counter += 1;
				result_string << killstorank(char_race, char_kills) << " " << counter << " " << classtostring(char_class) << " Name |cffFF0000[" << char_name.c_str() << "]|r, Flags:|cff00FFFF " << char_flags << "|r";

				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, result_string.str().c_str(), GOSSIP_SENDER_MAIN, 0);

			} while (result->NextRow());
			player->PlayerTalkClass->SendGossipMenu(50027, creature->GetGUID());
		}
		break;
		case 8:
		{
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "|TInterface\\PaperDollInfoFrame\\UI-GearManager-Undo:25:25:-22:0|t |cff6E353ARETURN|r", GOSSIP_SENDER_MAIN, 0);

			QueryResult result = CharacterDatabase.Query("SELECT `name`, `race`, `class`, `totalKills`, `FlagCapturesEY` FROM `characters` WHERE `FlagCapturesEY`>=1 ORDER BY `FlagCapturesEY` DESC LIMIT 30");
			if (!result)
			{
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|TInterface\\ICONS\\Inv_misc_questionmark:25:25:-22:0|t |cffFF0000No Results!|r", GOSSIP_SENDER_MAIN, 0);
				player->PlayerTalkClass->SendGossipMenu(50027, creature->GetGUID());
				return false;
			}

			uint16 counter = 0;
			Field * fields = NULL;
			do
			{
				std::ostringstream result_string;

				fields = result->Fetch();
				std::string char_name = fields[0].GetString();
				uint16 char_race = fields[1].GetUInt16();
				uint16 char_class = fields[2].GetUInt16();
				uint32 char_kills = fields[3].GetUInt32();
				uint32 char_flags = fields[4].GetUInt16();

				counter += 1;
				result_string << killstorank(char_race, char_kills) << " " << counter << " " << classtostring(char_class) << " Name |cffFF0000[" << char_name.c_str() << "]|r, Flags:|cff00FFFF " << char_flags << "|r";

				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, result_string.str().c_str(), GOSSIP_SENDER_MAIN, 0);

			} while (result->NextRow());
			player->PlayerTalkClass->SendGossipMenu(50027, creature->GetGUID());
		}
		break;
		case 6:
		{
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "|TInterface\\PaperDollInfoFrame\\UI-GearManager-Undo:25:25:-22:0|t |cff6E353ARETURN|r", GOSSIP_SENDER_MAIN, 0);

			QueryResult result = CharacterDatabase.Query("SELECT `name`, `race`, `class`, `WinStreek` FROM `characters` WHERE `WinStreek` > 0 ORDER BY `WinStreek` DESC LIMIT 30");
			if (!result)
			{
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|TInterface\\ICONS\\Inv_misc_questionmark:25:25:-22:0|t |cffFF0000No Results!|r", GOSSIP_SENDER_MAIN, 0);
				player->PlayerTalkClass->SendGossipMenu(50030, creature->GetGUID());
				return false;
			}

			uint16 counter = 0;
			Field * fields = NULL;
			do
			{
				std::ostringstream result_string;

				fields = result->Fetch();
				std::string char_name = fields[0].GetString();
				uint8 char_race = fields[1].GetUInt8();
				uint8 char_class = fields[2].GetUInt8();
				uint32 char_streek = fields[3].GetUInt32();

				counter += 1;
				result_string << counter << " " << classtostring(char_class) << " Name |cffFF0000[" << char_name.c_str() << "]|r, Spree:|cff00FFFF " << char_streek << ".|r";

				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, result_string.str().c_str(), GOSSIP_SENDER_MAIN, 0);

			} while (result->NextRow());
			player->PlayerTalkClass->SendGossipMenu(50030, creature->GetGUID());
		}
		break;
		}

		return true;
	}
};

void AddSC_npc_ladder()
{
    new npc_ladder();
}