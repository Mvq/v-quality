#include "ScriptPCH.h"

enum Spells
{
	s_LIGHTNINGBOLT = 72163,
};

enum Events
{
	e_LIGHTNINGBOLT = 1,
};

class mageboss : public CreatureScript
{
public:
	mageboss() : CreatureScript("mageboss") {}
struct magebossAI : public BossAI
{
	magebossAI(Creature* creature) : BossAI(creature, 0)
	{
	}
	
	void EnterCombat(Unit* /*who*/)
	{
		DoCastVictim(s_LIGHTNINGBOLT);
		events.ScheduleEvent(e_LIGHTNINGBOLT, 3000);
	}

	void UpdateAI(uint32 diff) override
		{

			if (!UpdateVictim())
			return;

			events.Update(diff);

			if (me->HasUnitState(UNIT_STATE_CASTING))
				return;

			while (uint32 eventId = events.ExecuteEvent())
			{
				switch (eventId)
				{
				case e_LIGHTNINGBOLT:
					if (Unit *target = SelectTarget(SELECT_TARGET_RANDOM, 0))
					DoCastVictim(s_LIGHTNINGBOLT);
					events.ScheduleEvent(e_LIGHTNINGBOLT, 3000);
					break;
				default:
					break;
				}
			}

			DoMeleeAttackIfReady();
		}
	};

	CreatureAI* GetAI(Creature* creature) const
	{
		return new magebossAI(creature);
	}
};

class warriorboss : public CreatureScript
{
public:
	warriorboss() : CreatureScript("warriorboss") {}
	struct warriorbossAI : public BossAI
	{
		warriorbossAI(Creature* creature) : BossAI(creature, 0)
		{
		}

		void EnterCombat(Unit* /*s*/)
		{
			events.ScheduleEvent(1, 500);
			events.ScheduleEvent(2, 5000);
		}

		void UpdateAI(uint32 diff) override
		{
			if (!UpdateVictim())
				return;

			events.Update(diff);

			if (me->HasUnitState(UNIT_STATE_CASTING))
				return;

			while (uint32 eventId = events.ExecuteEvent())
			{
				switch (eventId)
				{
				case 1:
					events.ScheduleEvent(1, 500);
					if (me->GetHealthPct() == 25)
						DoCast(26662);
					break;
				case 2:
					if (roll_chance_i(1))
					{
						Talk(0);
						DoCastToAllHostilePlayers(71420);
					}
					else
					{
						Talk(1);
						events.ScheduleEvent(2, 5000);
					}

						break;

				default:
					break;
				}
			}

			DoMeleeAttackIfReady();
		}
	};

	CreatureAI* GetAI(Creature* creature) const
	{
		return new warriorbossAI(creature);
	}
};

void AddSC_mageboss()
{
	new mageboss();
	new warriorboss();
}