#include "ReputationMgr.h"

class tele : public CreatureScript
{
public:
	tele() : CreatureScript("tele") {}

	bool OnGossipHello(Player* player, Creature* me) 
	{
		if (player->GetTeam() == ALLIANCE)
		{
			if (player->GetAreaId() == 1682)
			{
				player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/INV_Misc_Bandana_03:50:50:-25:0|t Already going?", GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(1, "Nevermind.", GOSSIP_SENDER_MAIN, 3);
				player->PlayerTalkClass->SendGossipMenu(610000, me->GetGUID());
			}
			else
			{
				player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/INV_Misc_Bandana_03:50:50:-25:0|t I see you're comitted.\nWould you like to Upgrade your rankings within the Syndicate?", GOSSIP_SENDER_MAIN, 1);
				player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/Spell_Holy_ChampionsBond:50:50:-25:0|t Redeem 10x |cffb048f8[Badge of Justice]|r", GOSSIP_SENDER_MAIN, 6);
				player->ADD_GOSSIP_ITEM(1, "Nevermind.", GOSSIP_SENDER_MAIN, 3);
				player->PlayerTalkClass->SendGossipMenu(610000, me->GetGUID());
			}
		}
		else
		{
			if (player->GetAreaId() == 3486)
			{
				player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/INV_Misc_Bandana_01:50:50:-25:0|t Already leaving?", GOSSIP_SENDER_MAIN, 4);
				player->ADD_GOSSIP_ITEM(1, "Nevermind.", GOSSIP_SENDER_MAIN, 3);
				player->PlayerTalkClass->SendGossipMenu(610000, me->GetGUID());
			}
			else
			{
				player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/INV_Misc_Bandana_01:50:50:-25:0|t I see you're comitted.\nWould you like to Upgrade your rankings within the Ravenholdt?", GOSSIP_SENDER_MAIN, 2);
				player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/Spell_Holy_ChampionsBond:50:50:-25:0|t Redeem 10x |cffb048f8[Badge of Justice]|r", GOSSIP_SENDER_MAIN, 7);
				player->ADD_GOSSIP_ITEM(1, "Nevermind.", GOSSIP_SENDER_MAIN, 3);
				player->PlayerTalkClass->SendGossipMenu(610000, me->GetGUID());
			}
		}
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
	{
		switch (action)
		{
			case 1:
			player->TeleportTo(0, 1193.715698f, -264.133636f, 34.724926f, 5.048222f);
			break;

		case 2:
			player->TeleportTo(0, 23.538435f, -1591.401978f, 195.762009f, 4.713132f);
			break;

		case 3:
			player->CLOSE_GOSSIP_MENU();
				break;

		case 4:
			player->TeleportTo(0, -1537.694458f, -1861.948853f, 67.649536f, 5.294305f);
			break;

		case 5:
			player->TeleportTo(0, -1685.221802f, -1786.758911f, 84.311157f, 3.152790f);
			break;

		case 6:
			if (player->GetItemCount(29434) >= 10)
			{
				player->DestroyItemCount(29434, 10, true);
				player->GetReputationMgr().ModifyReputation(sFactionStore.LookupEntry(70), 750);
				player->CastSpell(player, 47292, true);
				player->PlayerTalkClass->ClearMenus();
				OnGossipHello(player, creature);
			}
			else
			{
				ChatHandler(player->GetSession()).PSendSysMessage("|cff53F06BYou don't have enough|r |cffb048f8[Badge of Honor]|r|cff53F06B.|r");
				player->PlayerTalkClass->ClearMenus();
				OnGossipHello(player, creature);
			}
			break;
		case 7:
			if (player->GetItemCount(29434) >= 10)
			{
				player->DestroyItemCount(29434, 10, true);
				player->GetReputationMgr().ModifyReputation(sFactionStore.LookupEntry(349), 750);
				player->CastSpell(player, 47292, true);
				player->PlayerTalkClass->ClearMenus();
				OnGossipHello(player, creature);
			}
			else
			{
				ChatHandler(player->GetSession()).PSendSysMessage("|cff53F06BYou don't have enough|r |cffb048f8[Badge of Honor]|r|cff53F06B.|r");
				player->PlayerTalkClass->ClearMenus();
				OnGossipHello(player, creature);
			}
			break;

		default:
			return false;
				break;
		}	
		return false;
	}
};	
void AddSC_tele()
{
	new tele();

}