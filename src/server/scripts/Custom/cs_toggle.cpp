#include "Define.h"
#include "GossipDef.h"
#include "Player.h"
#include "ScriptedGossip.h"
#include "ScriptMgr.h"
#include "QueryResult.h"
#include "Language.h"
#include "../game/Custom/CustomMgr.h"

// Set USE_TOKEN to 1 if you want to have it use tokens in place of gold
#define USE_TOKEN       0
#define TOKEN_ID        29434

struct BloodMoneyInfo
{
	ObjectGuid guid;
	uint32 amount;
	bool accepted;
};

typedef std::list<BloodMoneyInfo> BloodMoneyList;
typedef std::map<ObjectGuid, BloodMoneyList> BloodMoney;
static BloodMoney m_bloodMoney;

bool HasBloodMoneyChallenger(ObjectGuid playerGUID)
{
	return m_bloodMoney.find(playerGUID) != m_bloodMoney.end();
}

bool HasBloodMoneyChallenger(ObjectGuid targetGUID, ObjectGuid playerGUID)
{
	if (!HasBloodMoneyChallenger(targetGUID))
		return false;
	BloodMoneyList bml = m_bloodMoney[targetGUID];
	for (BloodMoneyList::const_iterator itr = bml.begin(); itr != bml.end(); ++itr)
		if (itr->guid == playerGUID)
			return true;
	return false;
}

void AddBloodMoneyEntry(ObjectGuid targetGUID, ObjectGuid playerGUID, uint32 amount)
{
	BloodMoneyInfo bmi;
	bmi.guid = playerGUID;
	bmi.amount = amount;
	bmi.accepted = false;
	m_bloodMoney[targetGUID].push_back(bmi);
}

void RemoveBloodMoneyEntry(ObjectGuid targetGUID, ObjectGuid playerGUID)
{
	if (!HasBloodMoneyChallenger(targetGUID, playerGUID))
		return;
	BloodMoneyList &list = m_bloodMoney[targetGUID];
	BloodMoneyList::iterator itr;
	for (itr = list.begin(); itr != list.begin(); ++itr)
		if (itr->guid == playerGUID)
			break;
	list.erase(itr);
}

void SetChallengeAccepted(ObjectGuid targetGUID, ObjectGuid playerGUID)
{
	if (!HasBloodMoneyChallenger(targetGUID, playerGUID))
		return;
	BloodMoneyList &list = m_bloodMoney[targetGUID];
	BloodMoneyList::iterator itr;
	for (itr = list.begin(); itr != list.end(); ++itr)
	{
		if (itr->guid == playerGUID)
		{
			itr->accepted = true;
			break;
		}
	}
}

static const uint32 max_gossip = 32;
static const uint32 max_options = max_gossip - 5;

bool validString4(const std::string& str)
{
	return !str.empty() && str.length() < 25 && str.find_first_of("\\\'\"\a\b\f\n\r\t\v\0") == std::string::npos;
}

// action, accountid, pageid
// sender = accountid
// melt = melt(action, pageid)
uint32 melt(uint8 action, uint8 pageid)
{
	return (pageid << 8) + action;
}

void unmelt(uint32 melt, uint8& action, uint8& pageid)
{
	pageid = (melt >> 8);
	action = (melt & 0xFF);
}

// returns true if sends a new gossip menu or closes gossip
bool showResults(Player* player, uint32 accountId, uint8 action, uint8 pageid)
{
	if (!accountId || accountId < 0)
	{
		player->GetSession()->SendNotification("You must enter a real account ID.");
		return false;
	}

	QueryResult result = WorldDatabase.PQuery("SELECT charName, itemName, itemId, price, amount, date FROM check_donation_purchases WHERE accId = '%u'", accountId);
	if (result)
	{
		if ((pageid + 1)*max_options > result->GetRowCount())
			pageid = (uint16)floorf((float)result->GetRowCount() / (float)max_options);
		for (uint32 i = 0; i < pageid; ++i)
			if (!result->NextRow())
				return false;

		char label[256];
		snprintf(label, 256, "|TInterface\\icons\\Achievement_Dungeon_GloryoftheHERO:30:30:-22:0|t Showing Records For ID: %u", accountId);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, label, 0, 5);

		char price[250];
		uint32 counter = 0;
		do
		{
			Field* fields = result->Fetch();
			std::string charName = fields[0].GetString();
			std::string itemName = fields[1].GetString();
			uint32 itemId = fields[2].GetUInt32();
			uint32 dpprice = fields[3].GetUInt32();
			uint32 amount = fields[4].GetUInt32();
			std::string date = fields[5].GetString();
			snprintf(price, 250, "|TInterface/ICONS/INV_Misc_Coin_02:20:20:-22:0|tChar: [%s] Has Bought:\nItem [%s|r]\nId: %u. Price: %u. Amount: %u.\nOn Date: %s", charName.c_str(), itemName.c_str(), itemId, dpprice, amount, date.c_str());
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, price, 0, 5);
			counter++;

			if (counter >= max_options)
				break;
		} while (result->NextRow());

		if (pageid == 0)
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Main menu", GOSSIP_SENDER_MAIN, 5);
		if (pageid > 0)
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Prev page", accountId, melt(action, pageid - 1));
		if ((pageid + 1)*max_options < result->GetRowCount())
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Next page", accountId, melt(action, pageid + 1));

		player->SEND_GOSSIP_MENU(20011, player->GetGUID());
		return true;
	}
	else
	{
		ChatHandler(player->GetSession()).PSendSysMessage("No records found for account ID %u before 12/08-2014.", accountId);
		player->CLOSE_GOSSIP_MENU();
		return true;
	}
}

// returns true if sends a new gossip menu or closes gossip
bool showResults(Player* player, const char* code, uint8 action, uint8 pageid)
{
	std::string name = std::string(code);
	QueryResult result = WorldDatabase.PQuery("SELECT itemName, itemId, price, amount, date, accId FROM check_donation_purchases WHERE charName = \"%s\"", name.c_str());
	if (result)
	{
		if ((pageid + 1)*max_options > result->GetRowCount())
			pageid = (uint16)floorf((float)result->GetRowCount() / (float)max_options);
		for (uint32 i = 0; i < pageid; ++i)
			if (!result->NextRow())
				return false;

		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Dungeon_GloryoftheHERO:30:30:-22:0|t Showing Records For: " + name, 0, 5);
		char price[250];
		uint32 counter = 0;
		uint32 accountId = 0;
		do
		{
			Field* fields = result->Fetch();
			std::string itemName = fields[0].GetString();
			uint32 itemId = fields[1].GetUInt32();
			uint32 dpprice = fields[2].GetUInt32();
			uint32 amount = fields[3].GetUInt32();
			std::string date = fields[4].GetString();
			accountId = fields[5].GetUInt32();
			snprintf(price, 250, "|TInterface/ICONS/INV_Misc_Coin_02:20:20:-22:0|tItem [%s|r]\nId: %u. Price: %u. Amount: %u.\nBought on: %s", itemName.c_str(), itemId, dpprice, amount, date.c_str());
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, price, 0, 5);
			counter++;

			if (counter >= max_options)
				break;
		} while (result->NextRow());

		if (pageid == 0)
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Main menu", GOSSIP_SENDER_MAIN, 5);
		if (pageid > 0)
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Prev page", accountId, melt(action, pageid - 1));
		if ((pageid + 1)*max_options < result->GetRowCount())
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Next page", accountId, melt(action, pageid + 1));

		player->SEND_GOSSIP_MENU(20011, player->GetGUID());
		return true;
	}
	else
	{
		ChatHandler(player->GetSession()).PSendSysMessage("No records found for character %s before 12/08-2014.", code);
		player->CLOSE_GOSSIP_MENU();
		return true;
	}
}

enum ArenaRankActionIds {
	ARENA_2V2_LADDER = GOSSIP_ACTION_INFO_DEF + 1,
	ARENA_3V3_LADDER = GOSSIP_ACTION_INFO_DEF + 2,
	ARENA_5V5_LADDER = GOSSIP_ACTION_INFO_DEF + 3,
	ARENA_GOODBYE = GOSSIP_ACTION_INFO_DEF + 4,
	ARENA_NOOP = 1,
	ARENA_START_TEAM_LOOKUP = GOSSIP_ACTION_INFO_DEF + 5,

};

enum ArenaRankOptions {
	ARENA_MAX_RESULTS = 30,
};

enum ArenaGossipText {
	ARENA_GOSSIP_HELLO = 11201,
	ARENA_GOSSIP_NOTEAMS = 11202,
	ARENA_GOSSIP_TEAMS = 11203,
	ARENA_GOSSIP_TEAM_LOOKUP = 11204,

};

typedef std::map<uint32, float> PlayerRankData;
static PlayerRankData playerranks;

// Load ranks if needed
static void LoadPlayerRanks()
{
	static time_t lastrun = 0;
	time_t curtime = time(NULL);
	if (lastrun && difftime(curtime, lastrun) < 600) // in seconds
		return;
	QueryResult result = CharacterDatabase.Query("SELECT guid, rankPoints FROM characters WHERE rankPoints >= 50");
	if (!result)
		return;
	lastrun = curtime;
	playerranks.clear();
	do
	{
		Field* fields = result->Fetch();
		playerranks[fields[0].GetUInt32()] += fields[1].GetFloat();
	} while (result->NextRow());
}

// functions to get all data or one guild's points
const PlayerRankData& GetPlayerRankData()
{
	LoadPlayerRanks();
	return playerranks;
}

float GetPlayerRankPoints(uint32 playerID)
{
	LoadPlayerRanks();
	PlayerRankData::const_iterator it = playerranks.find(playerID);
	if (it == playerranks.end())
		return 0.0f;
	return it->second;
}

class class_update : public BasicEvent
{
public:
	class_update(Player* player) : _player(player) { }

	bool Execute(uint64 /*time*/, uint32 /*diff*/)
	{
		if (_player && _player->GetSession())
			_player->PlayerTalkClass->GetGossipMenu().SetMenuId(31);
		return true;
	}

private:
	Player* _player;
};

class Devcommandscript : public CommandScript
{
public:
	Devcommandscript() : CommandScript("Devcommandscript") { }

	ChatCommand* GetCommands() const
	{
		static ChatCommand boosterCommandTable[] =
		{
			//{  "add",        SEC_ADMINISTRATOR,     false,    &HandleAddBoostCommand,         "", NULL },
			{ "info", rbac::RBAC_PERM_COMMAND_SERVER_EXIT, false, &HandleGetBoostInfoCommand, "", NULL },
			{ NULL, 0, false, NULL, "", NULL }
		};
		static ChatCommand checkDPCommandTable[] =
		{
			{ "purchases", rbac::RBAC_PERM_COMMAND_SERVER_EXIT, false, &HandleCheckDPCommand, "", NULL },
			{ NULL, 0, false, NULL, "", NULL }
		};
		static ChatCommand checkCommandTable[] =
		{
			{ "dp", rbac::RBAC_PERM_COMMAND_SERVER_EXIT, false, NULL, "", checkDPCommandTable },
			{ NULL, 0, false, NULL, "", NULL }
		};
		static ChatCommand commandTable[] =
		{
			{ "scoreboard", rbac::RBAC_PERM_COMMAND_HELP, false, &HandleGossipCommand, "", NULL },
			//{ "betduel", rbac::RBAC_PERM_COMMAND_HELP, false, &HandleBetDuelCommand, "", NULL },
			{ "booster", rbac::RBAC_PERM_COMMAND_SERVER_EXIT, false, NULL, "", boosterCommandTable },
			{ "castall", rbac::RBAC_PERM_COMMAND_SERVER_EXIT, false, &HandleCastAllCommand, "", NULL },
			{ "testreset", rbac::RBAC_PERM_COMMAND_SERVER_EXIT, false, &HandleTestResetCommand, "", NULL },
			{ "check", rbac::RBAC_PERM_COMMAND_SERVER_EXIT, false, NULL, "", checkCommandTable },
			{ NULL, 0, false, NULL, "", NULL }
		};
		return commandTable;
	}


	static bool HandleTestResetCommand(ChatHandler* handler, const char *args)
	{
		sWorld->ResetAndRewardPvP();
		return true;
	}

	static bool HandleCastAllCommand(ChatHandler* handler, const char *args)
	{
		if (!*args)
			return false;

		Player* player = handler->GetSession()->GetPlayer();
		if (player->GetSession()->GetSecurity() < 3)
		{
			handler->SendSysMessage("You do not have permissions to use this command.");
			handler->SetSentErrorMessage(true);
			return false;
		}

		// number or [name] Shift-click form |color|Hspell:spell_id|h[name]|h|r or Htalent form
		uint32 spell = handler->extractSpellIdFromLink((char*)args);
		if (!spell || !sSpellMgr->GetSpellInfo(spell))
		{
			handler->PSendSysMessage(LANG_COMMAND_NOSPELLFOUND);
			handler->SetSentErrorMessage(true);
			return false;
		}

		char* trig_str = strtok(NULL, " ");
		if (trig_str)
		{
			int l = strlen(trig_str);
			if (strncmp(trig_str, "triggered", l) != 0)
				return false;
		}

		bool triggered = (trig_str != NULL);

		boost::shared_lock<boost::shared_mutex> lock(*HashMapHolder<Player>::GetLock());
		HashMapHolder<Player>::MapType const& m = sObjectAccessor->GetPlayers();
		for (HashMapHolder<Player>::MapType::const_iterator itr = m.begin(); itr != m.end(); ++itr)
			itr->second->CastSpell(itr->second, spell, triggered);
		return true;
	}

	static bool HandleGossipCommand(ChatHandler* handler, const char* /*args*/) // gossip1
	{
		// This triggers the //OnGossipHello hook for player. You can use it pretty much anywhere to trigger gossip
		sScriptMgr->OnGossipSelect(handler->GetSession()->GetPlayer(), 1, 0, 10000);
		return true;
	}

	static bool HandleBetDuelCommand(ChatHandler* handler, const char* /*args*/) // gossip1
	{
		//sScriptMgr->//OnGossipHello(handler->GetSession()->GetPlayer(), 10);
		return true;
	}

	static bool HandleCheckDPCommand(ChatHandler* handler, const char* /*args*/)
	{
		sScriptMgr->OnGossipSelect(handler->GetSession()->GetPlayer(), 60, 0, 0);
		return true;
	}

	static bool HandleGetBoostInfoCommand(ChatHandler* handler, const char* args)
	{
		std::string name;
		Player* player;
		char const* TargetName = strtok((char*)args, " ");
		if (!TargetName)
		{
			player = handler->getSelectedPlayer();
			if (player)
			{
				name = player->GetName();
				normalizePlayerName(name);
			}
		}
		else
		{
			name = TargetName;
			normalizePlayerName(name);
			player = sObjectAccessor->FindPlayerByName(name);
		}
		if (!player)
		{
			handler->PSendSysMessage("No player with that name was found.");
			handler->SetSentErrorMessage(true);
			return false;
		}

		if (player->m_BoostContainer.empty())
		{
			handler->PSendSysMessage("The target %s has no active boosters.", player->GetName().c_str());
			return true;
		}

		for (Player::BoostContainer::const_iterator itr = player->m_BoostContainer.begin(); itr != player->m_BoostContainer.end(); ++itr)
			handler->PSendSysMessage("%s, Expiration: %s, Modifier: %f.", player->GetBoosterName(itr->second->boost_type).c_str(), secsToTimeString(player->GetRemainingBoostTime(itr->second->startdate, itr->second->unsetdate)).c_str(), itr->second->boost_modifier);

		return true;
	}
};

class custom_PlayerGossip : public PlayerScript
{
private:
	std::string getPlayerStatus(std::string code)
	{
		Player *player = sObjectAccessor->FindPlayerByName(code);
		if (!player)
			return "Offline";
		if (player->isAFK())
			return "Online, <AFK> " + player->autoReplyMsg;
		if (player->isDND())
			return "Online, <Busy> " + player->autoReplyMsg;
		return "Online";
	}

	std::string getPlayerStatusByGuid(ObjectGuid guid)
	{
		Player *player = sObjectAccessor->FindPlayer(guid);
		if (!player)
			return "Offline";
		if (player->isAFK())
			return "Online, <AFK> " + player->autoReplyMsg;
		if (player->isDND())
			return "Online, <Busy> " + player->autoReplyMsg;
		return "Online";
	}

	uint32 optionToTeamType(uint32 option)
	{
		uint32 teamType;
		switch (option) {
		case ARENA_2V2_LADDER: teamType = 2; break;
		case ARENA_3V3_LADDER: teamType = 3; break;
		case ARENA_5V5_LADDER: teamType = 1; break;
		default: teamType = 1; break;
		}
		return teamType;
	}

	uint32 teamTypeToOption(uint32 teamType)
	{
		uint32 option;
		switch (teamType) {
		case 2: option = ARENA_2V2_LADDER; break;
		case 3: option = ARENA_3V3_LADDER; break;
		case 1: option = ARENA_5V5_LADDER; break;
		default: option = ARENA_5V5_LADDER; break;
		}
		return option;
	}

	std::string raceToString(uint8 race)
	{
		std::string race_s = "Unknown";
		switch (race)
		{
		case RACE_HUMAN:            race_s = "Human";       break;
		case RACE_ORC:              race_s = "Orc";         break;
		case RACE_DWARF:            race_s = "Dwarf";       break;
		case RACE_NIGHTELF:         race_s = "Night Elf";   break;
		case RACE_UNDEAD_PLAYER:    race_s = "Undead";      break;
		case RACE_TAUREN:           race_s = "Tauren";      break;
		case RACE_GNOME:            race_s = "Gnome";       break;
		case RACE_TROLL:            race_s = "Troll";       break;
		case RACE_BLOODELF:         race_s = "Blood Elf";   break;
		case RACE_DRAENEI:          race_s = "Draenei";     break;
		default:                    race_s = "unknown";
		}
		return race_s;
	}

	std::string classToString(uint8 Class)
	{
		std::string Class_s = "Unknown";
		switch (Class)
		{
		case CLASS_WARRIOR:         Class_s = "Warrior";        break;
		case CLASS_PALADIN:         Class_s = "Paladin";        break;
		case CLASS_HUNTER:          Class_s = "Hunter";         break;
		case CLASS_ROGUE:           Class_s = "Rogue";          break;
		case CLASS_PRIEST:          Class_s = "Priest";         break;
		case CLASS_DEATH_KNIGHT:    Class_s = "Death Knight";   break;
		case CLASS_SHAMAN:          Class_s = "Shaman";         break;
		case CLASS_MAGE:            Class_s = "Mage";           break;
		case CLASS_WARLOCK:         Class_s = "Warlock";        break;
		case CLASS_DRUID:           Class_s = "Druid";          break;
		}
		return Class_s;
	}

	std::string getWinPercent(uint32 wins, uint32 losses)
	{
		uint32 totalGames = wins + losses;
		if (totalGames == 0)
			return "0%";

		std::stringstream buf;
		uint32 percentage = (wins * 100) / totalGames;
		buf << percentage << "%";
		return buf.str();
	}

public:
	custom_PlayerGossip() : PlayerScript("custom_PlayerGossip") { }

	/*void //OnGossipHello(Player* player, uint32 menu_id) override
	{
		switch (menu_id) // If menu_id is 900, send our gossip menu
		{
		case 1:
		{
			
		}
		break;
		case 10:
		{
			player->PlayerTalkClass->ClearMenus();

			if (player->duel || player->IsInCombat())
			{
				ChatHandler(player->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou can not access the betting table while dueling or in combat.");
				player->CLOSE_GOSSIP_MENU();
				return;
			}
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "|TInterface\\icons\\Ability_paladin_artofwar:35:35:-22:0|tChallenge a player!", GOSSIP_SENDER_MAIN, 2500, "Enter the amount of gold you wish to bet : \n", 0, true);
			if (HasBloodMoneyChallenger(player->GetGUID()))
			{
				BloodMoneyList list = m_bloodMoney[player->GetGUID()];
				for (BloodMoneyList::const_iterator itr = list.begin(); itr != list.end(); ++itr)
				{
					char msg[50];

					if (Player* plr = Player::GetPlayer(*player, itr->guid))
					{
						if (USE_TOKEN)
						{
							sprintf(msg, "Accept %s's Challenge of %d tokens", plr->GetName().c_str(), itr->amount);
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, msg, 2501, itr->guid);
							sprintf(msg, "Decline %s's Challenge of %d tokens", plr->GetName().c_str(), itr->amount);
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, msg, 2502, itr->guid);
						}
						else
						{
							sprintf(msg, "Accept %s's Challenge of %dg", plr->GetName().c_str(), itr->amount / 10000);
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, msg, 2501, itr->guid);
							sprintf(msg, "Decline %s's Challenge of %dg", plr->GetName().c_str(), itr->amount / 10000);
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, msg, 2502, itr->guid);
						}

					}
				}
			}
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|tNevermind", GOSSIP_SENDER_MAIN, 50);
			player->PlayerTalkClass->GetGossipMenu().SetMenuId(10);
			player->SEND_GOSSIP_MENU(80025, player->GetGUID());
		}
		break;
		case 60:
		{
			
		}
		break;
		default:
			player->CLOSE_GOSSIP_MENU();
			break;
		}

	}*/

	void OnGossipSelect(Player* player, uint32 menu_id, uint32 sender, uint32 _action)
	{
		player->PlayerTalkClass->ClearMenus();
		if (player->IsInCombat())
		{
			ChatHandler(player->GetSession()).PSendSysMessage("You can not use the Help Desk when in combat.");
			player->PlayerTalkClass->SendCloseGossip();
			return;
		}

		if (menu_id == 1)
		{
			switch (_action)
			{
			case 10000:
			{
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Quests_Completed_08:35:35:-22:0|t - V-Quality Scoreboard -", GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface/ICONS/INV_Misc_Book_11:35:35:-22:0|t - How It Works -", GOSSIP_SENDER_MAIN, 6);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------", GOSSIP_SENDER_MAIN, 5);
				if (player->GetRankPoints() < 1000)
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Quests_Completed_07:35:35:-22:0|t My Scoreboard.", GOSSIP_SENDER_MAIN, 1);
				else
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Quests_Completed_08:35:35:-22:0|t My Scoreboard.", GOSSIP_SENDER_MAIN, 1);
				/*
				if (player->GetGuild())
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_PVP_P_14:35:35:-22:0|t My Guild Scoreboard.", GOSSIP_SENDER_MAIN, 2);
				else
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|t Guild Scoreboard Unavailable.", GOSSIP_SENDER_MAIN, 4);*/

				player->ADD_GOSSIP_ITEM_EXTENDED(10, "|TInterface\\icons\\Ability_Hunter_MasterMarksman:35:35:-22:0|t Search Scoreboard.", GOSSIP_SENDER_MAIN, 10, "Enter Playername", 0, true);
				//player->ADD_GOSSIP_ITEM_EXTENDED(10, "|TInterface\\icons\\Ability_Hunter_MasterMarksman:35:35:-22:0|t Search Guild Scoreboard.", GOSSIP_SENDER_MAIN, 11, "Enter Guildname", 0, true);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------", GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_PVP_A_06:35:35:-22:0|t1v1 Rankings", GOSSIP_SENDER_MAIN, ARENA_5V5_LADDER);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_PVP_A_07:35:35:-22:0|t2v2 Rankings", GOSSIP_SENDER_MAIN, ARENA_2V2_LADDER);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_PVP_A_08:35:35:-22:0|t3v3 Rankings", GOSSIP_SENDER_MAIN, ARENA_3V3_LADDER);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------", GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|t Close Scoreboard.", GOSSIP_SENDER_MAIN, 3);
				player->PlayerTalkClass->GetGossipMenu().SetMenuId(1);
				player->SEND_GOSSIP_MENU(65004, player->GetGUID());
			}
				break;
			case ARENA_GOODBYE:
				player->PlayerTalkClass->SendCloseGossip();
				break;
			case ARENA_2V2_LADDER:
			case ARENA_5V5_LADDER:
			case ARENA_3V3_LADDER:
			{
				uint32 teamType = optionToTeamType(_action);
				QueryResult result = CharacterDatabase.PQuery(
					"SELECT arenaTeamId, name, rating, seasonWins, seasonGames - seasonWins "
					"FROM arena_team WHERE type = '%u' ORDER BY rating DESC LIMIT %u;", teamType, ARENA_MAX_RESULTS
					);

				if (!result)
				{
					player->GetSession()->SendNotification("There are no Arena Teams of that type.");
					//OnGossipHello(player, menu_id); // main menu
					return;
				}
				else
				{
					//uint64 rowCount = result->GetRowCount();
					std::string name;
					uint32 teamId, rating, seasonWins, seasonLosses, rank = 1;
					do {
						Field *fields = result->Fetch();
						teamId = fields[0].GetUInt32();
						name = fields[1].GetString();
						rating = fields[2].GetUInt32();
						seasonWins = fields[3].GetUInt32();
						seasonLosses = fields[4].GetUInt32();

						std::stringstream buffer;
						buffer << rank << ". [" << rating << "] " << name;
						buffer << " (" << seasonWins << "-" << seasonLosses << ")";
						player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, buffer.str(), GOSSIP_SENDER_MAIN, ARENA_START_TEAM_LOOKUP + teamId);

						rank++;
					} while (result->NextRow());

					player->SEND_GOSSIP_MENU(ARENA_GOSSIP_TEAMS, player->GetGUID());
				}
				break;
			}
			default:
			{
				if (_action > ARENA_START_TEAM_LOOKUP)
				{
					uint32 teamId = _action - ARENA_START_TEAM_LOOKUP;

					// lookup team
					QueryResult result = CharacterDatabase.PQuery(
						//      0     1       2           3
						"SELECT name, rating, seasonWins, seasonGames - seasonWins, "
						//  4      5                     6     7             8
						"weekWins, weekGames - weekWins, rank, captainGuid , type "
						"FROM arena_team WHERE arenaTeamId = '%u'", teamId);

					// no team found
					if (!result) {
						player->GetSession()->SendNotification("Arena team not found...");
						player->PlayerTalkClass->SendCloseGossip();
						return;
					}

					// populate the results
					Field *fields = result->Fetch();
					std::string name = fields[0].GetString();
					uint32 rating = fields[1].GetUInt32();
					uint32 seasonWins = fields[2].GetUInt32();
					uint32 seasonLosses = fields[3].GetUInt32();
					uint32 weekWins = fields[4].GetUInt32();
					uint32 weekLosses = fields[5].GetUInt32();
					uint32 rank = fields[6].GetUInt32();
					uint32 captainGuid = fields[7].GetUInt32();
					uint32 type = fields[8].GetUInt32();
					uint32 parentOption = teamTypeToOption(type);

					std::string seasonWinPercentage = getWinPercent(seasonWins, seasonLosses);
					std::string weekWinPercentage = getWinPercent(weekWins, weekLosses);

					std::stringstream buf;
					buf << "Team Name: " << name;
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, buf.str(), GOSSIP_SENDER_MAIN, parentOption);
					buf.str("");
					buf << "Rating: " << rating << " (rank " << rank << ", bracket " << type << "v" << type << ")";
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, buf.str(), GOSSIP_SENDER_MAIN, parentOption);
					buf.str("");
					buf << "Total Week: " << weekWins << "-" << weekLosses << " (" << weekWinPercentage << " win), " << (weekWins + weekLosses) << " played";
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, buf.str(), GOSSIP_SENDER_MAIN, parentOption);
					buf.str("");
					buf << "Total Season: " << seasonWins << "-" << seasonLosses << " (" << seasonWinPercentage << " win), " << (seasonWins + seasonLosses) << " played";
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, buf.str(), GOSSIP_SENDER_MAIN, parentOption);

					QueryResult members = CharacterDatabase.PQuery("SELECT  a.guid, a.personalRating, a.weekWins, a.weekGames - a.weekWins, a.seasonWins, a.seasonGames - a.seasonWins, c.name, c.race, c.class, c.level FROM arena_team_member a LEFT JOIN characters c ON c.guid = a.guid WHERE arenaTeamId = '%u' ORDER BY a.guid = '%u' DESC, a.seasonGames DESC, c.name ASC", teamId, captainGuid);
					if (!members) {
						player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "No team members found...?", GOSSIP_SENDER_MAIN, parentOption);
					}
					else {
						uint32 memberPos = 1;
						uint32 memberCount = members->GetRowCount();
						uint64 guid;
						uint32 personalRating;
						uint32 level;
						std::string name, race, Class;

						buf.str("");
						buf << memberCount << " team " << ((memberCount == 1) ? "member" : " members") << " found:";
						player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, buf.str(), GOSSIP_SENDER_MAIN, parentOption);

						do {
							// populate fields
							fields = members->Fetch();
							guid = fields[0].GetUInt64();
							personalRating = fields[1].GetUInt32();
							weekWins = fields[2].GetUInt32();
							weekLosses = fields[3].GetUInt32();
							seasonWins = fields[4].GetUInt32();
							seasonLosses = fields[5].GetUInt32();
							name = fields[6].GetString();
							race = raceToString(fields[7].GetUInt8());
							Class = classToString(fields[8].GetUInt8());
							level = fields[9].GetUInt32();

							seasonWinPercentage = getWinPercent(seasonWins, seasonLosses);
							weekWinPercentage = getWinPercent(weekWins, weekLosses);

							// TODO: add output
							buf.str(""); // clear it
							buf << memberPos << ". ";
							if (guid == captainGuid)
								buf << "Team Captain ";
							buf << name << ", " << getPlayerStatusByGuid(ObjectGuid(guid));

							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, buf.str(), GOSSIP_SENDER_MAIN, parentOption);
							buf.str("");
							buf << "Level " << level << " " << race << " " << Class << ", " << personalRating << " personal rating.";
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, buf.str(), GOSSIP_SENDER_MAIN, parentOption);
							buf.str("");
							buf << "Week: " << weekWins << "-" << weekLosses << " (" << weekWinPercentage << " win), " << (weekWins + weekLosses) << " played";
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, buf.str(), GOSSIP_SENDER_MAIN, parentOption);
							buf.str("");
							buf << "Season: " << seasonWins << "-" << seasonLosses << " (" << seasonWinPercentage << " win), " << (seasonWins + seasonLosses) << " played";
							player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, buf.str(), GOSSIP_SENDER_MAIN, parentOption);
							memberPos++;
						} while (members->NextRow());

					}

					buf.str("");
					buf << "Return to " << type << "v" << type << " rankings!";
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, buf.str(), GOSSIP_SENDER_MAIN, parentOption);
					player->SEND_GOSSIP_MENU(ARENA_GOSSIP_TEAM_LOOKUP, player->GetGUID());
				}
			}
			break;
			}
			if (_action == 1)
			{
				// populate field
				std::ostringstream level;
				uint8 race;
				uint8 Class;
				uint8 Gender;
				uint32 mapId;
				uint32 areaId;
				std::string Guild;
				std::ostringstream Rank;
				std::ostringstream TopRank;
				std::ostringstream TopRankG;
				std::ostringstream KillsLifetime;
				std::ostringstream KillsToday;
				std::string name;

				if (player) {
					level << (uint32)player->getLevel();
					race = player->getRace();
					Class = player->getClass();
					Gender = player->getGender();
					mapId = player->GetMapId();
					areaId = player->GetAreaId();
					name = player->GetName();
					Rank << "|TInterface\\icons\\Achievement_worldevent_lunar:27:28:-22:0|tRankpoints: " << (uint32)player->GetRankPoints() << ".";
					TopRank << "|TInterface\\icons\\Achievement_worldevent_lunar:27:28:-22:0|tTop Rankpoints: " << (uint32)player->GetTopRankPoints() << ".";
					KillsLifetime << (uint32)player->GetUInt32Value(PLAYER_FIELD_LIFETIME_HONORABLE_KILLS);
					KillsToday << PAIR32_LOPART(player->GetUInt32Value(PLAYER_FIELD_KILLS));
					if (player->GetGuild())
						Guild = player->GetGuildName();
					else
						Guild = "No Guild";
				}
				else
					return;

				std::string race_s, Class_s, Gender_s;
				switch (race)
				{
				case RACE_HUMAN:            race_s = "Human";       break;
				case RACE_ORC:              race_s = "Orc";         break;
				case RACE_DWARF:            race_s = "Dwarf";       break;
				case RACE_NIGHTELF:         race_s = "Night Elf";   break;
				case RACE_UNDEAD_PLAYER:    race_s = "Undead";      break;
				case RACE_TAUREN:           race_s = "Tauren";      break;
				case RACE_GNOME:            race_s = "Gnome";       break;
				case RACE_TROLL:            race_s = "Troll";       break;
				case RACE_BLOODELF:         race_s = "Blood Elf";   break;
				case RACE_DRAENEI:          race_s = "Draenei";     break;
				default:                    race_s = "unknown";
				}
				switch (Class)
				{
				case CLASS_WARRIOR:         Class_s = "Warrior";        break;
				case CLASS_PALADIN:         Class_s = "Paladin";        break;
				case CLASS_HUNTER:          Class_s = "Hunter";         break;
				case CLASS_ROGUE:           Class_s = "Rogue";          break;
				case CLASS_PRIEST:          Class_s = "Priest";         break;
				case CLASS_DEATH_KNIGHT:    Class_s = "Death Knight";   break;
				case CLASS_SHAMAN:          Class_s = "Shaman";         break;
				case CLASS_MAGE:            Class_s = "Mage";           break;
				case CLASS_WARLOCK:         Class_s = "Warlock";        break;
				case CLASS_DRUID:           Class_s = "Druid";          break;
				default:                    Class_s = "unknown";
				}
				switch (Gender)
				{
				case GENDER_MALE:           Gender_s = "Male";          break;
				case GENDER_FEMALE:         Gender_s = "Female";        break;
				default:                    Gender_s = "unknown";       break;
				}

				const PlayerRankData& rankdata = GetPlayerRankData();
				std::vector<std::pair<float, uint32> > items;
				// The old loop
				for (PlayerRankData::const_iterator it = rankdata.begin(); it != rankdata.end(); ++it)
				{
					uint32 charguid = it->first;
					float points = it->second;
					items.push_back(std::make_pair(points, charguid));
				}
				std::sort(items.begin(), items.end());
				uint32 count = 0;
				uint32 searched_playerid = player->GetGUIDLow();
				for (std::vector<std::pair<float, uint32> >::const_reverse_iterator it = items.rbegin(); it != items.rend(); ++it)
				{
					++count;
					if ((*it).second == searched_playerid)
					{
						TopRankG << "|TInterface\\icons\\Achievement_worldevent_lunar:27:28:-22:0|tBoard Rank: " << count << " of " << items.size() << " Players. ";
						break;
					}
				}

				std::string playedTime = secsToTimeString(player->GetTotalPlayedTime());

				int locale = player->GetSession()->GetSessionDbcLocale();
				std::string areaName = "<unknown>";
				std::string mapName = "<unknown>";

				if (MapEntry const* map = sMapStore.LookupEntry(mapId))
					mapName = map->name[locale];

				AreaTableEntry const* area = GetAreaEntryByAreaID(areaId);
				if (area)
					areaName = area->area_name[locale];

				if (player->GetRankPoints() < 1000)
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Quests_Completed_07:35:35:-22:0|tScoreboard Information.", GOSSIP_SENDER_MAIN, 1);
				else
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Quests_Completed_08:35:35:-22:0|tScoreboard Information.", GOSSIP_SENDER_MAIN, 1);
				if (player->GetSession()->IsPremium())
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_BG_trueAVshutout:35:35:-22:0|t* VIP Member *", GOSSIP_SENDER_MAIN, 1);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------", GOSSIP_SENDER_MAIN, 1);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Dungeon_Outland_Dungeon_Hero:35:35:-22:0|tPlayer Information.", GOSSIP_SENDER_MAIN, 1);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tName: " + name, GOSSIP_SENDER_MAIN, 1);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tGuild: " + Guild, GOSSIP_SENDER_MAIN, 1);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tLevel: " + level.str(), GOSSIP_SENDER_MAIN, 1);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tRace: " + race_s, GOSSIP_SENDER_MAIN, 1);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tClass: " + Class_s, GOSSIP_SENDER_MAIN, 1);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tKills Today: " + KillsToday.str(), GOSSIP_SENDER_MAIN, 1);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tLifetime Kills: " + KillsLifetime.str(), GOSSIP_SENDER_MAIN, 1);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------", GOSSIP_SENDER_MAIN, 2);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_dungeon_utgardepinnacle_normal:35:35:-22:0|t - World Ranking Leaderboard - ", GOSSIP_SENDER_MAIN, 8);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, Rank.str(), GOSSIP_SENDER_MAIN, 8);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, TopRank.str(), GOSSIP_SENDER_MAIN, 8);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, TopRankG.str(), GOSSIP_SENDER_MAIN, 8);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------", GOSSIP_SENDER_MAIN, 1);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_bg_grab_cap_flagunderxseconds:32:32:-22:0|tTotal Playedtime:\n \n " + playedTime, GOSSIP_SENDER_MAIN, 1);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------", GOSSIP_SENDER_MAIN, 1);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Zone_ArathiHighlands_01:31:32:-22:0|tLocation Information:", GOSSIP_SENDER_MAIN, 1);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Zone_Northrend_01:27:27:-22:0|tMap: " + mapName, GOSSIP_SENDER_MAIN, 1);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Zone_Kalimdor_01:27:27:-22:0|tArea: " + areaName, GOSSIP_SENDER_MAIN, 1);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------", GOSSIP_SENDER_MAIN, 1);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Holy_DevineAegis:30:30:-22:0|tBack..", GOSSIP_SENDER_MAIN, 5);
				player->SEND_GOSSIP_MENU(65004, player->GetGUID());
				return;
			}
			else if (_action == 3)
			{
				player->PlayerTalkClass->SendCloseGossip();
				return;
			}
			else if (_action == 5)
			{
				//OnGossipHello(player, 1);
			}
			else if (_action == 6)
			{
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Holy_DevineAegis:35:35:-22:0|t <- Main Menu.", GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Shadow_SacrificialShield:35:35:-22:0|t Close Scoreboard.", GOSSIP_SENDER_MAIN, 3);
				player->PlayerTalkClass->SendGossipMenu(65005, player->GetGUID());
			}
			else if (_action == 8)
			{
				ChatHandler(player->GetSession()).PSendSysMessage("The Board Rank is where players are placed against each other. The player with the highest Rankpoints has the lowest ranking, which is the best.");
				OnGossipSelect(player, 1, GOSSIP_SENDER_MAIN, 1);
			}
			else if (_action == 10)
			{
				ChatHandler(player->GetSession()).PSendSysMessage("The Board Rank is where players are placed against each other. The player with the highest Rankpoints has the lowest ranking, which is the best.");
				//OnGossipHello(player, 1);
			}
		}
		else if (menu_id == 10)
		{
			if (sender == 2501)
			{
				if (Player* target = Player::GetPlayer(*player, ObjectGuid(uint64(_action))))
				{
					SetChallengeAccepted(player->GetGUID(), target->GetGUID());
					ChatHandler(target->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFF%s has accepted your challenge!", player->GetName().c_str());
					ChatHandler(player->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou have accepted %s's challenge!", target->GetName().c_str());
					player->CLOSE_GOSSIP_MENU();
				}
			}
			else if (sender == 2502)
			{
				if (Player* target = Player::GetPlayer(*player, ObjectGuid(uint64(_action))))
				{
					ChatHandler(target->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFF%s has declined your challenge!", player->GetName().c_str());
					ChatHandler(player->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou have declined %s challenge!", target->GetName().c_str());
					RemoveBloodMoneyEntry(player->GetGUID(), ObjectGuid(uint64(_action)));
					//OnGossipHello(player, menu_id);
				}
			}
			else if (_action == 50)
			{
				player->CLOSE_GOSSIP_MENU();
				return;
			}
		}
		else if (menu_id == 60)
		{
			player->PlayerTalkClass->ClearMenus();

			if (_action == 10000)
			{
				player->PlayerTalkClass->ClearMenus();

				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Dungeon_GloryoftheHERO:35:35:-22:0|t - Check For Donations -", GOSSIP_SENDER_MAIN, 4);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------\n", GOSSIP_SENDER_MAIN, 4);
				player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_FeatsOfStrength_Gladiator_06:35:35:-22:0|t Insert Character Name:", GOSSIP_SENDER_MAIN, 1, "Enter Playername:", 0, true);
				player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Dungeon_NexusRaid_25man:35:35:-22:0|t Insert Account ID:", GOSSIP_SENDER_MAIN, 2, "Enter Account Id:", 0, true);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Dungeon_UtgardePinnacle_25man:35:35:-22:0|t Close.", GOSSIP_SENDER_MAIN, 4);
				player->PlayerTalkClass->GetGossipMenu().SetMenuId(60);
				player->SEND_GOSSIP_MENU(20010, player->GetGUID());
			}

			uint8 pageid, action;
			unmelt(_action, action, pageid);

			if (_action == 4)
			{
				player->CLOSE_GOSSIP_MENU();
			}
			else if (action == 1 || action == 2)
			{
				//if (!showResults(player, sender, action, pageid))
					//OnGossipHello(player, menu_id);
			}
			else if (action == 5)
			{
				//OnGossipHello(player, 60);
				return;
			}
			else if (_action == 5)
			{
				//OnGossipHello(player, 60);
				return;
			}
		}
		else if (menu_id == 70)
		{
			player->PlayerTalkClass->ClearMenus();

			if (_action == 1000)
			{
				//OnGossipHello(player, 70);
				return;
			}
			else if (_action == 1002)
			{
				OnGossipSelect(player, 70, 0, 1);
				return;
			}
			else if (_action == 1001)
			{
				player->CLOSE_GOSSIP_MENU();
				return;
			}
		}
	}

	void OnGossipSelectCode(Player* player, uint32 menu_id, uint32 sender, uint32 _action, const char* code)
	{
		player->PlayerTalkClass->ClearMenus();

		if (menu_id == 1)
		{
			if (_action == 10)
			{
				if (!validString4(code))
				{
					ChatHandler(player->GetSession()).PSendSysMessage("Please enter a real message.");
					return;
				}
				// populate field
				std::ostringstream level;
				uint8 race = 1;
				uint8 Class = 1;
				uint8 Gender = 0;
				uint32 mapId;
				uint32 areaId;
				uint32 guid = 0;
				std::string name;
				std::string SecondaryClass;
				bool online;
				std::string Guild = "No Guild.";
				std::ostringstream Rank;
				std::ostringstream TopRank;
				std::ostringstream TopRankG;
				std::string playedTime;
				std::ostringstream KillsLifetime;
				std::ostringstream KillsToday;

				Player* target = sObjectAccessor->FindPlayerByName(code);
				if (target) {
					guid = target->GetGUIDLow();
					level << (uint32)target->getLevel();
					race = target->getRace();
					Class = target->getClass();
					mapId = target->GetMapId();
					areaId = target->GetAreaId();
					name = target->GetName();
					Rank << (uint32)target->GetRankPoints();
					TopRank << (uint32)target->GetRankPoints();
					playedTime = secsToTimeString(target->GetTotalPlayedTime());
					KillsLifetime << (uint32)target->GetUInt32Value(PLAYER_FIELD_LIFETIME_HONORABLE_KILLS);
					KillsToday << (uint32)PAIR32_LOPART(target->GetUInt32Value(PLAYER_FIELD_KILLS));
					Gender = target->getGender();
					if (target->GetGuild())
						Guild = target->GetGuildName();
					else
						Guild = "No Guild";

					online = true;
				}
				else {
					std::string nameStr = std::string(code);
					std::transform(nameStr.begin(), nameStr.end(), nameStr.begin(), ::tolower);
					if (nameStr.begin() != nameStr.end())
						nameStr[0] = toupper(nameStr[0]);
					//                                                     0       1     2     3     4    5    6           7              8
					QueryResult result = CharacterDatabase.PQuery("SELECT level, race, class, gender, map, zone, name, totalKills, todayKills, RankPoints, TopRankPoints, totaltime, guid FROM characters WHERE name like '%s'",
						nameStr.c_str());
					if (!result)
					{
						player->GetSession()->SendNotification("No player with name %s exists", nameStr.c_str());
						//OnGossipHello(player, menu_id); // main menu
						return;
					}

					Field *fields = result->Fetch();
					level << (uint32)fields[0].GetUInt8();
					race = fields[1].GetUInt8();
					Class = fields[2].GetUInt8();
					Gender = fields[3].GetUInt8();
					mapId = fields[4].GetUInt16();
					areaId = fields[5].GetUInt16();
					name = fields[6].GetString();
					KillsLifetime << (uint32)fields[7].GetUInt32();
					KillsToday << (uint32)fields[8].GetUInt32();
					Rank << (uint32)fields[9].GetUInt16();
					TopRank << (uint32)fields[10].GetUInt16();
					playedTime = secsToTimeString(fields[11].GetUInt32());
					guid = fields[12].GetUInt32();
					online = false;
				}
				std::string race_s, Class_s, Gender_s;
				switch (race)
				{
				case RACE_HUMAN:            race_s = "Human";       break;
				case RACE_ORC:              race_s = "Orc";         break;
				case RACE_DWARF:            race_s = "Dwarf";       break;
				case RACE_NIGHTELF:         race_s = "Night Elf";   break;
				case RACE_UNDEAD_PLAYER:    race_s = "Undead";      break;
				case RACE_TAUREN:           race_s = "Tauren";      break;
				case RACE_GNOME:            race_s = "Gnome";       break;
				case RACE_TROLL:            race_s = "Troll";       break;
				case RACE_BLOODELF:         race_s = "Blood Elf";   break;
				case RACE_DRAENEI:          race_s = "Draenei";     break;
				default:                    race_s = "unknown";
				}
				switch (Class)
				{
				case CLASS_WARRIOR:         Class_s = "Warrior";        break;
				case CLASS_PALADIN:         Class_s = "Paladin";        break;
				case CLASS_HUNTER:          Class_s = "Hunter";         break;
				case CLASS_ROGUE:           Class_s = "Rogue";          break;
				case CLASS_PRIEST:          Class_s = "Priest";         break;
				case CLASS_DEATH_KNIGHT:    Class_s = "Death Knight";   break;
				case CLASS_SHAMAN:          Class_s = "Shaman";         break;
				case CLASS_MAGE:            Class_s = "Mage";           break;
				case CLASS_WARLOCK:         Class_s = "Warlock";        break;
				case CLASS_DRUID:           Class_s = "Druid";          break;
				default:                    Class_s = "unknown";
				}
				switch (Gender)
				{
				case GENDER_MALE:           Gender_s = "Male";          break;
				case GENDER_FEMALE:         Gender_s = "Female";        break;
				default:                    Gender_s = "unknown";       break;
				}

				int locale = player->GetSession()->GetSessionDbcLocale();
				std::string areaName = "<unknown>";
				std::string zoneName = "<unknown>";
				std::string mapName = "<unknown>";

				if (MapEntry const* map = sMapStore.LookupEntry(mapId))
					mapName = map->name[locale];

				AreaTableEntry const* area = GetAreaEntryByAreaID(areaId);
				if (area)
				{
					areaName = area->area_name[locale];

					AreaTableEntry const* zone = GetAreaEntryByAreaID(area->zone);
					if (zone)
						zoneName = zone->area_name[locale];
				}

				const PlayerRankData& rankdata = GetPlayerRankData();
				std::vector<std::pair<float, uint32> > items;
				// The old loop
				for (PlayerRankData::const_iterator it = rankdata.begin(); it != rankdata.end(); ++it)
				{
					uint32 charguid = it->first;
					float points = it->second;
					items.push_back(std::make_pair(points, charguid));
				}
				std::sort(items.begin(), items.end());
				uint32 count = 0;
				uint32 searched_playerid = guid;
				for (std::vector<std::pair<float, uint32> >::const_reverse_iterator it = items.rbegin(); it != items.rend(); ++it)
				{
					++count;
					if ((*it).second == searched_playerid)
					{
						TopRankG << "|TInterface\\icons\\Achievement_worldevent_lunar:27:28:-22:0|tBoard Rank: " << count << " of " << items.size() << " Players. ";
						break;
					}
				}

				if (player->GetRankPoints() < 1000)
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Quests_Completed_07:35:35:-22:0|tScoreboard Information.", GOSSIP_SENDER_MAIN, 5);
				else
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Quests_Completed_08:35:35:-22:0|tScoreboard Information.", GOSSIP_SENDER_MAIN, 5);
				if (target && target->GetSession()->IsPremium())
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_BG_trueAVshutout:35:35:-22:0|t* VIP Member *", GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------", GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Dungeon_Outland_Dungeon_Hero:35:35:-22:0|tPlayer Information.", GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tName: " + name, GOSSIP_SENDER_MAIN, 5);
				if (online)
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tGuild: " + Guild, GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tStatus: " + getPlayerStatus(code), GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tLevel: " + level.str(), GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tRace: " + race_s, GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tClass: " + Class_s, GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tKills Today: " + KillsToday.str(), GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tLifetime Kills: " + KillsLifetime.str(), GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------", GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_dungeon_utgardepinnacle_normal:35:35:-22:0|t - World Ranking Leaderboard - ", GOSSIP_SENDER_MAIN, 8);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, Rank.str(), GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, TopRank.str(), GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, TopRankG.str(), GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------", GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_bg_grab_cap_flagunderxseconds:32:32:-22:0|tTotal Playedtime:\n \n " + playedTime, GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------", GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Zone_ArathiHighlands_01:31:32:-22:0|tLocation Information:", GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Zone_Northrend_01:27:27:-22:0|tMap: " + mapName, GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Zone_Kalimdor_01:27:27:-22:0|tArea: " + areaName, GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------", GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Holy_DevineAegis:30:30:-22:0|tBack..", GOSSIP_SENDER_MAIN, 5);
				player->SEND_GOSSIP_MENU(65004, player->GetGUID());
				return;
			}
		}
			/*else if (_action == 11)
			{
				if (!validString4(code))
				{
					ChatHandler(player->GetSession()).PSendSysMessage("Please enter a real message.");
					return;
				}

				// populate field
				std::string Creation;
				std::ostringstream MemberCount;
				std::string name;
				std::string Motd = "";
				std::string leaderName = "Unknown.";
				std::ostringstream Rank;
				std::ostringstream TopRank;
				std::ostringstream RankG;
				std::ostringstream TopRankG;
				std::ostringstream experience;
				std::ostringstream level;

				if (Guild* guild = sGuildMgr->GetGuildByName(code))
				{
					QueryResult result = CharacterDatabase.PQuery("SELECT name FROM characters WHERE guid = %u", guild->GetLeaderGUID());
					if (result)
					{
						Field *fields = result->Fetch();
						leaderName = fields[0].GetString();
					}

					MemberCount << (uint32)guild->GetMemberCount();
					name = guild->GetName();
					Creation = TimeToTimestampStr2(guild->GetCreatedDate());
					Motd = guild->GetMOTD();
					experience << "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tExperience: [" << guild->GetCurrentXP() << " of " << guild->GetXpForNextLevel() << "]";
					level << (uint32)guild->GetLevel();
					Rank << (uint32)GetPlayerRankPoints(guild->GetId());
					RankG << "|TInterface\\icons\\Achievement_worldevent_lunar:27:28:-22:0|tRankpoints: " << (uint32)guild->GetRankPoints() << ".";
					TopRankG << "|TInterface\\icons\\Achievement_worldevent_lunar:27:28:-22:0|tTop Rankpoints: " << (uint32)guild->GetTopRankPoints() << ".";

					const PlayerRankData& rankdata = GetPlayerRankData();
					std::vector<std::pair<float, uint32> > items;
					// The old loop
					for (PlayerRankData::const_iterator it = rankdata.begin(); it != rankdata.end(); ++it)
					{
						uint32 guildid = it->first;
						float points = it->second;
						items.push_back(std::make_pair(points, guildid));
					}
					std::sort(items.begin(), items.end());
					uint32 count = 0;
					uint32 searched_guildid = guild->GetId();
					for (std::vector<std::pair<float, uint32> >::const_reverse_iterator it = items.rbegin(); it != items.rend(); ++it)
					{
						++count;
						if ((*it).second == searched_guildid)
						{
							TopRank << "|TInterface\\icons\\Achievement_worldevent_lunar:27:28:-22:0|tBoard Rank: " << count << " of " << items.size() << " Guilds. ";
							break;
						}
					}
				}
				else
				{
					player->GetSession()->SendAreaTriggerMessage("No guild name with the name: %s exists.", code);
					player->PlayerTalkClass->SendCloseGossip(); // main menu
					return;
				}

				/*if (player->GetGuild()->GetRankPoints() > 0 && player->GetGuild()->GetRankPoints() < 500)
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_PVP_P_12:35:35:-22:0|t Guild Scoreboard.", GOSSIP_SENDER_MAIN, 11);
				else if (player->GetGuild()->GetRankPoints() >= 1500 && player->GetGuild()->GetRankPoints() < 3000)
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_PVP_P_13:35:35:-22:0|t Guild Scoreboard.", GOSSIP_SENDER_MAIN, 11);
				else if (player->GetGuild()->GetRankPoints() >= 3000)
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_PVP_P_14:35:35:-22:0|t Guild Scoreboard.", GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------", GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Dungeon_Outland_Dungeon_Hero:35:35:-22:0|tGuild Information.", GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tName: " + name, GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tGuild Leader: " + leaderName, GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tLevel: " + level.str(), GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, experience.str().c_str(), GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tMember Count: " + MemberCount.str(), GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentChampion:27:28:-22:0|tCreation Date: " + Creation, GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------", GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Dungeon_UtgardeKeep_Normal:35:35:-22:0|t - Guild Level Bonuses.", GOSSIP_SENDER_MAIN, 5);
				if (player->GetGuild()->GetLevel() > 0)
				{
					if (player->GetGuild()->HasLevelForBonus(GUILD_BONUS_SCHNELLER_GEIST))
						player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentCrusader:27:28:-22:0|t Faster Ghost", GOSSIP_SENDER_MAIN, 5);
					if (player->GetGuild()->HasLevelForBonus(GUILD_BONUS_GOLD_1) && !player->GetGuild()->HasLevelForBonus(GUILD_BONUS_GOLD_2))
						player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentCrusader:27:28:-22:0|t Gold Bonus [Rank 1: |cffFF0000+10%|r", GOSSIP_SENDER_MAIN, 5);
					if (player->GetGuild()->HasLevelForBonus(GUILD_BONUS_XP_1) && !player->GetGuild()->HasLevelForBonus(GUILD_BONUS_XP_2))
						player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentCrusader:27:28:-22:0|t Bonus Experience [Rank 1: |cffFF0000+10%|r]", GOSSIP_SENDER_MAIN, 5);
					if (player->GetGuild()->HasLevelForBonus(GUILD_BONUS_REPERATUR_1) && !player->GetGuild()->HasLevelForBonus(GUILD_BONUS_REPERATUR_2))
						player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentCrusader:27:28:-22:0|t Cheaper Repairs [Rank 1: |cffFF0000+10%|r]", GOSSIP_SENDER_MAIN, 5);
					if (player->GetGuild()->HasLevelForBonus(GUILD_BONUS_GOLD_2))
						player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentCrusader:27:28:-22:0|t Gold Bonus [Rank 2: |cffFF0000+20%|r]", GOSSIP_SENDER_MAIN, 5);
					if (player->GetGuild()->HasLevelForBonus(GUILD_BONUS_REITTEMPO_1) && !player->GetGuild()->HasLevelForBonus(GUILD_BONUS_REITTEMPO_2))
						player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentCrusader:27:28:-22:0|t Mount Speed [Rank 1: |cffFF0000+10%|r]", GOSSIP_SENDER_MAIN, 5);
					if (player->GetGuild()->HasLevelForBonus(GUILD_BONUS_RUF_1) && !player->GetGuild()->HasLevelForBonus(GUILD_BONUS_RUF_2))
						player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentCrusader:27:28:-22:0|t Reputation [Rank 1: |cffFF0000+10%|r]", GOSSIP_SENDER_MAIN, 5);
					if (player->GetGuild()->HasLevelForBonus(GUILD_BONUS_XP_2))
						player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentCrusader:27:28:-22:0|t Bonus Experience [Rank 2: |cffFF0000+20%|r]", GOSSIP_SENDER_MAIN, 5);
					if (player->GetGuild()->HasLevelForBonus(GUILD_BONUS_REPERATUR_2))
						player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentCrusader:27:28:-22:0|t Cheaper Repairs [Rank 2: |cffFF0000+20%|r]", GOSSIP_SENDER_MAIN, 5);
					if (player->GetGuild()->HasLevelForBonus(GUILD_BONUS_REITTEMPO_2))
						player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentCrusader:27:28:-22:0|t Mount Speed [Rank 2: |cffFF0000+20%|r]", GOSSIP_SENDER_MAIN, 5);
					if (player->GetGuild()->HasLevelForBonus(GUILD_BONUS_RUF_2))
						player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentCrusader:27:28:-22:0|t Reputation [Rank 2: |cffFF0000+20%|r]", GOSSIP_SENDER_MAIN, 5);
					if (player->GetGuild()->HasLevelForBonus(GUILD_BONUS_EHRE_1) && !player->GetGuild()->HasLevelForBonus(GUILD_BONUS_EHRE_2))
						player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentCrusader:27:28:-22:0|t Bonus Honor [Rank 1: |cffFF0000+10%|r]", GOSSIP_SENDER_MAIN, 5);
					if (player->GetGuild()->HasLevelForBonus(GUILD_BONUS_EHRE_2))
						player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_Reputation_ArgentCrusader:27:28:-22:0|t Bonus Honor [Rank 2: |cffFF0000+20%|r]", GOSSIP_SENDER_MAIN, 5);
				}
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------", GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Achievement_dungeon_utgardepinnacle_normal:35:35:-22:0|t - Guild World Leaderboard - ", GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, TopRank.str(), GOSSIP_SENDER_MAIN, 10);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, RankG.str(), GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, TopRankG.str(), GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "----------------------------------", GOSSIP_SENDER_MAIN, 5);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "|TInterface\\icons\\Spell_Holy_DevineAegis:30:30:-22:0|tBack..", GOSSIP_SENDER_MAIN, 5);
				player->SEND_GOSSIP_MENU(65004, player->GetGUID());
				return;
			}
		}*/
		else if (menu_id == 10)
		{
			if (_action == 2500)
			{
				if (!validString4(code))
				{
					ChatHandler(player->GetSession()).PSendSysMessage("Please enter a real message.");
					return;
				}

				uint32 money = atoi(code) * 10000;
				if (money <= 0)
				{
					player->GetSession()->SendNotification("You must place a real amount.");
					//OnGossipHello(player, menu_id);
					return;
				}

				if (!player->HasEnoughMoney(money))
				{
					player->GetSession()->SendNotification("You do not have that amount of money.");
					//OnGossipHello(player, menu_id);
					return;
				}

				player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_MONEY_BAG, "Enter Name", 5000, money, "Type in the player's name", 0, true);
				player->SEND_GOSSIP_MENU(65006, player->GetGUID());
				return;
			}
			else if (sender == 5000)
			{
				if (!validString4(code))
				{
					ChatHandler(player->GetSession()).PSendSysMessage("Please enter a real message.");
					return;
				}

				std::string ccode = code;
				std::transform(ccode.begin(), ccode.end(), ccode.begin(), ::tolower);
				ccode[0] = toupper(ccode[0]);

				if (player->GetName().c_str() == ccode)
				{
					ChatHandler(player->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFNow why would you want to challenge yourself?");
					player->CLOSE_GOSSIP_MENU();
					return;
				}
				if (ObjectGuid targetGUID = sObjectMgr->GetPlayerGUIDByName(ccode))
				{
					if (Player* target = Player::GetPlayer(*player, targetGUID))
					{
						if (target->GetGUID() == player->GetGUID())
						{
							ChatHandler(player->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFNow why would you want to challenge yourself?");
							player->CLOSE_GOSSIP_MENU();
							return;
						}
						if (target->GetZoneId() == player->GetZoneId())
						{
							if (USE_TOKEN)
							{
								if (target->GetItemCount(TOKEN_ID) < _action)
								{
									ChatHandler(player->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFThat player does not have enough tokens to make the bet!");
									player->CLOSE_GOSSIP_MENU();
									return;
								}
								if (player->GetItemCount(TOKEN_ID) < _action)
								{
									ChatHandler(player->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou do not have enough tokens to make the bet!");
									player->CLOSE_GOSSIP_MENU();
									return;
								}

								bool found = false;
								if (HasBloodMoneyChallenger(player->GetGUID()))
								{
									BloodMoneyList list = m_bloodMoney[player->GetGUID()];
									for (BloodMoneyList::const_iterator itr = list.begin(); itr != list.end(); ++itr)
										if (itr->guid == target->GetGUID())
											found = true;
								}
								if (!found)
								{
									if (!HasBloodMoneyChallenger(target->GetGUID(), player->GetGUID()))
									{
										AddBloodMoneyEntry(target->GetGUID(), player->GetGUID(), _action);
										ChatHandler(target->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFF%s has requested to Gamble on a duel with you!", player->GetName().c_str());
										ChatHandler(player->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou have requested to Gamble on a duel against %s", target->GetName().c_str());
									}
									else
										ChatHandler(target->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou cannot request a duel with the same person!");
								}
								else
									ChatHandler(player->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou cannot request a duel with somebody that has challenged you!");
								player->CLOSE_GOSSIP_MENU();
								return;
							}
							else
							{
								uint32 money = _action;
								if (target->GetMoney() < money)
								{
									ChatHandler(player->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFThat player does not have enough money to make the bet!");
									player->CLOSE_GOSSIP_MENU();
									return;
								}
								if (player->GetMoney() < money)
								{
									ChatHandler(player->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou do not have enough money to make the bet!");
									player->CLOSE_GOSSIP_MENU();
									return;
								}

								bool found = false;
								if (HasBloodMoneyChallenger(player->GetGUID()))
								{
									BloodMoneyList list = m_bloodMoney[player->GetGUID()];
									for (BloodMoneyList::const_iterator itr = list.begin(); itr != list.end(); ++itr)
										if (itr->guid == target->GetGUID())
											found = true;
								}
								if (!found)
								{
									if (!HasBloodMoneyChallenger(target->GetGUID(), player->GetGUID()))
									{
										AddBloodMoneyEntry(target->GetGUID(), player->GetGUID(), money);
										ChatHandler(target->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFF%s has requested to Gamble on a duel with you!", player->GetName().c_str());
										ChatHandler(player->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou have requested to Gamble on a duel against %s", target->GetName().c_str());
									}
									else
										ChatHandler(target->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou cannot request a duel with the same person!");
								}
								else
									ChatHandler(player->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou cannot request a duel with somebody that has challenged you!");
								player->CLOSE_GOSSIP_MENU();
								return;
							}

						}
						else
						{
							ChatHandler(player->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFThat player is not in your zone!");
							player->CLOSE_GOSSIP_MENU();
							return;
						}
					}
					else
					{
						ChatHandler(player->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFThat player was not found!");
						player->CLOSE_GOSSIP_MENU();
						return;
					}
				}
				else
				{
					ChatHandler(player->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFThat player was not found!");
					player->CLOSE_GOSSIP_MENU();
					return;
				}
				player->CLOSE_GOSSIP_MENU();
				return;
			}
		}
		else  if (menu_id == 60)
		{
			player->PlayerTalkClass->ClearMenus();

			uint8 pageid, action;
			unmelt(_action, action, pageid);

			if (action == 1)
			{
				if (!code || !validString4(code))
				{
					//TC_LOG_ERROR("!code","Invalid code: %s ", code);

					player->GetSession()->SendNotification("You must enter a real character name.");
				}
				else
				{
					//TC_LOG_ERROR("NAME.SHOW", "GossipSelectCode: NAME: %s, action: %u", code, action);
					if (showResults(player, code, action, 0))
						return;
				}
			}
			else if (action == 2)
			{
				if (!code || !validString4(code))
				{
					//TC_LOG_ERROR("!code", "Invalid code: %s ", code);

					player->GetSession()->SendNotification("You must enter a real account ID.");
				}
				else
				{
					//TC_LOG_ERROR("ID.SHOW","GossipSelectCode: ID: %u, action: %u", atol(code), action);
					if (showResults(player, atol(code), action, 0))
						return;
				}
			}
			//OnGossipHello(player, menu_id); // main menu
		}
		//OnGossipHello(player, menu_id); // main menu
	}
};

class BloodMoneyReward : public PlayerScript
{
public:
	BloodMoneyReward() : PlayerScript("BloodMoneyReward") {}

	void OnDuelEnd(Player* winner, Player* loser, DuelCompleteType type)
	{
		if (HasBloodMoneyChallenger(winner->GetGUID()) || HasBloodMoneyChallenger(loser->GetGUID()))
		{
			BloodMoneyList list1 = m_bloodMoney[winner->GetGUID()];
			BloodMoneyList list2 = m_bloodMoney[loser->GetGUID()];

			BloodMoneyList::const_iterator itr;
			for (itr = list1.begin(); itr != list1.end(); ++itr)
			{
				if (itr->guid == loser->GetGUID() && itr->accepted)
				{
					if (USE_TOKEN)
					{
						if (winner->GetItemCount(TOKEN_ID) < itr->amount)
						{
							winner->AddAura(15007, winner);         // Apply Rez sickness for possible cheating
							ChatHandler(winner->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou have gained Resurrection Sickness for possibly trying to abuse the system.");
							ChatHandler(loser->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYour opponent tried to cheat you. Don't worry you did not lose any tokens because of this.");
							RemoveBloodMoneyEntry(winner->GetGUID(), itr->guid);
							return;
						}
						if (loser->GetItemCount(TOKEN_ID) >= itr->amount)
						{
							winner->AddItem(TOKEN_ID, itr->amount);
							ChatHandler(winner->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFCongratulations on winning %d tokens!", itr->amount);
							ChatHandler(loser->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou just lost a duel bet of %d tokens..", itr->amount);
							Item* item = loser->GetItemByEntry(TOKEN_ID);
							loser->DestroyItemCount(TOKEN_ID, itr->amount, true);
							RemoveBloodMoneyEntry(winner->GetGUID(), itr->guid);
						}
						else
						{
							loser->AddAura(15007, loser);           // Apply Rez sickness for possible cheating
							ChatHandler(winner->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYour opponent tried to cheat you. He did not have enough tokens to pay off the bet.");
							ChatHandler(loser->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou have gained Resurrection Sickness for possibly trying to abuse the system.");
							RemoveBloodMoneyEntry(winner->GetGUID(), itr->guid);
						}
						return;
					}
					else
					{
						if (winner->GetMoney() < itr->amount)
						{
							winner->AddAura(15007, winner);         // Apply Rez sickness for possible cheating
							ChatHandler(winner->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou have gained Resurrection Sickness for possibly trying to abuse the system.");
							ChatHandler(loser->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYour opponent tried to cheat you. Don't worry you did not lose any money because of this.");
							RemoveBloodMoneyEntry(winner->GetGUID(), itr->guid);
							return;
						}
						if (loser->GetMoney() >= itr->amount)
						{
							winner->ModifyMoney(itr->amount);
							ChatHandler(winner->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFCongratulations on winning %dg!", itr->amount / 10000);
							ChatHandler(loser->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou just lost a bet of %dg...", itr->amount / 10000);
							loser->ModifyMoney(-(int32)(itr->amount));
							RemoveBloodMoneyEntry(winner->GetGUID(), itr->guid);
						}
						else
						{
							loser->AddAura(15007, loser);           // Apply Rez sickness for possible cheating
							ChatHandler(winner->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYour opponent tried to cheat you. He did not have enough money to pay off the bet.");
							ChatHandler(loser->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou have gained Resurrection Sickness for possibly trying to abuse the system.");
							RemoveBloodMoneyEntry(winner->GetGUID(), itr->guid);
						}
						return;
					}
				}
			}
			for (itr = list2.begin(); itr != list2.end(); ++itr)
			{
				if (itr->guid == winner->GetGUID() && itr->accepted)
				{
					if (USE_TOKEN)
					{
						if (winner->GetItemCount(TOKEN_ID) < itr->amount)
						{
							winner->AddAura(15007, winner);         // Apply Rez sickness for possible cheating
							ChatHandler(winner->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou have gained Resurrection Sickness for possibly trying to abuse the system.");
							ChatHandler(loser->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYour opponent tried to cheat you. Don't worry you did not lose any tokens because of this.");
							RemoveBloodMoneyEntry(loser->GetGUID(), itr->guid);
							return;
						}
						if (loser->GetItemCount(TOKEN_ID) >= itr->amount)
						{
							winner->AddItem(TOKEN_ID, itr->amount);
							ChatHandler(winner->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFCongratulations on winning %d tokens!", itr->amount);
							ChatHandler(loser->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou just lost a duel bet of %d tokens...", itr->amount);
							Item* item = loser->GetItemByEntry(TOKEN_ID);
							loser->DestroyItemCount(TOKEN_ID, itr->amount, true);
							RemoveBloodMoneyEntry(loser->GetGUID(), itr->guid);
						}
						else
						{
							loser->AddAura(15007, loser);           // Apply Rez sickness for possible cheating
							ChatHandler(winner->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYour opponent tried to cheat you. He did not have enough tokens to pay off the bet.");
							ChatHandler(loser->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou have gained Resurrection Sickness for possibly trying to abuse the system.");
							RemoveBloodMoneyEntry(loser->GetGUID(), itr->guid);
						}
						return;
					}
					else
					{
						if (winner->GetMoney() < itr->amount)
						{
							winner->AddAura(15007, winner);         // Apply Rez sickness for possible cheating
							ChatHandler(winner->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou have gained Resurrection Sickness for possibly trying to abuse the system.");
							ChatHandler(loser->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYour opponent tried to cheat you. Don't worry you did not lose any money because of this.");
							RemoveBloodMoneyEntry(loser->GetGUID(), itr->guid);
							return;
						}
						if (loser->GetMoney() >= itr->amount)
						{
							winner->ModifyMoney(itr->amount);
							ChatHandler(winner->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFCongratulations on winning %dg!", itr->amount / 10000);
							ChatHandler(loser->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou just lost a duel bet of %dg...", itr->amount / 10000);
							loser->ModifyMoney(-(int32)(itr->amount));
							RemoveBloodMoneyEntry(loser->GetGUID(), itr->guid);
						}
						else
						{
							loser->AddAura(15007, loser);           // Apply Rez sickness for possible cheating
							ChatHandler(winner->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYour opponent tried to cheat you. He did not have enough money to pay off the bet.");
							ChatHandler(loser->GetSession()).PSendSysMessage("|cff800C0C[Duel Gambler] |cffFFFFFFYou have gained Resurrection Sickness for possibly trying to abuse the system.");
							RemoveBloodMoneyEntry(loser->GetGUID(), itr->guid);
						}
						return;
					}
				}
			}
		}
	}
};

class toggle_commandscript : public CommandScript
{
public:
    toggle_commandscript() : CommandScript("toggle_commandscript") { }

    ChatCommand* GetCommands() const override
    {

        static ChatCommand toggleCommandTable[] =
        {
			{ "chat",		  rbac::RBAC_PERM_COMMAND_HELP,      false, &HandleToggleWorldChatCommand,   "", NULL },
			{ "announcements",rbac::RBAC_PERM_COMMAND_HELP,      false, &HandleToggleAnnouncesCommand,   "", NULL },
			{ "status",       rbac::RBAC_PERM_COMMAND_HELP,      false, &HandleToggleStatusCommand,   "", NULL },
            { "appear",       rbac::RBAC_PERM_COMMAND_HELP,	  false, &HandleToggleAppearCommand,  "", NULL },
            { "summon",       rbac::RBAC_PERM_COMMAND_HELP,	  false, &HandleToggleSummonCommand,  "", NULL },
            { NULL,           0,                                      false, NULL,                         "", NULL }

        };

        static ChatCommand commandTable[] =
        {
			{ "unstuck", rbac::RBAC_PERM_COMMAND_HELP, false, &HandleUnstuckCommand, "", NULL },
			{ "rank", rbac::RBAC_PERM_COMMAND_HELP, false, &HandleRankCommand, "", NULL },
            { "toggle",          rbac::RBAC_PERM_COMMAND_HELP, false, NULL, "", toggleCommandTable },
            { NULL,             0,                       false, NULL, "", NULL }
        };
        return commandTable;
    }
	
	static bool HandleRankCommand(ChatHandler* handler, const char* /*args*/)
	{
		std::ostringstream TopRankG;
		const PlayerRankData& rankdata = GetPlayerRankData();
		std::vector<std::pair<float, uint32> > items;
		// The old loop
		for (PlayerRankData::const_iterator it = rankdata.begin(); it != rankdata.end(); ++it)
		{
			uint32 charguid = it->first;
			float points = it->second;
			items.push_back(std::make_pair(points, charguid));
		}
		std::sort(items.begin(), items.end());
		uint32 count = 0;
		uint32 searched_playerid = handler->GetSession()->GetPlayer()->GetGUIDLow();
		for (std::vector<std::pair<float, uint32> >::const_reverse_iterator it = items.rbegin(); it != items.rend(); ++it)
		{
			++count;
			if ((*it).second == searched_playerid)
			{
				TopRankG << "|TInterface\\icons\\Achievement_worldevent_lunar:20:20:-5:0|tYou are ranked: " << count << " of " << items.size() << " Players. ";
				break;
			}
		}

		handler->PSendSysMessage("You have: |cffFF0000%u|r Rank Points.", uint32(handler->GetSession()->GetPlayer()->GetRankPoints()));
		handler->SendSysMessage(TopRankG.str().c_str());
		handler->PSendSysMessage("You must have at least |cffFF000050|r Rank Points to enter the world leaderboard.\nThe World Board reloads every 10 minutes.");
		return true;
	}

	static bool HandleUnstuckCommand(ChatHandler* handler, const char* /*args*/)
	{
		Player* player = handler->GetSession()->GetPlayer();

		if (player->GetMap()->IsBattlegroundOrArena())
		{
			handler->SendSysMessage("You can't cast unstuck in a battleground.");
			handler->SetSentErrorMessage(true);
			return false;
		}

		if (player->IsInCombat())
		{
			handler->SendSysMessage("You can't cast unstuck while in combat.");
			handler->SetSentErrorMessage(true);
			return false;
		}

		if (player->IsInFlight())
		{
			handler->SendSysMessage("You can't cast unstuck while in flight.");
			handler->SetSentErrorMessage(true);
			return false;
		}

		player->TeleportTo(530, 8435.507812f, -7595.535645f, 157.055557f, 1.923103f);
		return true;
	}

	static bool HandleToggleWorldChatCommand(ChatHandler* handler, const char* args)
	{
		if (!handler->GetSession() && !handler->GetSession()->GetPlayer())
			return false;

		std::string argstr = (char*)args;

		if (!*args)
			argstr = (handler->GetSession()->GetPlayer()->GetWorldChat() ? "off" : "on");

		if (argstr == "off")
		{
			handler->GetSession()->GetPlayer()->SetWorldChat(false);
			handler->SendSysMessage("World Chat is off. You will no longer see the messages.");
			return true;
		}
		else if (argstr == "on")
		{
			handler->GetSession()->GetPlayer()->SetWorldChat(true);
			handler->SendSysMessage("World Chat is on. You can now see the messages.");
			return true;
		}

		return true;
	}

	static bool HandleToggleAnnouncesCommand(ChatHandler* handler, const char* args)
	{
		if (!handler->GetSession() && !handler->GetSession()->GetPlayer())
			return false;

		std::string argstr = (char*)args;

		if (!*args)
			argstr = (handler->GetSession()->GetPlayer()->GetAnnouncers() ? "off" : "on");

		if (argstr == "off")
		{
			handler->GetSession()->GetPlayer()->SetAnnouncers(false);
			handler->SendSysMessage("Announcements are off. You can no longer see BG announcement or staff announcements.");
			return true;
		}
		else if (argstr == "on")
		{
			handler->GetSession()->GetPlayer()->SetAnnouncers(true);
			handler->SendSysMessage("Announcements are on.");
			return true;
		}

		return true;
	}

	static bool HandleToggleAppearCommand(ChatHandler* handler, const char* args)
	{
		if (!handler->GetSession() && !handler->GetSession()->GetPlayer())
			return false;

		std::string argstr = (char*)args;

		if (!*args)
			argstr = (handler->GetSession()->GetPlayer()->GetAllowAppear() ? "off" : "on");

		if (argstr == "off")
		{
			handler->GetSession()->GetPlayer()->SetAllowAppear(false);
			handler->SendSysMessage("Appearing is off. Other players can't appear you.");
			return true;
		}
		else if (argstr == "on")
		{
			handler->GetSession()->GetPlayer()->SetAllowAppear(true);
			handler->SendSysMessage("Appearing is on. Other players can appear you.");
			return true;
		}

		return true;
	}

	static bool HandleToggleSummonCommand(ChatHandler* handler, const char* args)
	{
		if (!handler->GetSession() && !handler->GetSession()->GetPlayer())
			return false;

		std::string argstr = (char*)args;

		if (!*args)
			argstr = (handler->GetSession()->GetPlayer()->GetAllowSummon() ? "off" : "on");

		if (argstr == "off")
		{
			handler->GetSession()->GetPlayer()->SetAllowSummon(false);
			handler->SendSysMessage("Summoning is off. Other players can't summon you.");
			return true;
		}
		else if (argstr == "on")
		{
			handler->GetSession()->GetPlayer()->SetAllowSummon(true);
			handler->SendSysMessage("Summoning is on. Other players can summon you.");
			return true;
		}

		return true;
	}

	static bool HandleToggleStatusCommand(ChatHandler* handler, const char* /*args*/)
	{
		if (!handler->GetSession() && !handler->GetSession()->GetPlayer())
			return false;

		handler->SendSysMessage(".:Toggle Status:.");
		if (handler->GetSession()->GetPlayer()->GetWorldChat())
			handler->SendSysMessage("World Chat: |cffFF0000Enabled.|r");
		else
			handler->SendSysMessage("World Chat: |cffFF0000Disabled.|r");
		if (handler->GetSession()->GetPlayer()->GetAnnouncers())
			handler->SendSysMessage("Announces: |cffFF0000Enabled.|r");
		else
			handler->SendSysMessage("Announces: |cffFF0000Disabled.|r");
		if (handler->GetSession()->GetPlayer()->GetAllowAppear())
			handler->SendSysMessage("Appearing: |cffFF0000Enabled.|r");
		else
			handler->SendSysMessage("Appearing: |cffFF0000Disabled.|r");
		if (handler->GetSession()->GetPlayer()->GetAllowSummon())
			handler->SendSysMessage("Summoning: |cffFF0000Enabled.|r");
		else
			handler->SendSysMessage("Summoning: |cffFF0000Disabled.|r");

		return true;
	}
};

void AddSC_toggle_commandscript()
{
    new toggle_commandscript();
	//new Devcommandscript();
	new custom_PlayerGossip();
	new BloodMoneyReward();
}
