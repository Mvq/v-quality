#include "ScriptPCH.h"
#include "SuffixVendor.h"

void SuffixVendor::HandleSuffix(Player* player, Creature* creature, uint32 sender, uint32 entry)
{
    QueryResult result = CharacterDatabase.PQuery("SELECT DISTINCT entry, suffix, name FROM suffix where entry=%u ORDER BY name", entry);

    if (!result)
    {
        ChatHandler(player->GetSession()).PSendSysMessage("There is no available suffix for this item");
        return;
    }

    ChatHandler(player->GetSession()).PSendSysMessage("You tried to purchase item id: %u", entry); // Were used for debug purposes

    /*Item* pItem = Item::CreateItem(entry, 1);
    ItemTemplate const* proto = pItem->GetTemplate();

    std::string iName = proto->Name1;*/ // Unwanted Feature

    //delete pItem;
    player->PlayerTalkClass->ClearMenus();
    do {
        Field *fields = result->Fetch();
        uint32 item = fields[0].GetUInt32();
        uint32 suffix = fields[1].GetUInt32();
        std::string name = fields[2].GetString();

        std::stringstream buffer;

        buffer << name;

        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, buffer.str(), suffix, entry);
        
    } while (result->NextRow());
    
    player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
}

class NPC_suffix : public CreatureScript
{
public:
    NPC_suffix() : CreatureScript("SuffixVendor")
	{
	}

    bool OnGossipHello(Player* player, Creature* creature) override
    {
        player->GetSession()->SendListInventory(creature->GetGUID());
		return true;
    }

	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action) override // Suffix = sender, Item entry = Action
	{
		switch (action)
		{

        default:
        {
            player->PlayerTalkClass->ClearMenus();
                
                ItemPosCountVec dest;
                uint8 msg = player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, action, 1); 
                if (msg == EQUIP_ERR_OK)
                    player->StoreNewItem(dest, action, true, sender);

                player->CLOSE_GOSSIP_MENU();
            break;
        }
		}
		return true;
	}
};


void AddSC_NPC_suffix()
{
    new NPC_suffix();
}